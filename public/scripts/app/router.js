// router.js
App.router = new(Backbone.Router.extend({

  setUrl: function(path) {
    this.navigate(path);
  },
  redirect: function(path) {
    if (typeof(App.page) !== 'undefined' && App.page && typeof(App.page.isReady) !== 'undefined' && !App.page.isReady)
      App.loadingStatus(false);
    this.navigate(path, {
      trigger: true
    });
  },

  routes: {
    "(/)": "index", // #help
    "signin(/)": "signin",
    "profiles(/)": "profiles",
    "profile(/)": "profile",
    "service/make(/)": "servicemake",
    "service/make/:make_id/model(/)": "servicemodel",
    "user/updatepassword/:code/:hash": "updatePassword",
    "dashboard(/)": "dashboard",
    "dashboard/quotes": "dashboard",
    "dashboard/quotes/:quote_id": "dashboardquote",
    "acquaintance(/)": "acquaintance",
    "acquaintance-ok(/)": "acquaintanceok",
    "insurer(/)": "insurer",
    "insured(/)": "insured",
    "insurance(/)": "insurance",
    "quotes/:quote_id/ok": "insuranceok",
    "quotes/:quote_id/vehicles/:vehicle_id/drivers": "drivers",
    "quotes/:quote_id/vehicles/:vehicle_id/insurancetype": "vehicleinsurancetype",
    "quotes/:quote_id/vehicles/:vehicle_id/value": "vehiclevalue",
    "quotes/:quote_id/vehicles/:vehicle_id/makemodel":"vehiclemakemodel",
    "quotes/:quote_id/vehicles/:vehicle_id/plannedusage":"vehicleplannedusage",
    "quotes/:quote_id/vehicles/:vehicle_id/drivinghistory":"drivinghistory",
    "quotes/:quote_id/vehicles/:vehicle_id/accident":"vehicleaccident",
    "quotes/:quote_id/vehicles/:vehicle_id/benefits":"vehiclebenefits",
    "quotes/:quote_id/vehicles/:vehicle_id/discounts":"vehiclediscounts",
    "quotes/:quote_id/vehicles":"vehicles",
    "quotes/:quote_id/immovables/:immovable_id/insurancetypeandusage":"insurancetypeandusage",
    "quotes/:quote_id/immovables/:immovable_id/address":"immovableaddress",
    "quotes/:quote_id/immovables/:immovable_id/characteristics":"immovablecharacteristics",
    "quotes/:quote_id/immovables/:immovable_id/security":"immovablesecurity",
    "quotes/:quote_id/immovables/:immovable_id/benefits":"immovablebenefits",
    "quotes/:quote_id/submit": "submitquote",
    "quotes/:quote_id/confirmation": "quoteconfirmation",
  },

  dialogs: {
    "user/signin": "Signin",
    "user/registration": "Registration",
    "user/restore": "Restore",
    "user/logout": "Logout"
  },

  index: function() {
    App.showPage('Index');
  },

  signin: function () {
     App.showPage('Signin');
  },

  profiles: function() {
    App.showPage('Profiles');
  },
  profile: function() {
    App.showPage('Profile');
  },

  updatePassword: function(code, hash) {
    App.showPage('UpdatePassword', {
      password_restore_code: code,
      password_restore_hash: hash
    });
  },

  servicemake: function() {
    App.showPage('ServiceMake');
  },

  servicemodel: function(id) {
    App.showPage('ServiceModel', {
      id: id
    });
  },

  dashboard: function() {
    App.showPage('Dashboard');
  },
  dashboardquote: function (id) {
      App.showPage('DashboardQuote', {
          id: id
      });
  },
  acquaintance: function() {
    App.showPage('Acquaintance');
  },

  acquaintanceok: function() {
    App.showPage('AcquaintanceOk');
  },

  insurer: function() {
    App.showPage('Insurer');
  },

  insurance: function() {
    App.showPage('Insurance');
  },

  insured: function (id) {
      App.showPage('Insured');
  },
  insuranceok: function (id) {
      App.showPage('InsuranceOk', {
          id: id
      });
  },

  drivers: function (quote_id, id) {
      App.showPage('Drivers', {
          id: id,
          quote_id: quote_id 
      });
  },
  vehicleinsurancetype: function (quote_id, id) {
      App.showPage('VehicleInsuranceType', {
          id: id,
          quote_id: quote_id 
      });
  },
  vehiclevalue: function (quote_id, id) {
      App.showPage('VehicleValue', {
          id: id,
          quote_id: quote_id 
      });
  },
  vehiclemakemodel: function (quote_id, id) {
      App.showPage('VehicleMakeModel', {
          id: id,
          quote_id: quote_id 
      });
  },
  vehicleplannedusage: function (quote_id, id) {
      App.showPage('VehiclePlannedUsage', {
          id: id,
          quote_id: quote_id 
      });
  },
  drivinghistory: function (quote_id, id) {
      App.showPage('DrivingHistory', {
          id: id,
          quote_id: quote_id 
      });
  },
  vehicleaccident: function (quote_id, id) {
      App.showPage('VehicleAccident', {
          id: id,
          quote_id: quote_id 
      });
  },
  vehiclebenefits: function (quote_id, id) {
      App.showPage('VehicleBenefits', {
          id: id,
          quote_id: quote_id 
      });
  },
  vehiclediscounts: function (quote_id, id) {
      App.showPage('VehicleDiscounts', {
          id: id,
          quote_id: quote_id 
      });
  },
  vehicles: function (id) {
      App.showPage('Vehicles', {
          id: id,
      });
  },
  insurancetypeandusage: function (quote_id, id) {
      App.showPage('TypeOfPropertyInsuranceAndUsage', {
          id: id,
          quote_id: quote_id 
      });
  },
  immovableaddress: function (quote_id, id) {
      App.showPage('ImmovableAddress', {
          id: id,
          quote_id: quote_id 
      });
  },
  immovablecharacteristics: function (quote_id, id) {
      App.showPage('ImmovableCharacteristics', {
          id: id,
          quote_id: quote_id 
      });
  },
  immovablesecurity: function (quote_id, id) {
      App.showPage('ImmovableSecurity', {
          id: id,
          quote_id: quote_id 
      });
  },
  immovablebenefits: function (quote_id, id) {
      App.showPage('ImmovableBenefits', {
          id: id,
          quote_id: quote_id 
      });
  },
  submitquote: function (id) {
      App.showPage('SubmitQuote', {
          id: id
      });
  },
  quoteconfirmation: function (id) {
      App.showPage('QuoteConfirmation', {
          id: id
      });
  }, 
  
  init: function() {
    Backbone.history.start({
      pushState: App.settings.history.pushState,
      silent: App.settings.history.startSilent
    });
    Backbone.history.isRoutingURL = function(fragment) {
      for (var k in this.handlers)
        if (this.handlers[k].route.test(fragment))
          return true;
      return false;
    };

    var that = this;

    if (Backbone.history && Backbone.history._hasPushState) {
      $(document).on("click", "a", function(evt) {
        if (typeof(evt.ctrlKey) !== 'undefined' && evt.ctrlKey)
          return true;
        var href = $(this).attr("href");
        var protocol = this.protocol + "//";
        href = href.split(App.settings.sitePath).join('');
        href = href.slice(-1) == '/' ? href.slice(0, -1) : href;
        href = href.slice(0, 1) == '/' ? href.slice(1) : href;

        /// trying to find dialog
        for (var k in that.dialogs)
          if (k == href) {
            console.log('Showing "' + that.dialogs[k] + '" dialog from document click event');
            App.showDialog(that.dialogs[k]);

            return false;
          }

          // Ensure the protocol is not part of URL, meaning its relative.
        if (href.slice(protocol.length) !== protocol && Backbone.history.isRoutingURL(href)) {
          console.log('Navigating to "' + href + '" from document click event');
          evt.preventDefault();
          App.router.navigate(href, {
            trigger: true
          });

          return false;
        }

        return true;
      });
    }
  }

}))();