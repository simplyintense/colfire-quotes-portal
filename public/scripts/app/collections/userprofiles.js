//userprofiles.js
App.Collections.UserProfiles = Backbone.Collection.extend({
	model: App.Models.User,
	url: function() {
		return App.settings.apiEntryPoint + 'userprofiles';
	},
});