//quotes.js
App.Collections.Quotes = Backbone.Collection.extend({
	model: App.Models.Quote,
    state: 'loading',
    comparator: function(item) {
      return -item.get("id");
    },
	url: function() {
		return App.settings.apiEntryPoint + 'quotes';
    },
    search: function (opts) {
        var result = this.where(opts);
        var resultCollection = new App.Collections.Quotes(result);

        return resultCollection;
    }
});