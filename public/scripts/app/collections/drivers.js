//drivers.js
App.Collections.Drivers = Backbone.Collection.extend({

    model: App.Models.Driver,
    state: 'loading',
    comparator: function(item) {
      return [item.get("status"), item.get("id") ]
    },
    url: function () {
        return App.settings.apiEntryPoint + this.conntotable_url_name + '/' + this.conntotable_id + '/drivers';
    },
    setConnToTableParams: function (conntotable_url_name, conntotable_id) {
        this.conntotable_id = conntotable_id;
        this.conntotable_url_name = conntotable_url_name;
    },
    initialize: function () {
        this.state = 'loading';
        this.on('request', function () {
            this.state = 'loading';
        }, this);
        this.on('sync', function () {
            this.state = 'ready';
        }, this);
    }
});