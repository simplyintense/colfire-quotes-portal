//car makes.js
App.Collections.CarMakes = Backbone.Collection.extend({

    model: App.Models.CarMakes,
    state: 'loading',
    url: function () {
        return App.settings.apiEntryPoint + 'service-lists/car-makes';
    },
    initialize: function () {
        this.state = 'loading';
        this.on('request', function () {
            this.state = 'loading';
        }, this);
        this.on('sync', function () {
            this.state = 'ready';
        }, this);
    }
});