//car models.js
App.Collections.CarModels = Backbone.Collection.extend({

    model: App.Models.CarModels,
    state: 'loading',
    comparator: function(item) {
      return [ item.get("model_name") ];
    },
    url: function () {
        return App.settings.apiEntryPoint + this.conntotable_url_name + '/' + this.conntotable_id + '/car-models';
    },
    setConnToTableParams: function (conntotable_url_name, conntotable_id) {
        this.conntotable_id = conntotable_id;
        this.conntotable_url_name = conntotable_url_name;
    },
    initialize: function () {
        this.state = 'loading';
        this.on('request', function () {
            this.state = 'loading';
        }, this);
        this.on('sync', function () {
            this.state = 'ready';
        }, this);
    }
});