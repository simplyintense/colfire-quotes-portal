// header.js
App.Views.Header = Backbone.View.extend({

	el: $("#header"),
	events: {},

	render: function() {
		this.setElement($("#header"));

        if (App.currentUser.isAdmin()) {
			console.log('header.js | Rendering for admis');

			$('.header_is_not_admin').hide();
			$('.header_is_admin').show();
		} else {
			console.log('header.js | Rendering for non admins');
			$('.header_is_not_admin').show();
			$('.header_is_admin').hide();
		}

		if (App.currentUser.isSignedIn()) {
			console.log('header.js | Rendering for signed in user');

			$('.header_is_not_signed_in').hide();
			$('.header_is_signed_in').show();
		} else {
			console.log('header.js | Rendering for not signed in user');
			$('.header_is_not_signed_in').show();
			$('.header_is_signed_in').hide();
		}

		$(".menu_category").parent().removeClass('active');
		if (typeof(App.page) !== 'undefined' && App.page && typeof(App.page.category) !== 'undefined') {
			$(".menu_category_" + App.page.category).parent().addClass('active');
		} else {
			$(".menu_category_home").parent().addClass('active');
		}


		console.log('header.js | Header rendered');
		return this;
	}
});