// add_claim.js
App.Views.Dialogs.AddClaim = App.Views.Abstract.Dialog.extend({

    dialogName: 'add_claim',
    additionalScripts: [
        '/vendors/node_modules/moment/min/moment.min.js',
        '/vendors/node_modules/moment-timezone/moment-timezone.js',
        '/vendors/node_modules/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
    ],
	events: {
        "submit form": "onSubmit"
	},
    focusOnInit: '#input_claimdate',
    initialize: function (params) {
        if (typeof (params.conntotable_model) != 'undefined')
            this.conntotable_model = params.conntotable_model;
        else
            throw 'Can not initialize dialog without param.conntotable_model';

        var that = this;
        App.helper.loadAdditionalScripts(that.additionalScripts, function () {
            if (!moment.tz.zone(App.settings.apptimezone.zonename))
                moment.tz.add(App.settings.apptimezone.moment_tz_package);

            moment.tz.setDefault(App.settings.apptimezone.zonename);
            that.on('rendered', function () {
                that.$('#datetimepicker_claimdate').datetimepicker({
                    defaultDate: null,
                    maxDate: moment().add(1, 'days'),
                    format: App.settings.apptimezone.date_format,
                    locale: App.settings.apptimezone.locale,
                });
            });

            that.show();
        });
        
    },
	onSubmit: function() {
		var that = this;
        that.validationError = [];

        this.$('.btn-orange').button('loading');

        var claim_date = this.$('#datetimepicker_claimdate').data("DateTimePicker").date();
        var estimated_value = this.$('#input_estimated_value').val();
        var claims_history_descr = this.$('#input_claims_history_descr').val();

        this.listenTo(that, 'saved', function (item) {
            this.conntotable_model.getClaims().add(item);
            this.hide();
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.btn-orange').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, 2000);
        });

        if (!claim_date)
            that.validationError.push({ msg: 'Please enter Claim Date' });

        if (!estimated_value)
            that.validationError.push({ msg: 'Please enter Claim Estimated Value' });

        if (!claims_history_descr)
            that.validationError.push({ msg: 'Please enter Claim Short Description' });
        

        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            var item = new App.Models.Claim();

            item.set('claim_date', claim_date);
            item.set('estimated_value', estimated_value);
            item.set('claims_history_descr', claims_history_descr);
            item.set('immovable_id', this.conntotable_model.id);
            item.set('quote_id', this.conntotable_model.get('quote_id'));
            

            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null, {
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}

		return false;
	}
});