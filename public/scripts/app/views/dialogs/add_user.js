// inviteuser.js
App.Views.Dialogs.InviteUser = App.Views.Abstract.Dialog.extend({

	dialogName: 'invite_user',
	events: {
		"submit form": "onSubmit"
	},
	focusOnInit: '#input_invite_login',
	initialize: function (params) {
        if (typeof (params.items) != 'undefined')
            this.items = params.items;
        else
            throw 'Can not initialize dialog without param.items';

        this.show();
    },
    onSubmit: function() {
		var that = this;
        that.validationError = [];

        this.$('.btn-orange').button('loading');

        var login = this.$('#input_invite_login').val();
		var email = this.$('#input_invite_email').val();

        this.listenTo(that, 'saved', function (item) {
            this.items.add(item);
            
            this.$('.modal-body-default').slideUp();
			this.$('.modal-body-success').slideDown();
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.btn-orange').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, 2000);
        });

        if (!login)
            that.validationError.push({ msg: 'Please enter login' });

        if (!email)
            that.validationError.push({ msg: 'Please enter email' });


        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            var item = new App.Models.UserInvite();

            item.set('login', login);
            item.set('email', email);
            

            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null, {
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}

		return false;
	}
});