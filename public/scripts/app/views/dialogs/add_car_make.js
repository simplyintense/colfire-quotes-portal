// add_car_make.js
App.Views.Dialogs.AddCarMake = App.Views.Abstract.Dialog.extend({

    dialogName: 'add_car_make',
	events: {
        "submit form": "onSubmit"
	},
    focusOnInit: '#input_car_make',
    initialize: function (params) {
        // if (typeof (params.conntotable_model) != 'undefined')
        //     this.conntotable_model = params.conntotable_model;
        // else
        //     throw 'Can not initialize dialog without param.conntotable_model';

        if (typeof (params.items) != 'undefined')
            this.collection = params.items;
        else
            throw 'Can not initialize dialog without collection';

        this.show();
    },
	onSubmit: function() {
		var that = this;
        that.validationError = [];

        this.$('.btn-orange').button('loading');

        var make_name = this.$('#input_car_make').val().toUpperCase();

        this.listenTo(that, 'saved', function (item) {
            that.collection.add(item);
            that.hide();
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.btn-orange').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, 5000);
        });

        if (!make_name)
            that.validationError.push({ msg: 'Please enter Vehicle Make' });

        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            var item = new App.Models.CarMakes();
            item.set('make_name', make_name);      

            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null, {
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}

		return false;
	}
});