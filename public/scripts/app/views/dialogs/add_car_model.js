// add_car_model.js
App.Views.Dialogs.AddCarModel = App.Views.Abstract.Dialog.extend({

    dialogName: 'add_car_model',
	events: {
        "submit form": "onSubmit"
	},
    focusOnInit: '#input_car_model',
    initialize: function (params) {
        // if (typeof (params.conntotable_model) != 'undefined')
        //     this.conntotable_model = params.conntotable_model;
        // else
        //     throw 'Can not initialize dialog without param.conntotable_model';

        if (typeof (params.items) != 'undefined')
            this.collection = params.items;
        else
            throw 'Can not initialize dialog without collection';


        this.show();
    },
	onSubmit: function() {
		var that = this;
        that.validationError = [];

        this.$('.btn-orange').button('loading');

        var model_name = this.$('#input_car_model').val().toUpperCase();


        this.listenTo(that, 'saved', function (item) {
            that.collection.add(item);
            that.hide();
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.btn-orange').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, 5000);
        });

        if (!model_name)
            that.validationError.push({ msg: 'Please enter Vehicle Model' });


        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            var item = new App.Models.CarModels();
            item.setConnToTableParams('service-lists/car-makes', this.collection.conntotable_id);

            item.set('model_name', model_name);

            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null, {
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}

		return false;
	}
});