// user_details.js
App.Views.Dialogs.UserDetails = App.Views.Abstract.Dialog.extend({

    dialogName: 'user_details',
    additionalScripts: [
        '/vendors/node_modules/moment/min/moment.min.js',
        '/vendors/node_modules/moment-timezone/moment-timezone.js',
    ],
    events: {
        "submit form": "onSubmit",
        "click #remove_user_button": "removeUser",
    },
    //removeUser: function () {
    //    App.showDialog('RemoveUser', {
    //        item: this.item,
    //        items: this.items
    //    });
    //    return false;
    //},
    initialize: function (params) {
        if (typeof (params.item) != 'undefined')
            this.item = params.item;
        else
            throw 'Can not initialize dialog without param.item';
        if (typeof (params.items) != 'undefined')
            this.items = params.items;
        else
            throw 'Can not initialize dialog without param.items';

        var that = this;
        App.helper.loadAdditionalScripts(that.additionalScripts, function () {
            if (!moment.tz.zone(App.settings.apptimezone.zonename))
                moment.tz.add(App.settings.apptimezone.moment_tz_package);
            moment.tz.setDefault(App.settings.apptimezone.zonename);

            that.show({
                item: that.item.toJSON(),
                items: that.items.toJSON(),
            });
        });

    },
    onSubmit: function () {
        var that = this;

        this.$('.btn-orange').button('loading');

        this.hide();

        return false;
    }
});