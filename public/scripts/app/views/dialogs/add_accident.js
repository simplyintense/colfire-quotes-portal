// add_accident.js
App.Views.Dialogs.AddAccident = App.Views.Abstract.Dialog.extend({

    dialogName: 'add_accident',
    additionalScripts: [
        '/vendors/node_modules/moment/min/moment.min.js',
        '/vendors/node_modules/moment-timezone/moment-timezone.js',
        '/vendors/node_modules/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
    ],
	events: {
        "submit form": "onSubmit"
	},
    focusOnInit: '#input_accidentdate',
    initialize: function (params) {
        if (typeof (params.conntotable_model) != 'undefined')
            this.conntotable_model = params.conntotable_model;
        else
            throw 'Can not initialize dialog without param.conntotable_model';

        var that = this;
        App.helper.loadAdditionalScripts(that.additionalScripts, function () {
            if (!moment.tz.zone(App.settings.apptimezone.zonename))
                moment.tz.add(App.settings.apptimezone.moment_tz_package);

            moment.tz.setDefault(App.settings.apptimezone.zonename);
            that.on('rendered', function () {
                that.$('#datetimepicker_accidentdate').datetimepicker({
                    defaultDate: null,
                    maxDate: moment().add(1, 'days'),
                    format: App.settings.apptimezone.date_format,
                    locale: App.settings.apptimezone.locale,
                });
            });

            that.show();
        });
        
    },
	onSubmit: function() {
		var that = this;
        that.validationError = [];

        this.$('.btn-orange').button('loading');

        var accidentdate = this.$('#datetimepicker_accidentdate').data("DateTimePicker").date();
        var you_deemed_responsible = this.$('#you_deemed_responsible').val();
        var accident_cost = this.$('#accident_cost').val();
        var accident_description = this.$('#accident_description').val();

        this.listenTo(that, 'saved', function (item) {
            this.conntotable_model.getAccidents().add(item);
            this.hide();
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.btn-orange').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, 2000);
        });

        if (!accidentdate)
            that.validationError.push({ msg: 'Please enter Accident Date' });

        if (!you_deemed_responsible)
            that.validationError.push({ msg: 'Please advise if you were deemed responsible' });
        
        if (!accident_cost)
            that.validationError.push({ msg: 'Please enter Accident Cost' });
        
        if (!accident_description)
            that.validationError.push({ msg: 'Enter a brief description of the accident' });
        

        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            var item = new App.Models.Accident();

            item.set('accidentdate', accidentdate);
            item.set('you_deemed_responsible', you_deemed_responsible);
            item.set('accident_cost', accident_cost);
            item.set('accident_description', accident_description);
            item.set('vehicle_id', this.conntotable_model.id);
            item.set('quote_id', this.conntotable_model.get('quote_id'));
            

            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null, {
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}

		return false;
	}
});