// add_driver.js
App.Views.Dialogs.AddDriver = App.Views.Abstract.Dialog.extend({

    dialogName: 'add_driver',
	events: {
        "submit form": "onSubmit"
	},
    focusOnInit: '#input_drivername',
    initialize: function (params) {
        if (typeof (params.conntotable_model) != 'undefined')
            this.conntotable_model = params.conntotable_model;
        else
            throw 'Can not initialize dialog without param.conntotable_model';

        this.show({
            profile: App.currentUser.profile.toJSON(),
            drivers: this.collection.sort().toJSON(),
        });
    },
	onSubmit: function() {
		var that = this;
        that.validationError = [];

        this.$('.btn-orange').button('loading');

        var first_name = this.$('#input_firstname').val();
        var last_name = this.$('#input_lastname').val();
        var driving_experience = this.$('#driving_experience').val();
        var young_driver_age = this.$('#young_driver_age').val();
        var new_driver_permit = this.$('#new_driver_permit').val();
        var notes = this.$('#input_notes').val();

        this.listenTo(that, 'saved', function (item) {
            this.conntotable_model.getDrivers().add(item);
            this.hide();
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.btn-orange').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, 2000);
        });

        if (!first_name)
            that.validationError.push({ msg: 'Please enter Driver First Name' });

        if (!last_name)
            that.validationError.push({ msg: 'Please enter Driver Last Name' });

        if (!driving_experience)
            that.validationError.push({ msg: 'Please enter driving experience' });

        if (!young_driver_age)
            that.validationError.push({ msg: 'Is the driver under the age of 25?' });

        if (!new_driver_permit)
            that.validationError.push({ msg: 'Is driver\'s permit less than 24 months old?' });


        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            var item = new App.Models.Driver();

            item.set('firstname', first_name);
            item.set('lastname', last_name);
            item.set('driving_experience', driving_experience);
            item.set('young_driver_age', young_driver_age);
            item.set('new_driver_permit', new_driver_permit);
            item.set('notes', notes);
            item.set('vehicle_id', this.conntotable_model.id);
            item.set('quote_id', this.conntotable_model.get('quote_id'));
            

            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null, {
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}

		return false;
	}
});