// edit_remove_list_item.js
App.Views.Dialogs.RemoveListItem = App.Views.Abstract.Dialog.extend({

	dialogName: 'remove_list_item',
	events: {
		"submit form": "onSubmit",
		"click .process_button": "doProcess"
	},
	initialize: function(params) {
		if (typeof(params.item) != 'undefined')
			this.item = params.item;
		else
			throw 'Can not initialize dialog without param.item';

		this.show({
			item: this.item.toJSON()
		});
	},
	doProcess: function() {
        this.$('.btn-danger').button('loading');

		this.item.destroy();
		this.hide();
	},
	onSubmit: function() {
		this.hide();
		return false;
	}
});