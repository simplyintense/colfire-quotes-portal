// edit_driver.js
App.Views.Dialogs.EditDriver = App.Views.Abstract.Dialog.extend({

    dialogName: 'edit_driver',
	events: {
		"submit form": "onSubmit"
	},
    focusOnInit: '#input_firstname',
	initialize: function(params) {
		if (typeof(params.item) != 'undefined')
			this.item = params.item;
		else
			throw 'Can not initialize dialog without param.item';

        if (typeof (params.conntotable_model) != 'undefined')
            this.conntotable_model = params.conntotable_model;
        else
            throw 'Can not initialize dialog without param.conntotable_model';

        var that = this;
		that.show({
			item: this.item.toJSON()
		});
	},
	onSubmit: function() {
        var that = this;
        that.validationError = [];

        this.$('.btn-orange').button('loading');

        var first_name = this.$('#input_firstname').val();
        var last_name = this.$('#input_lastname').val();
        var driving_experience = this.$('#driving_experience').val();
        var young_driver_age = this.$('#young_driver_age').val();
        var new_driver_permit = this.$('#new_driver_permit').val();
        var notes = this.$('#input_notes').val();
        
        this.listenTo(that, 'saved', function () {
            this.hide();
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.btn-orange').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, 2000);
        });

        if (!first_name)
            that.validationError.push({ msg: 'Please enter Driver First Name' });

        if (!last_name)
            that.validationError.push({ msg: 'Please enter Driver Last Name' });

        if (!driving_experience)
            that.validationError.push({ msg: 'Please enter driving experience' });
        
        if (!young_driver_age)
            that.validationError.push({ msg: 'Is the driver under the age of 25?' });

        if (!new_driver_permit)
            that.validationError.push({ msg: 'Is driver\'s permit less than 24 months old?' });

        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {

            this.item.set('firstname', first_name);
            this.item.set('lastname', last_name);
            this.item.set('driving_experience', driving_experience);
            this.item.set('young_driver_age', young_driver_age);
            this.item.set('new_driver_permit', new_driver_permit);
            this.item.set('notes', notes);
            this.item.set('vehicle_id', this.conntotable_model.id);
            this.item.set('quote_id', this.conntotable_model.get('quote_id'));


            //trigger error if local model validation returns error
            this.listenTo(this.item, 'invalid', function (errors) {
                that.trigger('invalid', errors.validationError);
            });

            this.item.save(null, {
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        console.log("Server side save success");
                        that.trigger('saved');
                    }
                },
                error: function (model, response) {
                    console.log("Server side save error");
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });

		}

		return false;
	}
});