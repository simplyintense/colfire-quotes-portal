// edit_car_make.js
App.Views.Dialogs.EditCarMake = App.Views.Abstract.Dialog.extend({

    dialogName: 'edit_car_make',
	events: {
		"submit form": "onSubmit"
	},
    focusOnInit: '#input_firstname',
	initialize: function(params) {
		if (typeof(params.item) != 'undefined')
			this.item = params.item;
		else
			throw 'Can not initialize dialog without param.item';

        // if (typeof (params.conntotable_model) != 'undefined')
        //     this.conntotable_model = params.conntotable_model;
        // else
        //     throw 'Can not initialize dialog without param.conntotable_model';

        var that = this;
		that.show({
			item: this.item.toJSON()
        });   
	},
	onSubmit: function() {
        var that = this;
        that.validationError = [];

        this.$('.btn-orange').button('loading');

        var make_name = this.$('#input_make_name').val();

        
        this.listenTo(that, 'saved', function () {
            this.hide();
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.btn-orange').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, 2000);
        });

        if (!make_name)
            that.validationError.push({ msg: 'Please enter Vehicle Make' });

        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {

            this.item.set('make_name', make_name);

            //trigger error if local model validation returns error
            this.listenTo(this.item, 'invalid', function (errors) {
                that.trigger('invalid', errors.validationError);
            });

            this.item.save(null, {
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved');
                    }
                },
                error: function (model, response) {
                    console.log("Server side save error");
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });

		}

		return false;
	}
});