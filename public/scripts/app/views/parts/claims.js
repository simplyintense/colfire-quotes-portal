﻿// claims.js
App.Views.Parts.Claims = Backbone.View.extend({
    templateName: 'parts/claims',
    additionalScripts: [
        '/vendors/node_modules/moment/min/moment.min.js',
        '/vendors/node_modules/moment-timezone/moment-timezone.js',
    ],
    el: $("#claims_container"),
    events: {
        "click .item_button_remove": "removeItem",
        "click .item_button_edit": "editItem",
    },
    initialize: function () {
        console.log('views/parts/claims.js | Initializing Claims view');
        if (!this.model || !this.collection)
            console.error('views/parts/claims.js | model && collection && id should be provided for this view');

        var that = this;
        that.on('addscriptloaded', this.render);

        this.listenTo(this.collection, 'sync add change reset remove', function () {
            App.helper.loadAdditionalScripts(that.additionalScripts, function () {
                if (!moment.tz.zone(App.settings.apptimezone.zonename))
                    moment.tz.add(App.settings.apptimezone.moment_tz_package);
                moment.tz.setDefault(App.settings.apptimezone.zonename);

                that.trigger('addscriptloaded');
            });
        });





    },
    wakeUp: function () {
        console.error('views/parts/claims.js | Waking up');
        this.listenTo(this.collection, 'sync add change reset remove', this.render);
    },
    //fadeOut: function () {
    //    this.$('#claims_container').fadeTo(1, 0.5);
    //},
    render: function () {
        console.log('views/parts/claims.js | Rendering, state = ' + this.collection.state);
        this.setElement($('#' + this.id));
        this.$('#claims_container').fadeTo(1, 1);

        var data = {
            state: this.collection.state,
            collection: this.collection,
            claims: this.collection.sort().toJSON(),
            item: this.model.toJSON()
        };
        var that = this;
        App.templateManager.fetch(this.templateName, data, function (html) {

            that.$el.html(html);
            that.trigger('render');
            that.trigger('loaded');
        });
    },
    removeItem: function (ev) {
        var id = $(ev.currentTarget).parents('.item').data('id');
        var item = this.collection.get(id);
        item.set('immovable_id', this.model.id);
        item.set('quote_id', this.model.get('quote_id'));
        item.hide();
        return false;
    },
    editItem: function (ev) {
        var id = $(ev.currentTarget).parents('.item').data('id');
        App.showDialog('EditClaim', {
            item: this.collection.get(id),
            conntotable_model: this.model,
        });

        return false;
    },
});