﻿// vehicles.js
App.Views.Parts.Vehicles = Backbone.View.extend({
    templateName: 'parts/vehicles',
    el: $("#vehicles_container"),
    events: {
        "click .item_button_remove": "removeItem",
        "click .item_button_edit": "editItem",
    },
    initialize: function () {
        console.log('views/parts/vehicles.js | Initializing Vehicles view');
        if (!this.model || !this.collection)
            console.error('views/parts/vehicles.js | model && collection && id should be provided for this view');

        this.listenTo(this.collection, 'sync', this.render);
    },
    wakeUp: function () {
        console.error('views/parts/vehicles.js | Waking up');
        this.listenTo(this.collection, 'sync add change reset remove', this.render);
        this.collection.fetch();
    },
    //fadeOut: function () {
    //    this.$('#vehicles_container').fadeTo(1, 0.5);
    //},
    render: function () {
        console.log('views/parts/vehicles.js | Rendering, state = ' + this.collection.state);
        this.setElement($('#' + this.id));
        this.$('#vehicles_container').fadeTo(1, 1);

        var data = {
            state: this.collection.state,
            collection: this.collection,
            vehicles: this.collection.sort().toJSON(),
            item: this.model.toJSON()
        };
        var that = this;
        App.templateManager.fetch(this.templateName, data, function (html) {

            that.$el.html(html);
            that.trigger('render');
            that.trigger('loaded');
        });
    },
    removeItem: function (ev) {
        var id = $(ev.currentTarget).parents('.item').data('id');
        var item = this.collection.get(id);
        item.hide();

        return false;
    },
    editItem: function (ev) {
        var id = $(ev.currentTarget).parents('.item').data('id');
        App.showPage('VehicleMakeModel', {
            item: this.collection.get(id),
        });

        return false;
    },
});