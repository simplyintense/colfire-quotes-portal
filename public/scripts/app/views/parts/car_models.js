﻿// car models.js
App.Views.Parts.CarModels = Backbone.View.extend({
    templateName: 'parts/car_models',
    el: $("#" + this.id),
    events: {
        "change #input_model": "modelSelected",
    },
    initialize: function (params) {
        console.log('views/parts/car_models.js | Initializing Drop-Down Car Models as view');

        if (!this.model || !this.collection)
            console.error('views/parts/car_models.js | model && collection && id should be provided for this view');

        this.makesCollection = params.makesCollection;

        this.listenTo(this.collection, 'sync', function() {
            this.render();
        });
        
        this.collection.setConnToTableParams('service-lists/car-makes', this.model.get('make') || 5);

        this.listenTo(this.model, 'change', function() {
            if ( _.has(this.model.changed, 'make') ) {
                this.retrieveModelList();
            }
        });

        this.collection.fetch({
            // data: activeCollection.querySettings, 
            reset: true,
            success: function (collection, response, options) {
                // you can pass additional options to the event you trigger here as well
                //that.trigger('successOnFetch');
            },
            error: function (collection, response, options) {
                // you can pass additional options to the event you trigger here as well
                //that.trigger('errorOnFetch');
                console.error('Error on collection fetch');
            } 
        });
    },
    wakeUp: function () {
        console.error('views/parts/car_models.js | Waking up');
        this.listenTo(this.model, 'change', function() {
            this.retrieveModelList();
        });
        this.listenTo(this.collection, 'sync', this.render); //'sync add change update reset remove'
    },
    render: function () {
        console.log('views/parts/car_models.js | Rendering, state = ' + this.collection.state);
        this.setElement($('#' + this.id));
        this.$('#input_model_form_group').fadeTo(1, 1);

        this.on('loaded', function () {
            $('#input_model_form_group').validator('update');
        });

        var data = {
            state: this.collection.state,
            item: this.model.attributes,
            models_list: _.sortBy(this.collection.models, function (model_name) { return model_name; }),
        };

        var that = this;
        App.templateManager.fetch(this.templateName, data, function (html) {

            that.$el.html(html);
            that.trigger('render');
            that.trigger('loaded');
            that.model.trigger('loaded');
        });
    },
    modelSelected: function () {
        var vehicle_model = this.$('#input_model').val();
        if (vehicle_model == '') vehicle_model = null;

        this.model.set({ 'model': vehicle_model });

        this.render();
    },
    retrieveModelList: function () {
        var vehicle_make = this.model.get('make_id');
    
        if ( _.has(this.model.changed, 'make') ) {
            $("#input_model_form_group").removeClass('has-success').addClass('has-error has-danger');
            if (vehicle_make == null) {
                // this.render(); //render here does not showing when model is in loading status. need render for both IF stat
            } else {
                this.collection.setConnToTableParams('service-lists/car-makes/', vehicle_make);
                this.collection.fetch();
            }
        }
        this.render();
    }
});