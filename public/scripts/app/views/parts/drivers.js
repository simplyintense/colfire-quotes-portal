﻿// drivers.js
App.Views.Parts.Drivers = Backbone.View.extend({
    templateName: 'parts/drivers',
    el: $("#drivers_container"),
    events: {
        "click .item_button_remove": "removeItem",
        "click .item_button_edit": "editItem",
    },
    initialize: function () {
        console.log('views/parts/drivers.js | Initializing Drivers view');
        if (!this.model || !this.collection)
            console.error('views/parts/drivers.js | model && collection && id should be provided for this view');

        this.listenTo(this.collection, 'sync', this.render);
    },
    wakeUp: function () {
        console.error('views/parts/drivers.js | Waking up');
        this.listenTo(this.collection, 'sync add update reset remove', this.render);
    },
    //fadeOut: function () {
    //    this.$('#drivers_container').fadeTo(1, 0.5);
    //},
    render: function () {
        console.log('views/parts/drivers.js | Rendering, state = ' + this.collection.state);
        this.setElement($('#' + this.id));
        this.$('#drivers_container').fadeTo(1, 1);

        var data = {
            state: this.collection.state,
            collection: this.collection,
            drivers: this.collection.sort().toJSON(),
            item: this.model.toJSON()
        };
        var that = this;
        App.templateManager.fetch(this.templateName, data, function (html) {

            that.$el.html(html);
            that.trigger('render');
            that.trigger('loaded');
        });
    },
    removeItem: function (ev) {
        var id = $(ev.currentTarget).parents('.item').data('id');
        var item = this.collection.get(id);
        item.set('vehicle_id', this.model.id);
        item.set('quote_id', this.model.get('quote_id'));
        item.hide();
        return false;
    },
    editItem: function (ev) {
        var id = $(ev.currentTarget).parents('.item').data('id');
        App.showDialog('EditDriver', {
            item: this.collection.get(id),
            conntotable_model: this.model,
        });

        return false;
    },
});