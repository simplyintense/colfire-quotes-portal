﻿// car makes.js
App.Views.Parts.CarMakes = Backbone.View.extend({
    templateName: 'parts/car_makes',
    el: $("#" + this.id),
    events: {
        "change #input_make": "makeSelected",
    },
    initialize: function () {
        console.log('views/parts/car_makes.js | Initializing Drop-Down Car Makes as view');

        if (!this.model || !this.collection)
            console.error('views/parts/car_makes.js | model && collection && id should be provided for this view');

        this.listenTo(this.collection, 'sync', function() {
            this.render();
        });

        this.collection.fetch({
            // data: activeCollection.querySettings, 
            reset: true,
            success: function (collection, response, options) {
                // you can pass additional options to the event you trigger here as well
                //that.trigger('successOnFetch');
            },
            error: function (collection, response, options) {
                // you can pass additional options to the event you trigger here as well
                //that.trigger('errorOnFetch');
                console.error('Error on collection fetch');
            } 
        });
    },
    wakeUp: function () {
        console.error('views/parts/car_makes.js | Waking up');
        this.listenTo(this.collection, 'sync', this.render); //'sync add change update reset remove'
    },
    render: function () {
        console.log('views/parts/car_makes.js | Rendering, state = ' + this.collection.state);
        this.setElement($('#' + this.id));
        this.$('#input_make_form_group').fadeTo(1, 1);

        this.on('loaded', function () {
            $('#input_make_form_group').validator('update');
        });

        var data = {
            state: this.collection.state,
            item: this.model.attributes,
            makes_list: _.sortBy(this.collection.models, "make_name"),
        };

        var that = this;
        App.templateManager.fetch(this.templateName, data, function (html) {

            that.$el.html(html);
            that.trigger('render');
            that.trigger('loaded');
        });
    },
    makeSelected: function () {
        var make_id = this.$('#input_make').find('option:selected').attr('data-id');
        var make_name = this.$('#input_make').find('option:selected').attr('value');

        if (make_id) {
            this.model.set({'make': make_name, 'make_id': make_id, 'model': null });
        } else {
            this.model.set({ 'make': null, 'make_id': null, 'model': null });
            $("#input_make_form_group").removeClass('has-success').addClass('has-error has-danger');
        }

        $('#input_make_form_group').validator('update');
    },
});