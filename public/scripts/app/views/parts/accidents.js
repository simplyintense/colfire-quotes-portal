﻿// accidents.js
App.Views.Parts.Accidents = Backbone.View.extend({
    templateName: 'parts/accidents',
    additionalScripts: [
        '/vendors/node_modules/moment/min/moment.min.js',
        '/vendors/node_modules/moment-timezone/moment-timezone.js',
    ],
    el: $("#accidents_container"),
    events: {
        "click .item_button_remove": "removeItem",
        "click .item_button_edit": "editItem",
    },
    initialize: function () {
        console.log('views/parts/accidents.js | Initializing Accidents view');
        if (!this.model || !this.collection)
            console.error('views/parts/accidents.js | model && collection && id should be provided for this view');

        var that = this;
        that.on('addscriptloaded', this.render);

        this.listenTo(this.collection, 'sync add change reset remove', function () {
            App.helper.loadAdditionalScripts(that.additionalScripts, function () {
                if (!moment.tz.zone(App.settings.apptimezone.zonename))
                    moment.tz.add(App.settings.apptimezone.moment_tz_package);
                moment.tz.setDefault(App.settings.apptimezone.zonename);

                that.trigger('addscriptloaded');
            });
        });





    },
    wakeUp: function () {
        console.error('views/parts/accidents.js | Waking up');
        this.listenTo(this.collection, 'sync add change update reset remove', this.render);
    },
    //fadeOut: function () {
    //    this.$('#accidents_container').fadeTo(1, 0.5);
    //},
    render: function () {
        console.log('views/parts/accidents.js | Rendering, state = ' + this.collection.state);
        this.setElement($('#' + this.id));
        this.$('#accidents_container').fadeTo(1, 1);

        var data = {
            state: this.collection.state,
            collection: this.collection,
            accidents: this.collection.sort().toJSON(),
            item: this.model.toJSON()
        };
        var that = this;
        App.templateManager.fetch(this.templateName, data, function (html) {

            that.$el.html(html);
            that.trigger('render');
            that.trigger('loaded');
        });
    },
    removeItem: function (ev) {
        var id = $(ev.currentTarget).parents('.item').data('id');
        var item = this.collection.get(id);
        item.set('vehicle_id', this.model.id);
        item.set('quote_id', this.model.get('quote_id'));
        item.hide();
        return false;
    },
    editItem: function (ev) {
        var id = $(ev.currentTarget).parents('.item').data('id');
        App.showDialog('EditAccident', {
            item: this.collection.get(id),
            conntotable_model: this.model,
        });

        return false;
    },
});