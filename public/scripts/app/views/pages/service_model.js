﻿//service model.js
App.Views.Pages.ServiceModel = App.Views.Abstract.Page.extend({

    templateName: 'pages/service/service_model',
    additionalScripts: [
        '/vendors/node_modules/moment/min/moment.min.js',
        '/vendors/node_modules/moment-timezone/moment-timezone.js',
    ],
    category: 'service model',
    events: {
        "mouseenter .item": "moreMakeDetails",
        "mouseleave .item": "lessMakeDetails",
        "click .item": "toItem",
        "click .item_button_edit": "editItem",
        "click #add_model_small_button": "addModel",
        "click .item_button_remove": "removeItem",
        "click #search_model_button": "searchModel",
        "click #reset_model_search_button": "reset_searchModel",
        "click .toggle-btn": "toggleChevron",
    },
    title: function () {
        return 'Manage Vehicle Models List';
    },
    // url: function () {
    //     if (_.has(this.options, "make_id"))
    //         return 'service/make/5/model/';
    //         // return 'service/make/' + this.options.make_id + '/model/';
    // },
    url: function () {
        if (typeof (this.collection) != 'undefined' && this.collection.make_id)
            return 'service/make/' + this.collection.make_id + '/model';
    },
    render: function () {
        console.log('views/pages/service model.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });


        this.renderHTML({
            items: _.sortBy(this.collection.toJSON(), "model_name"),
            make_id: this.collection.make_id,
            //item: this.model.toJSON(),
            search_params: this.model_search_params,
        });

        $('.helpericon').hide();
    },
    initializeParts: function () {
        console.info('views/pages/service model.js | initializing parts');
        this.parts = [];
        this.partsInitialized = true;

        this.charts = [];
    },
    wakeUp: function () {
        console.log('views/pages/service model.js | waking up');
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            if (App.currentUser.isDemo())
                window.location.pathname = '/signin';

            that.render();
            that.listenTo(that.collection, 'sync add change reset remove', that.render);
        
            that.reset_searchModel();
        });
    },
    initialize: function (params) {
        console.log('service model.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function () {
            if (App.currentUser.isDemo())
                window.location.pathname = '/signin';


                if (typeof (params.item) !== 'undefined') {
                    that.collection = new App.Collections.CarModels();
                    that.collection.make_id = params.item.id;
                    that.collection.setConnToTableParams('service-lists/car-makes', params.item.id);
    
                    that.on('addscriptloaded', that.render);
                    App.helper.loadAdditionalScripts(that.additionalScripts, function () {
                        if (!moment.tz.zone(App.settings.apptimezone.zonename))
                            moment.tz.add(App.settings.apptimezone.moment_tz_package);
                        moment.tz.setDefault(App.settings.apptimezone.zonename);
    
                        that.trigger('addscriptloaded');
                    });
                    that.listenTo(that.collection, 'change sync destroy', that.render);
                    
                    that.collection.fetch({
                        error: function () {
                            App.showPage('NotFound');
                        }
                    });
    
                } else if (typeof (params.id) !== 'undefined') {
                    that.collection = new App.Collections.CarModels();
                    that.collection.make_id = params.id;
                    that.collection.setConnToTableParams('service-lists/car-makes', params.id);
    
                    //that.listenTo(that.collection, 'change sync destroy', that.render);
                    that.on('addscriptloaded', that.render);
                    that.listenTo(that.collection, 'change sync destroy', function () {
                        App.helper.loadAdditionalScripts(that.additionalScripts, function () {
                            if (!moment.tz.zone(App.settings.apptimezone.zonename))
                                moment.tz.add(App.settings.apptimezone.moment_tz_package);
                            moment.tz.setDefault(App.settings.apptimezone.zonename);
    
                            that.trigger('addscriptloaded');
                        });
                    });
    
                    that.collection.fetch({
                        error: function () {
                            App.showPage('NotFound');
                        }
                    });
                } else
                    throw 'id or item parameters required';
        });
    },
    moreMakeDetails: function (ev) {
        $(ev.currentTarget).find(".item_buttons").show();
    },
    lessMakeDetails: function (ev) {
        $(ev.currentTarget).find(".item_buttons").hide();
    },
    addModel: function(){
        App.showDialog('AddCarModel',{
            items: this.collection,
        });

        return false;
    },
    toggleChevron: function (ev) {
        ev.preventDefault();
        ev.stopImmediatePropagation();
        $($(ev.currentTarget).attr('href')).toggleClass('hidden'); //$($(ev.currentTarget).attr('href')).toggleClass('in');
        $(ev.currentTarget).children('span').first().toggleClass("glyphicon glyphicon-chevron-down");
        $(ev.currentTarget).children('span').first().toggleClass("glyphicon glyphicon-chevron-up");
    },
    toItem: function (ev) {
        ev.preventDefault();
        ev.stopImmediatePropagation();
    },
    editItem: function (ev) {
        console.log('views/service.js | Show Edit Vehicle Make Dalog');
        ev.preventDefault();
        ev.stopImmediatePropagation();
        
        var id = $(ev.currentTarget).parents('.item').data('id');
        var item = this.collection.get(id);

        App.showDialog('EditCarModel', {
            item: item
        });

        return false;
    },
    removeItem: function (ev) {
        ev.preventDefault();
        ev.stopImmediatePropagation();

        var id = $(ev.currentTarget).parents('.item').data('id');
        var item = this.collection.get(id);
        item.setConnToTableParams('service-lists/car-makes', this.collection.make_id);

        App.showDialog('RemoveListItem', {
            item: item
        });

        return false;
    },
    searchModel: function (ev) {

        var car_model = this.$('#input_model').val();
        var data = {
            car_model: car_model,
            car_make: this.collection.conntotable_id
        };

        this.model_search_params = {
            car_model: car_model,
            car_make: this.collection.conntotable_id
        };

        this.filterCustomised(
            data
        );

        return false;
    },
    reset_searchModel: function () {
        this.model_search_params = {
            car_model: '',
            car_make: ''
        };

        this.filterCustomised({
            data: {
                car_model: '',
                car_make: ''
            }
        });
    },
    filterCustomised: function (params) {
        console.log('service model.js | customized filter applied');

        params = params || {};

        this.renderLoading();
        this.collection.fetch({ data: params, reset: true });
    },

});