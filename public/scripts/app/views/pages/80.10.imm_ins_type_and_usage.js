﻿//typeofpropertyinsuranceandusage.js
App.Views.Pages.TypeOfPropertyInsuranceAndUsage = App.Views.Abstract.Page.extend({

    templateName: 'pages/quote/immovable/insurancetypeandusage',
    category: 'typeofpropertyinsuranceandusage',
    events: {
        "submit form": "onSubmit",
        "change #input_insurance_type": "insuranceTypeChanged",
    },
    title: function () {
        return 'Insurance Type and Usage';
    },
    url: function () {
        if (typeof (this.model) != 'undefined' && this.model.id)
            return 'quotes/' + this.model.attributes.quote_id + '/immovables/' + this.model.id + '/insurancetypeandusage';
    },
    render: function () {
        console.log('views/pages/typeofpropertyinsuranceandusage.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.on('render', function () {
            $('.inpage-form').validator();
        });

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });
        
        this.renderHTML({
            item: this.model.toJSON()
        });

        $('.helpericon').show();
    },
    initializeParts: function () {
        console.info('views/pages/typeofpropertyinsuranceandusage.js | initializing parts');
        this.parts = [];
        this.partsInitialized = true;
        this.charts = [];
    },
    wakeUp: function () {
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            that.render();
        });
    },
    initialize: function (params) {
        console.log('typeofpropertyinsuranceandusage.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function (user) {


            if (typeof (params.item) !== 'undefined') {
                that.model = params.item;
                that.render();
                that.listenTo(that.model, 'sync', that.render);
            } else if (typeof (params.id) !== 'undefined' && typeof (params.quote_id) !== 'undefined' ) {
                that.model = new App.Models.Immovable();
                that.model.id = params.id;
                that.model.attributes.quote_id = params.quote_id;

                that.listenTo(that.model, 'sync', that.render);
            
                that.model.fetch({
                    error: function () {
                        App.showPage('NotFound');
                    }
                });
            } else
                throw 'id or item parameters required';

        });

    },
    onSubmit: function(){
		var that = this;
        that.validationError = [];
        
        if(this.$('.next-step').hasClass('disabled'))
            return false;

        this.$('.next-step').button('loading');

        var insurance_type = this.$('#input_insurance_type').val();
        var building_value = this.$('#input_building_value').val() || null;
        var contents_value = this.$('#input_contents_value').val() || null;
        var usage_type = this.$('#input_usage_type').val();
        
        this.listenTo(that, 'saved', function (item) {
            //that.render;/
            //navigate next step
            console.info('views/typeofpropertyinsuranceandusage.js | goto next step');
            App.showPage('ImmovableAddress', {
                item: item
            });
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.next-step').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, App.settings.errTimeout);
        });

        if (!insurance_type)
            that.validationError.push({ msg: 'Please select insurance type' });

        if ( (insurance_type == 'building_only' || insurance_type == 'building_and_contents' ) && !building_value)
            that.validationError.push({ msg: 'Please enter the value of the Building you would like to insure' });

        if ( (insurance_type == 'contents_only' || insurance_type == 'building_and_contents' ) &&!contents_value)
            that.validationError.push({ msg: 'Please enter the value of the Contents you would like to insure' });

        if (!usage_type)
            that.validationError.push({ msg: 'Please select property usage type' });
        

        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            var item = this.model;

            item.set('insurance_type', insurance_type);
            item.set('building_value', building_value);
            item.set('contents_value', contents_value);
            item.set('usage_type', usage_type);

            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null,{
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}
        
		return false;
	},
    insuranceTypeChanged: function (ev) {
        var insurance_type = this.$('#input_insurance_type').val() || '';
        if (insurance_type == 'building_and_contents') {
            this.$('.building_value_form_group').fadeIn();
            this.$('.contents_value_form_group').fadeIn();
        } else if (insurance_type == 'building_only') {
            this.$('.building_value_form_group').fadeIn();
            this.$('.contents_value_form_group').fadeOut();
        } else if (insurance_type == 'contents_only') {
            this.$('.building_value_form_group').fadeOut();
            this.$('.contents_value_form_group').fadeIn();
        } else {
            this.$('.building_value_form_group').fadeOut();
            this.$('.contents_value_form_group').fadeOut();
        }
    },

});