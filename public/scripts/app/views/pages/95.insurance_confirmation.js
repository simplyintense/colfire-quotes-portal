﻿//quoteconfirmation.js
App.Views.Pages.QuoteConfirmation = App.Views.Abstract.Page.extend({

    templateName: 'pages/quote/quoteconfirmation',
    category: 'quoteconfirmation',
    events: {
        "click #submit_rating": "submitRating",
        "click #service_rating i": "setRating",
    },
    title: function () {
        return 'Confirmation';
    },
    url: function () {
        if (typeof (this.model) != 'undefined' && this.model.id) {
            if (this.model.get('insurance_type') == 'motor')
                return 'quotes/' + this.model.id + '/motor-confirmation';

            if (this.model.get('insurance_type') == 'property')
                return 'quotes/' + this.model.id + '/property-confirmation';
        }
            
    },
    render: function () {
        console.log('views/pages/quoteconfirmation.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();
        
        this.on('render', function () {
            $('.inpage-form').validator();
        });

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });
        
        this.renderHTML({
            profile: App.currentUser.profile.toJSON(),
            item: this.model.toJSON()
        });

        $('.helpericon').hide();
    },
    initializeParts: function () {
        console.info('views/pages/quoteconfirmation.js | initializing parts');
        this.parts = [];

        this.listenTo(App.currentUser.getProfile(), 'sync',this.render);

        this.partsInitialized = true;
        this.charts = [];
    },
    wakeUp: function () {
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            that.render();
        });
    },
    initialize: function (params) {
        console.log('quoteconfirmation.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function (user) {
            if (typeof (params.item) !== 'undefined') {
                that.model = params.item;
                that.render();
                that.listenTo(that.model, 'sync', that.render);
            } else if (typeof (params.id) !== 'undefined') {
                that.model = new App.Models.Quote();
                that.model.id = params.id;

                that.listenTo(that.model, 'sync', that.render);
            
                that.model.fetch({
                    error: function () {
                        App.showPage('NotFound');
                    }
                });
            } else
                throw 'id or item parameters required';
        });

    },
    setRating: function (e) {
        var oldRateId = $('#input_rating').val();
        var rateId = $(e.target).data('id');
        

        for (k = 1; k <= 5; k++) {
            $('#star_' + k).removeClass('fa-star');
            $('#star_' + k).addClass('fa-star-o');
        } 

        if (oldRateId == 1) {
            $('#input_rating').val(0);
            $('#submit_rating_form_group').fadeOut();
            return;
        } else {
            $('#input_rating').val(rateId);
            $('#submit_rating_form_group').fadeIn();
        }

        for (k = 1; k <= rateId; k++) {
            $('#star_' + k).removeClass('fa-star-o');
            $('#star_' + k).addClass('fa-star');
        } 

        
    },
    submitRating: function (e) {
        e.preventDefault();
		var that = this;
        that.validationError = [];
        
        //if(this.$('.next-step').hasClass('disabled'))
        //    return false;

        this.$('#submit_rating').button('loading');

        //var status = this.model.get('status');
        var rating = $('#input_rating').val();
        var ratingFeedback = $('#rating_description').val();
                
        this.listenTo(that, 'saved', function (item) {
            console.info('views/quoteconfirmation.js | rating accepted');
            //$('.success-container').html('Thank you! Your feedback accepted.');
            //$('.success-container').fadeIn();
            //setTimeout(function () {
            //    $('.success-container').fadeOut();
            //}, 5000);
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.next-step').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, App.settings.errTimeout);
        });

        if (rating == 0)
            that.validationError.push({ msg: 'Please set rating from one to five stars.' });
        

        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            var item = this.model;
            
            item.set('rating', rating);
            item.set('ratingFeedback', ratingFeedback);

            
            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null, {
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}
        
		return false;
	}

});