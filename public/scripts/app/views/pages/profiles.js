﻿//profiles.js
App.Views.Pages.Profiles = App.Views.Abstract.Page.extend({

    templateName: 'pages/profile/profiles',
    additionalScripts: [
        '/vendors/node_modules/moment/min/moment.min.js',
        '/vendors/node_modules/moment-timezone/moment-timezone.js',
    ],
    category: 'profiles',
    events: {
        "click .item": "itemDetails",
        "click #add_user_button": "addUser",
        "click #search_users_button": "searchUsers",
        "click #reset_users_search_button": "reset_searchUsers"
    },
    title: function () {
        return 'Profiles';
    },
    url: function () {
        return 'profiles';
    },
    render: function () {
        console.log('views/pages/profiles.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });

        this.renderHTML({
            items: this.items.toJSON(),
            search_params: this.user_search_params,
        });

        $('.helpericon').hide();
    },
    initializeParts: function () {
        console.info('views/pages/profiles.js | initializing parts');
        this.parts = [];
        this.partsInitialized = true;

        this.charts = [];
    },
    wakeUp: function () {
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            if (App.currentUser.isDemo())
                window.location.pathname = '/signin';

            that.render();
            that.listenTo(that.items, 'sync add change reset remove', that.render);
        
            that.reset_searchUsers();
        });
    },
    initialize: function () {
        console.log('profiles.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function () {
            if (App.currentUser.isDemo())
                window.location.pathname = '/signin';

            that.items = new App.Collections.UserProfiles();

            // initialize models, collections etc. Request fetching from storage
            //that.listenTo(that.items, 'sync', that.render);
            that.on('addscriptloaded', that.render);
            that.listenTo(that.items, 'sync add remove', function () {
                App.helper.loadAdditionalScripts(that.additionalScripts, function () {
                    if (!moment.tz.zone(App.settings.apptimezone.zonename))
                        moment.tz.add(App.settings.apptimezone.moment_tz_package);
                    moment.tz.setDefault(App.settings.apptimezone.zonename);
                    that.trigger('addscriptloaded');
                });
            });

            that.items.fetch({ 
                data: { is_admin: 1 }
            }).done(function () {
                that.listenTo(that.items, 'add change reset remove', that.render);
            }).catch(function (error) {
                console.error(error);
                that.render();
            });
        });
    },
    addUser: function(){
        App.showDialog('InviteUser',{
            items: this.items,
        });

        return false;
    },
    itemDetails: function (ev) {
        console.log('views/profiles.js | Show user details');
        var data = $(ev.currentTarget).data();
        if (typeof (data.id) === 'undefined')
            return true;

        var id = parseInt(data.id, 10);
        var item = this.items.get(id);

        if (!item)
            return true;

        App.showDialog('UserDetails', {
            item: item,
            items: this.items
        });

        return false;
    },
    searchUsers: function (ev) {
        this.user_search_params = this.user_search_params || {};
        
        var login = this.$('#input_login').val();
        var email = this.$('#input_email').val();

        this.filterCustomised({
            data: {
                is_admin: 1,
                login: login,
                email: email,
            }
        });
        return false;
    },
    reset_searchUsers: function () {
        this.filterCustomised({
            data: {
                is_admin: 1,
            }
        });
    },
    filterCustomised: function (params) {
        console.log('profiles.js | customized filter applied');

        var params = params || {};
        this.renderLoading();
        this.items.fetch(params);
        this.user_search_params = params.data || {};

    },

});