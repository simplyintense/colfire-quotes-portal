﻿//vehiclebenefits.js
App.Views.Pages.VehicleBenefits = App.Views.Abstract.Page.extend({

    templateName: 'pages/quote/motor/benefits',
    category: 'vehiclebenefits',
    events: {
        'change #windscreen_coverage': 'changedWindscreenCoverage',
        'change #windscreen_coverage_3rd_party': 'changedWindscreenCoverage3rdParty',
        'change #windscreen_coverage_3rd_party_fire': 'changedWindscreenCoverage3rdPartyFire',
        "submit form": "onSubmit",
    },
    title: function () {
        return 'Benefits';
    },
    url: function () {
        if (typeof (this.model) != 'undefined' && this.model.id)
            return 'quotes/' + this.model.attributes.quote_id + '/vehicles/' + this.model.id + '/benefits';
    },
    render: function () {
        console.log('views/pages/vehiclebenefits.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.on('render', function () {
            $('.inpage-form').validator();
            //$('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="tooltip"]').tooltip({
               container: 'body'
            });
        });

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });
        
        this.renderHTML({
            item: this.model.toJSON(),
            settings: App.settings, 
            years_list: App.settings.car_years_list()
        });

        $('.helpericon').show();

    },
    initializeParts: function () {
        console.info('views/pages/vehiclebenefits.js | initializing parts');
        this.parts = [];
        this.partsInitialized = true;
        this.charts = [];
    },
    wakeUp: function () {
        console.log('views/pages/vehiclebenefits.js | waking up');
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            that.render();
            that.listenTo(that.model, 'change sync destroy', that.render);
            for (var k in that.parts)
                if (typeof (that.parts[k].wakeUp) === 'function')
                    that.parts[k].wakeUp();
            
            that.model.fetch();
        });
    },
    initialize: function (params) {
        console.log('vehiclebenefits.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function (user) {


            if (typeof (params.item) !== 'undefined') {
                that.model = params.item;
                that.render();
                that.listenTo(that.model, 'sync', that.render);
            } else if (typeof (params.id) !== 'undefined' && typeof (params.quote_id) !== 'undefined' ) {
                that.model = new App.Models.Vehicle();
                that.model.id = params.id;
                that.model.attributes.quote_id = params.quote_id;

                that.listenTo(that.model, 'sync', that.render);
            
                that.model.fetch({
                    error: function () {
                        App.showPage('NotFound');
                    }
                });
            } else
                throw 'id or item parameters required';

        });

    },
    changedWindscreenCoverage: function(ev){
        var windscreen_coverage =  (this.$('#windscreen_coverage').is(":checked") ? 'yes':'no');
        if(windscreen_coverage == 'yes'){
            this.$('.windscreen_coverage_amount_form_group').fadeIn();
            this.$('#input_windscreen_coverage_amount').prop('required', true).trigger('focusin');
        }else{
            this.$('#input_windscreen_coverage_amount').prop('required', false).trigger('focusout');
            this.$('.windscreen_coverage_amount_form_group').fadeOut();
        }
    },
    changedWindscreenCoverage3rdParty: function (ev) {
        var windscreen_coverage_3rd_party = (this.$('#windscreen_coverage_3rd_party').is(":checked") ? 'yes' : 'no');
        if (windscreen_coverage_3rd_party == 'yes') {
            this.$('.windscreen_coverage_amount_3rd_party_form_group').fadeIn();
            this.$('#input_windscreen_coverage_amount_3rd_party').prop('required', true).trigger('focusin');
        } else {
            this.$('#input_windscreen_coverage_amount_3rd_party').prop('required', false).trigger('focusout');
            this.$('.windscreen_coverage_amount_3rd_party_form_group').fadeOut();
        }
    },
    changedWindscreenCoverage3rdPartyFire: function (ev) {
        var windscreen_coverage_3rd_party_fire = (this.$('#windscreen_coverage_3rd_party_fire').is(":checked") ? 'yes' : 'no');
        if (windscreen_coverage_3rd_party_fire == 'yes') {
            this.$('.windscreen_coverage_amount_3rd_party_fire_form_group').fadeIn();
            this.$('#input_windscreen_coverage_amount_3rd_party_fire').prop('required', true).trigger('focusin');
        } else {
            this.$('#input_windscreen_coverage_amount_3rd_party_fire').prop('required', true).trigger('focusout');
            this.$('.windscreen_coverage_amount_3rd_party_fire_form_group').fadeOut();
        }
    },
    onSubmit: function(){
		var that = this;
        that.validationError = [];
        
        if(this.$('.next-step').hasClass('disabled'))
            return false;

        this.$('.next-step').button('loading');

        var windscreen_coverage =  (this.$('#windscreen_coverage').is(":checked") ? 'yes':'no');
        var windscreen_coverage_amount = this.$('#input_windscreen_coverage_amount').val() || 0;
        var emergency_roadside_assist = (this.$('#emergency_roadside_assist').is(":checked") ? 'yes' : 'no');

        var windscreen_coverage_3rd_party = (this.$('#windscreen_coverage_3rd_party').is(":checked") ? 'yes' : 'no');
        var windscreen_coverage_amount_3rd_party = this.$('#input_windscreen_coverage_amount_3rd_party').val() || 0;
        var emergency_roadside_assist_3rd_party = (this.$('#emergency_roadside_assist_3rd_party').is(":checked") ? 'yes' : 'no');

        var windscreen_coverage_3rd_party_fire = (this.$('#windscreen_coverage_3rd_party_fire').is(":checked") ? 'yes' : 'no');
        var windscreen_coverage_amount_3rd_party_fire = this.$('#input_windscreen_coverage_amount_3rd_party_fire').val() || 0;
        var emergency_roadside_assist_3rd_party_fire = (this.$('#emergency_roadside_assist_3rd_party_fire').is(":checked") ? 'yes' : 'no');

        if (typeof windscreen_coverage_amount === 'string' || windscreen_coverage_amount instanceof String)
            windscreen_coverage_amount = windscreen_coverage_amount.replace(/[^0-9.]/g, "");

        if (typeof windscreen_coverage_amount_3rd_party === 'string' || windscreen_coverage_amount_3rd_party instanceof String)
            windscreen_coverage_amount_3rd_party = windscreen_coverage_amount_3rd_party.replace(/[^0-9.]/g, "");

        if (typeof windscreen_coverage_amount_3rd_party_fire === 'string' || windscreen_coverage_amount_3rd_party_fire instanceof String)
            windscreen_coverage_amount_3rd_party_fire = windscreen_coverage_amount_3rd_party_fire.replace(/[^0-9.]/g, "");

        var flood_special_perils =  (this.$('#flood_special_perils').is(":checked") ? 'yes':'no');
        var partial_waiver_of_excess =  (this.$('#partial_waiver_of_excess').is(":checked") ? 'yes':'no');
        var loss_of_use =  (this.$('#loss_of_use').is(":checked") ? 'yes':'no');
        var new_for_old =  (this.$('#new_for_old').is(":checked") ? 'yes':'no');

        this.listenTo(that, 'saved', function (item) {
            //that.render;/
            //navigate next step
            console.info('views/driversexperience.js | goto next step');
            App.showPage('VehicleDiscounts', {
                item: item
            });
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.next-step').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, App.settings.errTimeout);
        });

        if (windscreen_coverage == 'yes' && !windscreen_coverage_amount )
            that.validationError.push({ msg: 'Please enter Comprehensive windscreen coverage amount' });

        if (windscreen_coverage == 'yes' && isNaN(windscreen_coverage_amount_3rd_party))
            that.validationError.push({ msg: 'Invalid Comprehensive windscreen coverage value entered. Please remove all special characters ($)' });

        if (windscreen_coverage_3rd_party == 'yes' && !windscreen_coverage_amount_3rd_party)
            that.validationError.push({ msg: 'Please enter Third Party windscreen coverage' });

        if (windscreen_coverage_3rd_party == 'yes' && isNaN(windscreen_coverage_amount_3rd_party))
            that.validationError.push({ msg: 'Invalid Third Party windscreen coverage value entered. Please remove all special characters ($)' });

        if (windscreen_coverage_3rd_party_fire == 'yes' && !windscreen_coverage_amount_3rd_party_fire)
            that.validationError.push({ msg: 'Please enter Third Party Fire and Theft windscreen coverage' });

        if (windscreen_coverage_3rd_party_fire == 'yes' && isNaN(windscreen_coverage_amount_3rd_party_fire))
            that.validationError.push({ msg: 'Invalid Third Party Fire & Theft windscreen coverage value entered. Please remove all special characters ($)' });


        

        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            var item = this.model;

            item.set('windscreen_coverage', windscreen_coverage);
            item.set('windscreen_coverage_amount', windscreen_coverage_amount);
            item.set('emergency_roadside_assist', emergency_roadside_assist);
            item.set('windscreen_coverage_3rd_party', windscreen_coverage_3rd_party);
            item.set('windscreen_coverage_amount_3rd_party', windscreen_coverage_amount_3rd_party);
            item.set('emergency_roadside_assist_3rd_party', emergency_roadside_assist_3rd_party);
            item.set('windscreen_coverage_3rd_party_fire', windscreen_coverage_3rd_party_fire);
            item.set('windscreen_coverage_amount_3rd_party_fire', windscreen_coverage_amount_3rd_party_fire);
            item.set('emergency_roadside_assist_3rd_party_fire', emergency_roadside_assist_3rd_party_fire);
            item.set('flood_special_perils', flood_special_perils);
            item.set('partial_waiver_of_excess', partial_waiver_of_excess);
            item.set('loss_of_use', loss_of_use);
            item.set('new_for_old', new_for_old);

            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null,{
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}
        
		return false;
	}

});