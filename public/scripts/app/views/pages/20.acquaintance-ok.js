﻿//acquaintance-ok.js
App.Views.Pages.AcquaintanceOk = App.Views.Abstract.Page.extend({

    templateName: 'pages/acquaintance/ok',
    category: 'acquaintanceok',
    events: {
        "submit form": "onSubmit",
    },
    title: function () {
        return 'AcquaintanceOK';
    },
    url: function () {
        return 'acquaintance-ok';
    },
    render: function () {
        console.log('views/pages/acquaintance-ok.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.on('render', function () {
            $('.inpage-form').validator();
        });

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });
        
        this.renderHTML({
            user: App.currentUser.toJSON(),
            item: this.model.toJSON()
        });

        $('.helpericon').hide();
    },
    initializeParts: function () {
        console.info('views/pages/acquaintance-ok.js | initializing parts');
        this.parts = [];
        this.partsInitialized = true;
        this.charts = [];
    },
    wakeUp: function () {
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            that.render();
        });
    },
    initialize: function () {
        console.log('acquaintance-ok.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function (user) {
            that.model = App.currentUser.getProfile();
            that.render();
            that.listenTo(that.model, 'sync', that.render);
        });
    },
    onSubmit: function(){
		var that = this;
        that.validationError = [];

        if(this.$('.next-step').hasClass('disabled'))
            return false;

        this.$('.next-step').button('loading');

        console.info('views/acquaintance-ok.js | goto next step');
        App.showPage('Insurer', {
            item: App.currentUser.getProfile()
        });
        
		return false;
	}

});