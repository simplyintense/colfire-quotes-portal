// session_expired.js
App.Views.Pages.SessionExpired = App.Views.Abstract.Page.extend({

	templateName: 'sessionexpired',
	title: 'Session Expired',
	render: function() {
		this.renderHTML();
        $('.helpericon').hide();
	},
	initialize: function(params) {
		this.render();
	}
    
});