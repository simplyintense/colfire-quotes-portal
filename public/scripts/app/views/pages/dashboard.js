﻿//dashboard.js
App.Views.Pages.Dashboard = App.Views.Abstract.Page.extend({

    templateName: 'pages/dashboard/index',
    additionalScripts: [
        '/vendors/node_modules/moment/min/moment.min.js',
        '/vendors/node_modules/moment-timezone/moment-timezone.js',
        '/vendors/node_modules/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
    ],
    category: 'dashboard',
    events: {
        "click .item": "toItem",
        "click #search_quotes_button": "searchQuotes",
        "click #export_quotes_button": "exportQuotes",
        "click #reset_quotes_search_button": "reset_searchQuotes"
    },
    title: function () {
        return 'Dashboard';
    },
    url: function () {
        return 'dashboard';
    },
    render: function () {
        console.log('views/pages/dashboard.js | rendering');
       
        if (!this.partsInitialized)
            this.initializeParts();

        this.on('render', function () {
            $('.inpage-form').validator();
            });
      

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });

        this.renderHTML({
            items: this.items.toJSON(),
            search_params: this.quote_search_params,
        });

        $('.helpericon').hide();
    },
    initializeParts: function () {
        console.info('views/pages/dashboard.js | initializing parts');
        this.parts = [];
        //this.parts.push(new App.Views.Widgets.SingleStatsCount({
        //    id: 'customer_stats_container',
        //    collection: new App.Collections.Customers()
        //}));

        this.partsInitialized = true;

        this.charts = [];
        //this.charts.push(new App.Views.Charts.Balance({
        //    id: 'balance_canvas',
        //    model: this.model
        //}));
    },
    wakeUp: function () {
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            if (App.currentUser.isDemo())
                window.location.pathname = '/signin';

            that.render();
            that.listenTo(that.items, 'sync add change reset remove', that.render);

            //that.items.fetch({ 
            //    data: { status: 'submitted' }
            //});
            that.reset_searchQuotes();

        });
    },
    initialize: function () {
        console.log('dashboard.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function () {
            if (App.currentUser.isDemo())
                window.location.pathname = '/signin';


            that.items = new App.Collections.Quotes();

            // initialize models, collections etc. Request fetching from storage
            //that.listenTo(that.items, 'sync', that.render);
            that.on('addscriptloaded', that.render);
            that.listenTo(that.items, 'sync', function () {
                App.helper.loadAdditionalScripts(that.additionalScripts, function () {
                    if (!moment.tz.zone(App.settings.apptimezone.zonename))
                        moment.tz.add(App.settings.apptimezone.moment_tz_package);
                    moment.tz.setDefault(App.settings.apptimezone.zonename);

                    that.on('render', function () {
                        $('#datetimepicker_qexportstart').datetimepicker({
                            format: App.settings.apptimezone.date_format,
                            locale: App.settings.apptimezone.locale,
                        });

                        $('#datetimepicker_qexportend').datetimepicker({
                            format: App.settings.apptimezone.date_format,
                            locale: App.settings.apptimezone.locale,
                        });
                    });


                    that.trigger('addscriptloaded');
                });
            });

            that.items.fetch({
                data: { status: 'submitted' }
            }).done(function () {
                that.listenTo(that.items, 'add change reset remove', that.render);
            }).catch(function (error) {
                console.error(error);
                that.render();
            });
        });
    },
    toItem: function (ev) {
        var data = $(ev.currentTarget).data();
        if (typeof (data.id) === 'undefined')
            return true;

        var id = parseInt(data.id, 10);
        var item = this.items.get(id);

        if (!item)
            return true;

        App.showPage('DashboardQuote', {
            item: item
        });

        return false;
    },
    searchQuotes: function (ev) {
        this.quote_search_params = this.quote_search_params || {};

        var ref_num = this.$('#input_ref_num').val();
        var firstname = this.$('#input_firstname').val();
        var lastname = this.$('#input_lastname').val();
        var email = this.$('#input_email').val();
        var phone_number = this.$('#input_phone_number').val();

        this.filterCustomised({
            data: {
                status: 'submitted',
                ref_num: ref_num,
                firstname: firstname,
                lastname: lastname,
                email: email,
                phone_number: phone_number,
            }
        });
        return false;
    },
    reset_searchQuotes: function () {
        this.filterCustomised({
            data: {
                status: 'submitted',
            }
        });
    },
    filterCustomised: function (params) {
        console.log('quote.js | customized filter applied');

        var params = params || {};
        this.renderLoading();
        this.items.fetch(params);
        this.quote_search_params = params.data || {};

    },
    exportQuotes: function (ev) {      

        var strExport = '';
        var proptype = this.$('#export_QuoteType').val();
        var statquote = this.$('#export_type').val(); 
        var startdate = this.$('#datetimepicker_qexportstart').data("DateTimePicker").date();
        var enddate = this.$('#datetimepicker_qexportend').data("DateTimePicker").date();

        strExport = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '') + ('/api/quotes/?csv=yes&proptype=' + proptype + '&statquote=' + statquote + '&startdate=' + moment(startdate).toISOString() + '&enddate=' + moment(enddate).toISOString());
        //strExport = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '') + ('/api/quotes/?csv=yes&proptype=' + proptype + '&statquote=' + statquote + '&startdate=' + startdate + '&enddate=' + enddate);

        window.location.href =  strExport;

         return false;
    }

});