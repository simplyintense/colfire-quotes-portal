﻿//drivers.js
App.Views.Pages.Drivers = App.Views.Abstract.Page.extend({

    templateName: 'pages/quote/motor/drivers',
    category: 'drivers',
    events: {
        "click .add-driver": "addDriver",
        "submit form": "onSubmit",
    },
    title: function () {
        return 'Drivers';
    },
    url: function () {
        if (typeof (this.model) != 'undefined' && this.model.id)
            return 'quotes/' + this.model.attributes.quote_id + '/vehicles/' + this.model.id + '/drivers';
    },
    render: function () {
        console.log('views/pages/drivers.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.on('render', function () {
            $('.inpage-form').validator();
        });

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });
        
        this.renderHTML({
            item: this.model.toJSON(),
            drivers: this.model.drivers.toJSON(),
        });

        $('.helpericon').show();
    },
    initializeParts: function () {
        console.info('views/pages/drivers.js | initializing parts');
        this.parts = [];

        this.listenTo(App.currentUser.getProfile(), 'sync',this.render);

        this.drivers = this.model.getDrivers();
        this.listenTo(this.drivers, 'add update remove',this.render);
        
        this.parts.push(new App.Views.Parts.Drivers({
            id: 'drivers_container',
            model: this.model,
            collection: this.drivers,
        }));
        this.partsInitialized = true;
        this.charts = [];
    },
    wakeUp: function () {
        console.log('views/pages/drivers.js | waking up');
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            that.render();
            that.listenTo(that.model, 'change sync destroy', that.render);
            for (var k in that.parts)
                if (typeof (that.parts[k].wakeUp) === 'function')
                    that.parts[k].wakeUp();
            
            that.model.fetch();
        });
    },
    initialize: function (params) {
        console.log('drivers.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function (user) {


            if (typeof (params.item) !== 'undefined') {
                that.model = params.item;
                that.render();
                that.listenTo(that.model, 'sync', that.render);
            } else if (typeof (params.id) !== 'undefined' && typeof (params.quote_id) !== 'undefined' ) {
                that.model = new App.Models.Vehicle();
                that.model.id = params.id;
                that.model.attributes.quote_id = params.quote_id;

                that.listenTo(that.model, 'sync', that.render);
            
                that.model.fetch({
                    error: function () {
                        App.showPage('NotFound');
                    }
                });
            } else
                throw 'id or item parameters required';

        });

    },
    addDriver: function () {
        App.showDialog('AddDriver', {
            conntotable_model: this.model,
            collection: this.model.getDrivers(),
        });
        return false;
    },
    onSubmit: function(){
		var that = this;
        that.validationError = [];
        
        if(this.$('.next-step').hasClass('disabled'))
            return false;

        this.$('.next-step').button('loading');

        this.drivers = this.model.getDrivers();
        
        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.next-step').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, App.settings.errTimeout);
        });
        
        this.listenToOnce(that.drivers, 'sync', function (drivers) {
            
            if (!this.drivers || this.drivers.length == 0)
                that.validationError.push({ msg: 'Please add at least one driver' });

            if (that.validationError.length > 0) {
                that.trigger('invalid', that.validationError);
		    } else {
                //navigate next step
                console.info('views/drivers.js | goto next step');
                App.showPage('VehicleInsuranceType', {
                    item: that.model
                });
		    }
            
        });

        this.drivers.fetch();
        
		return false;
	}

});