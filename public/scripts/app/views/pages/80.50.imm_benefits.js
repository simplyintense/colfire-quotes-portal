﻿//immovablebenefits.js
App.Views.Pages.ImmovableBenefits = App.Views.Abstract.Page.extend({

    templateName: 'pages/quote/immovable/benefits',
    category: 'immovablebenefits',
    events: {
        "submit form": "onSubmit",
        "change #subsidence_and_landslip": "SubsidenceAndLandslipChanged",
        "change input[type=radio][name=subsidence_and_landslip_retaining_wall]": "SubsidenceAndLandslipInsured",

    },
    title: function () {
        return 'Benefits';
    },
    url: function () {
        if (typeof (this.model) != 'undefined' && this.model.id)
            return 'quotes/' + this.model.attributes.quote_id + '/immovables/' + this.model.id + '/benefits';
    },
    render: function () {
        console.log('views/pages/immovablebenefits.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.on('render', function () {
            $('.inpage-form').validator();
        });

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });
        
        this.renderHTML({
            item: this.model.toJSON()
        });

        $('.helpericon').show();
    },
    initializeParts: function () {
        console.info('views/pages/immovablebenefits.js | initializing parts');
        this.parts = [];
        this.partsInitialized = true;
        this.charts = [];
    },
    wakeUp: function () {
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            that.render();
        });
    },
    initialize: function (params) {
        console.log('immovablebenefits.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function (user) {


            if (typeof (params.item) !== 'undefined') {
                that.model = params.item;
                that.render();
                that.listenTo(that.model, 'sync', that.render);
            } else if (typeof (params.id) !== 'undefined' && typeof (params.quote_id) !== 'undefined' ) {
                that.model = new App.Models.Immovable();
                that.model.id = params.id;
                that.model.attributes.quote_id = params.quote_id;

                that.listenTo(that.model, 'sync', that.render);
            
                that.model.fetch({
                    error: function () {
                        App.showPage('NotFound');
                    }
                });
            } else
                throw 'id or item parameters required';

        });

    },
    onSubmit: function(){
		var that = this;
        that.validationError = [];
        
        if(this.$('.next-step').hasClass('disabled'))
            return false;

        this.$('.next-step').button('loading');

        var flood =  (this.$('#flood').is(":checked") ? 'yes':'no');
        var subsidence_and_landslip = (this.$('#subsidence_and_landslip').is(":checked") ? 'yes' : 'no');
        var subsidence_and_landslip_retaining_wall = this.$('input[type=radio][name=subsidence_and_landslip_retaining_wall]:checked').val();
        var subsidence_and_landslip_insured = (this.$('#subsidence_and_landslip_insured').val() || '');
        var damage_to_external_radio_and_tv_aerials =  (this.$('#damage_to_external_radio_and_tv_aerials').is(":checked") ? 'yes':'no');
        var damage_to_undergr_water_gas_pipes_or_electr_cables =  (this.$('#damage_to_undergr_water_gas_pipes_or_electr_cables').is(":checked") ? 'yes':'no');
        var workmen_compens_any_domestic_workers =  (this.$('#workmen_compens_any_domestic_workers').is(":checked") ? 'yes':'no');
        var removal_debris =  (this.$('#removal_debris').is(":checked") ? 'yes':'no');
        
        
        this.listenTo(that, 'saved', function (item) {
            //that.render;/
            //navigate next step
            console.info('views/immovablebenefits.js | goto next step');
            App.showPage('SubmitQuote', {
                id: item.get('quote_id')
            });
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.next-step').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, App.settings.errTimeout);
        });

        
        if (subsidence_and_landslip == 'yes' && !subsidence_and_landslip_retaining_wall)
            that.validationError.push({ msg: 'Please select is there a retaining wall on the property' });

        if (subsidence_and_landslip == 'yes' && subsidence_and_landslip_retaining_wall == 'yes' && !subsidence_and_landslip_insured)
            that.validationError.push({ msg: 'Please enter the sum insured on the retaining wall' });
        

        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            var item = this.model;

            item.set('flood', flood);
            item.set('subsidence_and_landslip', subsidence_and_landslip);
            item.set('subsidence_and_landslip_retaining_wall', subsidence_and_landslip_retaining_wall);
            item.set('subsidence_and_landslip_insured', subsidence_and_landslip_insured);
            item.set('damage_to_external_radio_and_tv_aerials', damage_to_external_radio_and_tv_aerials);
            item.set('damage_to_undergr_water_gas_pipes_or_electr_cables', damage_to_undergr_water_gas_pipes_or_electr_cables);
            item.set('workmen_compens_any_domestic_workers', workmen_compens_any_domestic_workers);
            item.set('removal_debris', removal_debris);

            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null,{
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}
        
		return false;
    },

    SubsidenceAndLandslipChanged: function (ev) {
        var subsidence_and_landslip = (this.$('#subsidence_and_landslip').is(":checked") ? 'yes' : 'no');
        if (subsidence_and_landslip == 'yes') {
            this.$('.retaining_wall').fadeIn();
        } else {
            this.$('.retaining_wall').fadeOut();
        }
    },

    SubsidenceAndLandslipInsured: function () {
        
        var subsidence_and_landslip_retaining_wall = this.$('input[type=radio][name=subsidence_and_landslip_retaining_wall]:checked').val();
        if (subsidence_and_landslip_retaining_wall == 'yes') {
            this.$('.subsidence_and_landslip_insured_form_group').fadeIn();
        } else {
            this.$('.subsidence_and_landslip_insured_form_group').fadeOut();
        }
    },

});