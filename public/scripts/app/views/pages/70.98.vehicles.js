﻿//vehicles.js
App.Views.Pages.Vehicles = App.Views.Abstract.Page.extend({

    templateName: 'pages/quote/motor/vehicles',
    category: 'vehicles',
    events: {
        "click .add-vehicle": "addVehicle",
        "click .item_button_remove": "removeItem",
        "click .item_button_edit": "editItem",
        "submit form": "onSubmit",
    },
    title: function () {
        return 'Vehicles';
    },
    url: function () {
        if (typeof (this.model) != 'undefined' && this.model.id)
            return 'quotes/' + this.model.id + '/vehicles';
    },
    render: function () {
        console.log('views/pages/vehicles.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.on('render', function () {
            $('.inpage-form').validator();
        });

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });
        
        this.renderHTML({
            item: this.model.toJSON()
        });

        $('.helpericon').show();
    },
    initializeParts: function () {
        console.info('views/pages/vehicles.js | initializing parts');
        this.parts = [];
        this.parts.push(new App.Views.Parts.Vehicles({
            id: 'vehicles_container',
            model: this.model,
            collection: this.model.getVehicles()
        }));
        this.partsInitialized = true;
        this.charts = [];
    },
    wakeUp: function () {
        console.log('views/pages/vehicles.js | waking up');
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            that.render();
            that.listenTo(that.model, 'change sync destroy', that.render);
            for (var k in that.parts)
                if (typeof (that.parts[k].wakeUp) === 'function')
                    that.parts[k].wakeUp();
            
            that.model.fetch();
        });
    },
    initialize: function (params) {
        console.log('vehicles.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function (user) {

            if (typeof (params.item) !== 'undefined') {
                that.model = params.item;
                that.render();
                that.listenTo(that.model, 'sync', that.render);
            } else if (typeof (params.id) !== 'undefined' ) {
                that.model = new App.Models.Quote();
                that.model.id = params.id;

                that.listenTo(that.model, 'sync', that.render);
            
                that.model.fetch({
                    error: function () {
                        App.showPage('NotFound');
                    }
                });
            } else
                throw 'id or item parameters required';

        });

    },
    addVehicle: function () {
        console.info('views/vehicles.js | goto add vehicle');
        App.showPage('InsuranceOk', {
            id: this.model.id,
        });
        return false;
    },
    onSubmit: function(){
		var that = this;
        that.validationError = [];
        
        if(this.$('.next-step').hasClass('disabled'))
            return false;

        this.$('.next-step').button('loading');

        this.vehicles = this.model.getVehicles();
        
        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            //this.$('#input_customer_type').focus();
            this.$('.next-step').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, App.settings.errTimeout);
        });
        
        this.listenToOnce(that.vehicles, 'sync', function (vehicles) {
            
            if (!vehicles || vehicles.length == 0)
                that.validationError.push({ msg: 'Please add at least one vehicle' });

            if (that.validationError.length > 0) {
                that.trigger('invalid', that.validationError);
		    } else {
                //navigate next step
                console.info('views/vehicles.js | goto next step');
                App.showPage('SubmitQuote', {
                    id: this.model.id,
                });
		    }
            
        });

        this.vehicles.fetch();
        
		return false;
	}

});