// index.js
App.Views.Pages.Index = App.Views.Abstract.Page.extend({

	templateName: 'pages/index/index',
	category: 'home',
	events: {
        "click .start-process": "demoSignUp",
	},
	demoSignUp: function() {
		//this.renderLoading();
        this.$('.start-process').button('loading');
        if (typeof(App.currentUser) !== 'undefined' && App.currentUser && App.currentUser.isSignedIn()){
			//App.router.redirect('/acquaintance/');
            App.showPage('Acquaintance', {
                item: App.currentUser.getProfile()
            });
		} else {
			this.listenToOnce(App.currentUser, 'signedInStatusChanged', function() {
                if(App.currentUser.isDemo()){
                    //App.router.redirect('/acquaintance/');
                    App.showPage('Acquaintance', {
                        item: App.currentUser.getProfile()
                    });
                }
		    });
            App.currentUser.demoRegister();
		}
	},
	title: function() {
		return 'Welcome';
	},
	render: function() {
		this.renderHTML({});
	},
	wakeUp: function() {
		if (typeof(App.currentUser) !== 'undefined' && App.currentUser && App.currentUser.isSignedIn()){
			if(!App.currentUser.isDemo()){
                //App.router.redirect('/dashboard/');
            }
		} else {
			this.holderReady = false;
			this.render();
		}
	},
	sleep: function() {
		$(window).off('resize');
	},
	resize: function() {

	},
	initialize: function() {
		var that = this;
		if (typeof(App.currentUser) !== 'undefined' && App.currentUser && App.currentUser.isSignedIn()){
            if(!App.currentUser.isDemo()){
                //return App.router.redirect('/dashboard/');
            }
        }
		this.renderLoading();

		/// initialize models, collections etc. Request fetching from storage
		this.listenTo(App.currentUser, 'signedInStatusChanged', function() {
            if(!App.currentUser.isDemo()){
                //App.router.redirect('/dashboard/');
            }
		});

		this.render();
	}

});