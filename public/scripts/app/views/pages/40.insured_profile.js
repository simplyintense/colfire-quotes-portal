﻿//insured_profile.js
App.Views.Pages.Insured = App.Views.Abstract.Page.extend({

    templateName: 'pages/acquaintance/profile',
    additionalScripts: [
        '/vendors/node_modules/moment/min/moment.min.js',
        '/vendors/node_modules/moment-timezone/moment-timezone.js',
        '/vendors/node_modules/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
    ],
    category: 'profile',
    events: {
        "submit form": "onSubmit",
    },
    title: function () {
        return 'Profile';
    },
    url: function () {
        return 'insured';
    },
    render: function () {
        console.log('views/pages/profile.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.on('render', function () {
            $('.inpage-form').validator();
        });

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });
        
        this.renderHTML({
            user: App.currentUser.toJSON(),
            item: this.model.toJSON()
        });

        $('.helpericon').show();

    },
    initializeParts: function () {
        console.info('views/pages/profile.js | initializing parts');
        this.parts = [];
        this.partsInitialized = true;
        this.charts = [];
    },
    wakeUp: function () {
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            that.render();
        });
    },
    initialize: function (params) {
        console.log('profile.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function (user) {

            //remember quote_id from selected on previous page
            //if (typeof (params.item) !== 'undefined'){
            //    that.quote = params.item;
            //    that.quote_id = that.quote.id;
            //}else if(typeof (params.id) !== 'undefined'){
            //    that.quote_id = params.id;
            //}
            
            that.model = App.currentUser.getProfile();
            //that.render();
            //that.listenTo(that.model, 'sync', that.render);

            that.on('addscriptloaded', that.render);
            that.listenTo(that.model, 'sync', function () {
                App.helper.loadAdditionalScripts(that.additionalScripts, function () {
                    if (!moment.tz.zone(App.settings.apptimezone.zonename))
                        moment.tz.add(App.settings.apptimezone.moment_tz_package);
                    moment.tz.setDefault(App.settings.apptimezone.zonename);

                    that.on('render', function () {
                        $('#datetimepicker').datetimepicker({
                            defaultDate: that.model.get('dateofbirth') || null,
                            minDate: moment("1950-01-01T00:00:00.000Z"),
                            maxDate: moment().add(1,'days'),
                            viewMode: 'years',
                            format: App.settings.apptimezone.date_format,
                            locale: App.settings.apptimezone.locale,
                        });
                    });

                    that.trigger('addscriptloaded');
                });
            });
            that.model.fetch();

        });
    },
    onSubmit:  function(){
		var that = this;
        that.validationError = [];

        if(this.$('.next-step').hasClass('disabled'))
            return false;

        this.$('.next-step').button('loading');

        var home_address = this.$('#input_home_address').val();
        var email = this.$('#input_email').val();
        var occupation = this.$('#input_occupation').val();
        var phone_number = this.$('#input_phone_number').val();
        var gender = this.$('#input_gender').val();
        var dateofbirth = this.$('#datetimepicker').data("DateTimePicker").date();
        
        this.listenTo(that, 'saved', function (item) {
            //that.render;/
            //navigate next step
            console.info('views/acquaintance.js | goto next step');
            App.showPage('Insurance', {
                id: App.currentUser.getProfile()
            });
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.next-step').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, App.settings.errTimeout);
        });

        if (!home_address)
            that.validationError.push({ msg: 'Please enter your home address' });

        if (!email)
            that.validationError.push({ msg: 'Please enter your email' });

        if (!occupation)
            that.validationError.push({ msg: 'Please enter your occupation' });

        if (!phone_number)
            that.validationError.push({ msg: 'Please enter your phone number' });

        if (!gender)
            that.validationError.push({ msg: 'Please enter your gender' });

        if (!dateofbirth)
            that.validationError.push({ msg: 'Please enter your date of birth' });

        var a = moment();
        if (a.diff(dateofbirth, 'years') < 18 )
            that.validationError.push({ msg: 'You must be 18 years or older to request a quote' });
        
        

        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            var item = this.model; //App.currentUser;

            item.set('home_address', home_address);
            item.set('email', email);
            item.set('occupation', occupation);
            item.set('phone_number', phone_number);
            item.set('gender', gender);
            item.set('dateofbirth', dateofbirth);
            
            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null,{
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}

		return false;
	}

});