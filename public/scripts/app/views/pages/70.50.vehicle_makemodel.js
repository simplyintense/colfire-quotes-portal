﻿//vehiclemakemodel.js
App.Views.Pages.VehicleMakeModel = App.Views.Abstract.Page.extend({

    templateName: 'pages/quote/motor/makemodel',
    additionalScripts: [
        '/vendors/node_modules/moment/min/moment.min.js',
        '/vendors/node_modules/moment-timezone/moment-timezone.js'],
    category: 'vehiclemakemodel',
    events: {
        // "change #input_make": "makeSelected",
        // "change #input_model": "modelSelected",
        "submit form": "onSubmit",
    },
    title: function () {
        return 'Value';
    },
    url: function () {
        if (typeof (this.model) != 'undefined' && this.model.id)
            return 'quotes/' + this.model.attributes.quote_id + '/vehicles/' + this.model.id + '/makemodel';
    },
    render: function () {
        console.log('views/pages/vehiclemakemodel.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.on('render', function () {
            $('.inpage-form').validator();
        });

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });
        
        this.renderHTML({
            item: this.model.toJSON(),
            settings: App.settings,
            years_list: App.settings.car_years_list()
        });

        $('.helpericon').show();

        // $('.inpage-form').validator('update');
    },
    initializeParts: function () {
        console.info('views/pages/vehiclemakemodel.js | initializing parts');
        this.parts = [];

        var carMakes = new App.Collections.CarMakes();
        this.listenTo(carMakes, 'add update remove', this.render);
        this.parts.push(new App.Views.Parts.CarMakes({
            id: 'input_make_form_group',
            model: this.model,
            collection: carMakes,
        }));

        var carModels = new App.Collections.CarModels();
        this.parts.push(new App.Views.Parts.CarModels({
            id: 'input_model_form_group',
            model: this.model,
            collection: carModels,
            makesCollection: carMakes
        }));

        this.listenTo(this.model, 'loaded', function () {
            this.$('.inpage-form').validator('update');
        });

        this.partsInitialized = true;
        this.charts = [];
    },
    wakeUp: function () {
        console.log('views/pages/vehiclemakemodel.js | waking up');
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            
            that.listenTo(that.model, 'change', function (){
                this.resUpdateModel();
            });
            
            // that.render();

            that.listenTo(that.model, 'change sync destroy', that.render);
            for (var k in that.parts)
                if (typeof (that.parts[k].wakeUp) === 'function')
                    that.parts[k].wakeUp();

            that.model.fetch();
        });
    },
    initialize: function (params) {
        console.log('vehiclemakemodel.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function (user) {

            
            if (typeof (params.item) !== 'undefined') {
                that.model = params.item;
                that.render();
                that.listenTo(that.model, 'sync', that.render);
            } else if (typeof (params.id) !== 'undefined' && typeof (params.quote_id) !== 'undefined') {
                that.model = new App.Models.Vehicle();
                that.model.id = params.id;
                that.model.attributes.quote_id = params.quote_id;

                that.listenTo(that.model, 'sync', that.render);

                that.model.fetch({
                    error: function () {
                        App.showPage('NotFound');
                    }
                });
            } else
                throw 'id or item parameters required';


            that.on('addscriptloaded', that.render);
            that.listenTo(that.model, 'sync', function () {
                App.helper.loadAdditionalScripts(that.additionalScripts, function () {
                    if (!moment.tz.zone(App.settings.apptimezone.zonename))
                        moment.tz.add(App.settings.apptimezone.moment_tz_package);
                    moment.tz.setDefault(App.settings.apptimezone.zonename);
                    that.trigger('addscriptloaded');
                });
            });

            that.listenTo(that.model, 'change', function () {
                this.resUpdateModel();
            });
        });

    },
    resUpdateModel: function () {

        // var vehicle_model = this.$('#input_model').val();
        var vehicle_model = this.model.get('model');
        
        var make_id = this.$('#input_make').find('option:selected').attr('data-id');
    
        if (!vehicle_model || !make_id) {
            this.$('next-step').attr('disabled', 'disabled');
        }
        
        if (this.model.get('model') === 'OTHER') {
            this.$('.custom_descr_make_model_form_group').show();
            
            this.$('#custom_descr_make_model').attr('required', true);
            this.$('#custom_descr_make_model').attr('data-validate',true);

            //this.render(); //needed to pass car model = other -> activate require attr, but resets year and type
        } else {
            this.$('.custom_descr_make_model_form_group').fadeOut();

            this.$('#custom_descr_make_model').removeAttr('required');
            this.$('#custom_descr_make_model').attr('data-validate',false);

        }
        // this.render();
        $('.inpage-form').validator('update');

    },
    onSubmit: function(){
		var that = this;
        that.validationError = [];
        
        if(this.$('.next-step').hasClass('disabled'))
            return false;

        this.$('.next-step').button('loading');

        var make = this.$('#input_make').val();
        var model = this.$('#input_model').val();
        var year = this.$('#input_year').val();
        var vehicle_origin = this.$('#vehicle_origin').val();
        var custom_descr_make_model = this.$('#custom_descr_make_model').val();
        var vehicle_age = moment().year() - year;
        
        
        this.listenTo(that, 'saved', function (item) {
            //that.render;/
            //navigate next step
            console.info('views/driversexperience.js | goto next step');
            App.showPage('VehiclePlannedUsage', {
                item: item
            });
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.next-step').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, App.settings.errTimeout);
        });

        if (!make)
            that.validationError.push({ msg: 'Please select vehicle make' });

        if (!model)
            that.validationError.push({ msg: 'Please select vehicle model' });

        if (!year)
            that.validationError.push({ msg: 'Please enter vehicle manufacturing year' });

        if (!vehicle_origin)
            that.validationError.push({ msg: 'Please select vehicle origin type' });

        if (model == 'OTHER' && !custom_descr_make_model)
            that.validationError.push({ msg: 'Please enter custom vehicle make and model' });

        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            var item = this.model;

            item.set('make', make);
            item.set('model', model);
            item.set('year', year);
            item.set('vehicle_origin', vehicle_origin);
            item.set('custom_descr_make_model', custom_descr_make_model);
            item.set('vehicle_age', vehicle_age);

            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null,{
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}
        
		return false;
	}

});