﻿//dashboardquote.js
App.Views.Pages.DashboardQuote = App.Views.Abstract.Page.extend({

    templateName: 'pages/dashboard/quote',
    additionalScripts: [
        '/vendors/node_modules/moment/min/moment.min.js',
        '/vendors/node_modules/moment-timezone/moment-timezone.js',
    ],
    category: 'dashboardquote',
    events: {
        "click .toggler": "togglePanel",
    },
    title: function () {
        return 'Quote';
    },
    url: function () {
        if (typeof (this.model) != 'undefined' && this.model.id)
            return 'dashboard/quotes/' + this.model.id;
    },
    render: function () {
        console.log('views/pages/dashboardquote.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.on('render', function () {
            $('.inpage-form').validator();
        });

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });
        
        this.renderHTML({
            item: this.model.toJSON(),
            settings: App.settings
        });

        $('.helpericon').hide();
    },
    initializeParts: function () {
        console.info('views/pages/dashboardquote.js | initializing parts');
        this.parts = [];
        this.partsInitialized = true;
        this.charts = [];
    },
    wakeUp: function () {
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            if (App.currentUser.isDemo())
                window.location.pathname = '/signin';

            that.render();
        });
    },
    initialize: function (params) {
        console.log('dashboardquote.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function (user) {
            if (App.currentUser.isDemo())
                window.location.pathname = '/signin';

            if (typeof (params.item) !== 'undefined') {
                that.model = params.item;
                //that.render();
                that.on('addscriptloaded', that.render);
                App.helper.loadAdditionalScripts(that.additionalScripts, function () {
                    if (!moment.tz.zone(App.settings.apptimezone.zonename))
                        moment.tz.add(App.settings.apptimezone.moment_tz_package);
                    moment.tz.setDefault(App.settings.apptimezone.zonename);
                    that.trigger('addscriptloaded');
                });
                that.listenTo(that.model, 'sync', that.render);
            } else if (typeof (params.id) !== 'undefined') {
                that.model = new App.Models.Quote();
                that.model.id = params.id;

                //that.listenTo(that.model, 'sync', that.render);
                that.on('addscriptloaded', that.render);
                that.listenTo(that.model, 'sync', function () {
                    App.helper.loadAdditionalScripts(that.additionalScripts, function () {
                        if (!moment.tz.zone(App.settings.apptimezone.zonename))
                            moment.tz.add(App.settings.apptimezone.moment_tz_package);
                        moment.tz.setDefault(App.settings.apptimezone.zonename);
                        that.trigger('addscriptloaded');
                    });
                });
            
                that.model.fetch({
                    error: function () {
                        App.showPage('NotFound');
                    }
                });
            } else
                throw 'id or item parameters required';
        });

    },
    togglePanel: function (ev) {
        var curEvId = $(ev.currentTarget).attr('id');
        this.$('#' + curEvId + ' span').toggleClass("glyphicon glyphicon-chevron-down");
        this.$('#' + curEvId + ' span').toggleClass("glyphicon glyphicon-chevron-up");
        this.$('.' + curEvId ).toggleClass("hidden");
    },

});