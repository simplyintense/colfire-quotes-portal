﻿//insurance-ok.js
App.Views.Pages.InsuranceOk = App.Views.Abstract.Page.extend({

    templateName: 'pages/quote/ok',
    category: 'insuranceok',
    events: {
        "submit form": "onSubmit",
    },
    title: function () {
        return 'InsuranceOK';
    },
    url: function () {
        if (typeof (this.model) != 'undefined' && this.model.id)
            return 'quotes/' + this.model.id + '/ok';
    },
    render: function () {
        console.log('views/pages/insurance-ok.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.on('render', function () {
            $('.inpage-form').validator();
        });

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });
        
        this.renderHTML({
            item: this.model.toJSON()
        });

        $('.helpericon').show();
    },
    initializeParts: function () {
        console.info('views/pages/insurance-ok.js | initializing parts');
        this.parts = [];
        this.partsInitialized = true;
        this.charts = [];
    },
    wakeUp: function () {
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            that.render();
        });
    },
    initialize: function (params) {
        console.log('insurance-ok.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function (user) {
            if (typeof (params.item) !== 'undefined') {
                that.model = params.item;
                that.render();
                that.listenTo(that.model, 'sync', that.render);
            } else if (typeof (params.id) !== 'undefined') {
                that.model = new App.Models.Quote();
                that.model.id = params.id;

                that.listenTo(that.model, 'sync', that.render);
            
                that.model.fetch({
                    error: function () {
                        App.showPage('NotFound');
                    }
                });
            } else
                throw 'id or item parameters required';
        });

    },
    onSubmit: function(){
		var that = this;
        that.validationError = [];
        
        if(this.$('.next-step').hasClass('disabled'))
            return false;

        this.$('.next-step').button('loading');

        var insurance_type = this.model.get('insurance_type');
                
        this.listenTo(that, 'saved', function (item) {
            //that.render;
            //navigate next step
            console.info('views/insurance-ok.js | goto next step');

            var nextPageName = '';
            if(insurance_type == 'motor'){
                nextPageName = 'Drivers'; //'DriversExperience';
            }else if(insurance_type == 'property'){
                nextPageName = 'TypeOfPropertyInsuranceAndUsage';
            }

            App.showPage(nextPageName, {
                item: item
            });
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.next-step').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, App.settings.errTimeout);
        });

        if (!insurance_type)
            that.validationError.push({ msg: 'App error: no Insurance type available' });
        

        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            if(insurance_type == 'motor'){
                var item = new App.Models.Vehicle();
            }else if(insurance_type == 'property'){
                var item = new App.Models.Immovable();
            }
            
            item.set('quote_id', this.model.id);
            
            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null,{
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}
        
		return false;
	}

});