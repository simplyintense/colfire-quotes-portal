﻿//immovablesecurity.js
App.Views.Pages.ImmovableSecurity = App.Views.Abstract.Page.extend({

    templateName: 'pages/quote/immovable/security',
    category: 'immovablesecurity',
    events: {
        'change input[type=radio][name=claims_history]': 'changedClaimsHistory',
        "click .add-claim": "addClaim",
        "submit form": "onSubmit",
    },
    title: function () {
        return 'Security';
    },
    url: function () {
        if (typeof (this.model) != 'undefined' && this.model.id)
            return 'quotes/' + this.model.attributes.quote_id + '/immovables/' + this.model.id + '/security';
    },
    render: function () {
        console.log('views/pages/immovablesecurity.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.on('render', function () {
            $('.inpage-form').validator();
        });

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });

        
        this.renderHTML({
            profile: App.currentUser.profile.toJSON(),
            item: this.model.toJSON()
        });

        $('.helpericon').show();
    },
    initializeParts: function () {
        console.info('views/pages/immovablesecurity.js | initializing parts');
        this.parts = [];
        
        this.listenTo(App.currentUser.getProfile(), 'sync',this.render);

        this.parts.push(new App.Views.Parts.Claims({
            id: 'claims_container',
            model: this.model,
            collection: this.model.getClaims()
        }));

        this.partsInitialized = true;
        this.charts = [];
    },
    wakeUp: function () {
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            that.render();
            that.listenTo(that.model, 'change sync destroy', that.render);
            for (var k in that.parts)
                if (typeof (that.parts[k].wakeUp) === 'function')
                    that.parts[k].wakeUp();
            
            that.model.fetch();
        });
    },
    initialize: function (params) {
        console.log('immovablesecurity.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function (user) {

            if (typeof (params.item) !== 'undefined') {
                that.model = params.item;
                that.render();
                that.listenTo(that.model, 'sync', that.render);
            } else if (typeof (params.id) !== 'undefined' && typeof (params.quote_id) !== 'undefined' ) {
                that.model = new App.Models.Immovable();
                that.model.id = params.id;
                that.model.attributes.quote_id = params.quote_id;

                that.listenTo(that.model, 'sync', that.render);
            
                that.model.fetch({
                    error: function () {
                        App.showPage('NotFound');
                    }
                });
            } else
                throw 'id or item parameters required';

        });

    },
    changedClaimsHistory: function(){
        var claims_history = this.$('input[type=radio][name=claims_history]:checked').val();
        if(claims_history == 'yes'){
            this.$('#claims_history_descr_input_group').show();
        }else{
            this.$('#claims_history_descr_input_group').hide();
        }
    },
    addClaim: function () {
        App.showDialog('AddClaim', {
            conntotable_model: this.model,
        });
        return false;
    },
    onSubmit: function(){
		var that = this;
        that.validationError = [];
        
        if(this.$('.next-step').hasClass('disabled'))
            return false;

        this.$('.next-step').button('loading');

        var fire_extinguishers =  (this.$('#fire_extinguishers').is(":checked") ? 'yes':'no');
        var hose_reels =  (this.$('#hose_reels').is(":checked") ? 'yes':'no');
        var sprinkler_system =  (this.$('#sprinkler_system').is(":checked") ? 'yes':'no');
        var smoke_detectors =  (this.$('#smoke_detectors').is(":checked") ? 'yes':'no');
        var alarms =  (this.$('#alarms').is(":checked") ? 'yes':'no');
        var security_personnel =  (this.$('#security_personnel').is(":checked") ? 'yes':'no');
        var deadbolts_padlocks =  (this.$('#deadbolts_padlocks').is(":checked") ? 'yes':'no');
        var gated_community =  (this.$('#gated_community').is(":checked") ? 'yes':'no');
        var gated_property =  (this.$('#gated_property').is(":checked") ? 'yes':'no');
        var burglar_proofing =  (this.$('#burglar_proofing').is(":checked") ? 'yes':'no');
        var claims_history = this.$('input[type=radio][name=claims_history]:checked').val();
        
        this.listenTo(that, 'saved', function (item) {
            //that.render;/
            //navigate next step
            console.info('views/immovablesecurity.js | goto next step');
            App.showPage('ImmovableBenefits', {
                item: item
            });
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.next-step').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, App.settings.errTimeout);
        });

        var activeclaims = this.model.claims.where({status:'active'});
        if (claims_history == 'yes' && activeclaims.length == 0)
            that.validationError.push({ msg: 'Please add at least one Claim' });
        

        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            var item = this.model;

            item.set('fire_extinguishers', fire_extinguishers);
            item.set('hose_reels', hose_reels);
            item.set('smoke_detectors', smoke_detectors);
            item.set('sprinkler_system', sprinkler_system);
            item.set('alarms', alarms);
            item.set('security_personnel', security_personnel);
            item.set('deadbolts_padlocks', deadbolts_padlocks);
            item.set('gated_community', gated_community);
            item.set('gated_property', gated_property);
            item.set('burglar_proofing', burglar_proofing);
            item.set('claims_history', claims_history);
            
            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null,{
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}
        
		return false;
	}

});