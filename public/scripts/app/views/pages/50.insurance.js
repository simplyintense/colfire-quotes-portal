﻿//insurance.js
App.Views.Pages.Insurance = App.Views.Abstract.Page.extend({

    templateName: 'pages/quote/index',
    category: 'insurance',
    events: {
        'click button.btn-insurance': 'changedInsuranceType',
        "submit form": "onSubmit",
    },
    title: function () {
        return 'Insurance';
    },
    url: function () {
        return 'insurance';
    },
    render: function () {
        console.log('views/pages/insurance.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.on('render', function () {
            $('.inpage-form').validator();
        });

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });
        
        this.renderHTML({
            user: App.currentUser.toJSON(),
            item: this.model.toJSON()
        });

        $('.helpericon').show();

    },
    initializeParts: function () {
        console.info('views/pages/insurance.js | initializing parts');
        this.parts = [];
        this.partsInitialized = true;
        this.charts = [];
    },
    wakeUp: function () {
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            that.render();
        });
    },
    initialize: function () {
        console.log('insurance.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function (user) {
            that.model = App.currentUser.getProfile();
            that.render();
            that.listenTo(that.model, 'sync', that.render);
        });
    },
    changedInsuranceType: function(ev){
        this.$('button.btn-insurance').removeClass('selected');
        var btnId = this.$(ev.target).attr('data-id');
        this.$('#' + btnId).addClass('selected');
        this.onSubmit();
    },
    onSubmit: function(){

		var that = this;
        that.validationError = [];

        if(this.$('.next-step').hasClass('disabled'))
            return false;

        this.$('.next-step').button('loading');

        var insurance_type =  this.$('button.btn-insurance.selected').val();
        
        this.listenTo(that, 'saved', function (item) {
            //that.render;/
            //navigate next step
            console.info('views/insurance.js | goto next step');
            App.showPage('InsuranceOk', {
                item: item,
            });
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.next-step').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, App.settings.errTimeout);
        });

        if (!insurance_type)
            that.validationError.push({ msg: 'Please select Insurance Type' });
        

        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            var item = new App.Models.Quote();

            item.set('insurance_type', insurance_type);
            item.set('user_id', this.model.id);
            
            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null,{
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}

		return false;
	}

});