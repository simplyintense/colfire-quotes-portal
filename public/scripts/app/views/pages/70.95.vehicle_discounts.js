﻿//vehiclediscounts.js
App.Views.Pages.VehicleDiscounts = App.Views.Abstract.Page.extend({

    templateName: 'pages/quote/motor/discounts',
    category: 'vehiclediscounts',
    events: {
        'change #no_claim_discount_transfer': 'changedNoClaim',
        'change #defensive_driver_cert': 'changedDefensiveDriver',
        'change #loan': 'changedLoan',
        'change #input_bank_name': 'changedLoanBank',
        'change #credit_card_bank': 'changedCreditCardBank',
        'change #credit_card': 'changedCreditCard',
        "submit form": "onSubmit",
    },
    title: function () {
        return 'Discounts';
    },
    url: function () {
        if (typeof (this.model) != 'undefined' && this.model.id)
            return 'quotes/' + this.model.attributes.quote_id + '/vehicles/' + this.model.id + '/discounts';
    },
    render: function () {
        console.log('views/pages/vehiclediscounts.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.on('render', function () {
            $('.inpage-form').validator();
        });

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });
        
        this.renderHTML({
            item: this.model.toJSON(),
            settings: App.settings, 
            years_list: App.settings.car_years_list()
        });

        $('.helpericon').show();

    },
    initializeParts: function () {
        console.info('views/pages/vehiclediscounts.js | initializing parts');
        this.parts = [];
        this.partsInitialized = true;
        this.charts = [];
    },
    wakeUp: function () {
        console.log('views/pages/vehiclediscounts.js | waking up');
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            that.render();
            that.listenTo(that.model, 'change sync destroy', that.render);
            for (var k in that.parts)
                if (typeof (that.parts[k].wakeUp) === 'function')
                    that.parts[k].wakeUp();
            
            that.model.fetch();
        });
    },
    initialize: function (params) {
        console.log('vehiclediscounts.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function (user) {


            if (typeof (params.item) !== 'undefined') {
                that.model = params.item;
                that.render();
                that.listenTo(that.model, 'sync', that.render);
            } else if (typeof (params.id) !== 'undefined' && typeof (params.quote_id) !== 'undefined' ) {
                that.model = new App.Models.Vehicle();
                that.model.id = params.id;
                that.model.attributes.quote_id = params.quote_id;

                that.listenTo(that.model, 'sync', that.render);
            
                that.model.fetch({
                    error: function () {
                        App.showPage('NotFound');
                    }
                });
            } else
                throw 'id or item parameters required';

        });

    },
    changedNoClaim: function(ev){
        var no_claim_discount_transfer =  (this.$('#no_claim_discount_transfer').is(":checked") ? 'yes':'no');
        if(no_claim_discount_transfer == 'yes'){
            this.$('.no_claim_discount_years_form_group').fadeIn();
        }else{
            this.$('.no_claim_discount_years_form_group').fadeOut();
        }
    },
    changedDefensiveDriver: function(){
        var defensive_driver_cert =  (this.$('#defensive_driver_cert').is(":checked") ? 'yes':'no');
        if(defensive_driver_cert == 'yes'){
            this.$('.defensive_driver_school_form_group').fadeIn();
        }else{
            this.$('.defensive_driver_school_form_group').fadeOut();
        }
    },
    changedLoan: function(){
        var loan =  (this.$('#loan').is(":checked") ? 'yes':'no');
        if(loan == 'yes'){
            this.$('.bank_name_form_group').fadeIn();
        }else{
            this.$('.bank_name_form_group').fadeOut();
        }
    },
    changedCreditCard: function (ev) {
        var creditCard = (this.$('#credit_card').is(":checked") ? 'yes' : 'no');
        if (creditCard == 'yes') {
            this.$('.credit_card_form_group').fadeIn();
        } else {
            this.$('.credit_card_form_group').fadeOut();
        }
    },
    changedLoanBank: function (ev) {
        var loanBankOther = this.$('#input_bank_name').val();
        var loan = (this.$('#loan').is(":checked") ? 'yes' : 'no');
        if (loan == 'yes' && loanBankOther == 'other') {
            this.$('.custom_loan_bank_form_group').show();
        } else {
            this.$('.custom_loan_bank_form_group').fadeOut();
        }
    },
    changedCreditCardBank: function (ev) {
        var creditCardBankOther = this.$('#credit_card_bank').val();
        var credit_card = (this.$('#credit_card').is(":checked") ? 'yes' : 'no');

        if (credit_card == 'yes' && creditCardBankOther == 'other') {
            this.$('.custom_credit_card_bank_form_group').show();
        } else {
            this.$('.custom_credit_card_bank_form_group').fadeOut();
        }
    },
    onSubmit: function(){
		var that = this;
        that.validationError = [];
        
        if(this.$('.next-step').hasClass('disabled'))
            return false;

        this.$('.next-step').button('loading');

        var no_claim_discount_transfer =  (this.$('#no_claim_discount_transfer').is(":checked") ? 'yes':'no');
        var input_no_claim_discount_years = this.$('#input_no_claim_discount_years').val() || 0;
        var member_of_credit_union =  (this.$('#member_of_credit_union').is(":checked") ? 'yes':'no');
        var live_close_to_work =  (this.$('#live_close_to_work').is(":checked") ? 'yes':'no');
        var defensive_driver_cert =  (this.$('#defensive_driver_cert').is(":checked") ? 'yes':'no');
        var input_defensive_driver_school = this.$('#input_defensive_driver_school').val();
        var loan =  (this.$('#loan').is(":checked") ? 'yes':'no');
        var input_bank_name = this.$('#input_bank_name').val();
        var sole_driver = (this.$('#sole_driver').is(":checked") ? 'yes' : 'no');
        var credit_card = (this.$('#credit_card').is(":checked") ? 'yes' : 'no');
        var credit_card_bank = this.$('#credit_card_bank').val();
        var have_child = (this.$('#have_child').is(":checked") ? 'yes' : 'no');
        var custom_loan_bank = this.$('#custom_loan_bank').val();
        var credit_card_bank_custom = this.$('#custom_credit_card_bank').val();

 
        this.listenTo(that, 'saved', function (item) {
            //that.render;/
            //navigate next step
            console.info('views/driversexperience.js | goto next step');
            App.showPage('Vehicles', {
                id: item.get('quote_id')
            });
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.next-step').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, App.settings.errTimeout);
        });

        if (no_claim_discount_transfer == 'yes' && !input_no_claim_discount_years)
            that.validationError.push({ msg: 'Please enter the number of years you\'ve had no claim discount' });

        if (defensive_driver_cert == 'yes' && !input_defensive_driver_school)
            that.validationError.push({ msg: 'Please enter the name of the defensive driving school' });

        if (loan == 'yes' && !input_bank_name)
            that.validationError.push({ msg: 'Please enter the name of the bank in which you have received a loan' });

        if (loan == 'yes' && input_bank_name == 'other' && !custom_loan_bank)
            that.validationError.push({ msg: 'Please enter the name of the bank in which you have received a loan' });

        if (credit_card == 'yes' && !credit_card_bank)
            that.validationError.push({ msg: 'Please enter the name of the bank in which you have a credit card' });

        if (credit_card == 'yes' && credit_card_bank == 'other' && !credit_card_bank_custom)
            that.validationError.push({ msg: 'Please enter the name of the bank in which you have a credit card' });

        

        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            var item = this.model;

            item.set('no_claim_discount_transfer', no_claim_discount_transfer);
            item.set('no_claim_discount_years', input_no_claim_discount_years);
            item.set('member_of_credit_union', member_of_credit_union);
            item.set('live_close_to_work', live_close_to_work);
            item.set('defensive_driver_cert', defensive_driver_cert);
            item.set('defensive_driver_school', input_defensive_driver_school);
            item.set('loan', loan);
            item.set('bank_name', input_bank_name);
            item.set('sole_driver', sole_driver);
            item.set('credit_card', credit_card);
            item.set('credit_card_bank', credit_card_bank);
            item.set('have_child', have_child);
            item.set('custom_loan_bank', custom_loan_bank);
            item.set('credit_card_bank_custom', credit_card_bank_custom);
            

            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null,{
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}
        
		return false;
	}

});