﻿//service make.js
App.Views.Pages.ServiceMake = App.Views.Abstract.Page.extend({

    templateName: 'pages/service/service_make',
    additionalScripts: [
        '/vendors/node_modules/moment/min/moment.min.js',
        '/vendors/node_modules/moment-timezone/moment-timezone.js',
    ],
    category: 'service make',
    events: {
        "mouseenter .item": "moreMakeDetails",
        "mouseleave .item": "lessMakeDetails",
        "click .item": "toItem",
        "click .item_button_edit": "editItem",
        "click #add_make_small_button": "addMake",
        "click .item_button_remove": "removeItem",
        "click #search_makemodel_button": "searchMakeModel",
        "click #reset_makemodel_search_button": "reset_searchMakeModel",
        "click .toggle-btn": "toggleChevron"
    },
    title: function () {
        return 'Manage Vehicle Makes List';
    },
    url: function () {
        return 'service/make';
    },
    render: function () {
        console.log('views/pages/service_make.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });

        this.renderHTML({
            items: _.sortBy(this.collection.toJSON(), "make_name"),
            search_params: this.makemodel_search_params,
        });

        $('.helpericon').hide();
    },
    initializeParts: function () {
        console.info('views/pages/service.js | initializing parts');
        this.parts = [];
        this.partsInitialized = true;

        this.charts = [];
    },
    wakeUp: function () {
        console.log('views/pages/service make.js | waking up');
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            if (App.currentUser.isDemo())
                window.location.pathname = '/signin';

            that.render();
            that.listenTo(that.collection, 'sync add change reset remove', function() {
                that.render();
            });
        
            // that.reset_searchMakeModel();
        });
    },
    initialize: function () {
        console.log('service.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function () {
            if (App.currentUser.isDemo()) {
                window.location.pathname = '/signin';
            }
                
            that.collection = new App.Collections.CarMakes();

            that.on('addscriptloaded', that.render);
            that.listenTo(that.collection, 'sync add remove', function () {
                App.helper.loadAdditionalScripts(that.additionalScripts, function () {
                    if (!moment.tz.zone(App.settings.apptimezone.zonename))
                        moment.tz.add(App.settings.apptimezone.moment_tz_package);
                    moment.tz.setDefault(App.settings.apptimezone.zonename);
                    that.trigger('addscriptloaded');
                });
            });

            that.collection.fetch({reset: true}).done(function (res) {
                that.listenTo(that.collection, 'sync add change reset remove', function() {
                    that.render();
                });
            }).catch(function (error) {
                that.render();
            });

        });
    },
    moreMakeDetails: function (ev) {
        $(ev.currentTarget).find(".item_buttons").show();
    },
    lessMakeDetails: function (ev) {
        $(ev.currentTarget).find(".item_buttons").hide();
    },
    addMake: function(){
        App.showDialog('AddCarMake',{
            items: this.collection,
        });

        return false;
    },
    toggleChevron: function (ev) {
        ev.preventDefault();
        ev.stopImmediatePropagation();
        $($(ev.currentTarget).attr('href')).toggleClass('hidden'); //$($(ev.currentTarget).attr('href')).toggleClass('in');
        $(ev.currentTarget).children('span').first().toggleClass("glyphicon glyphicon-chevron-down");
        $(ev.currentTarget).children('span').first().toggleClass("glyphicon glyphicon-chevron-up");
    },
    toItem: function (ev) {
        // // ev.preventDefault();
        // // ev.stopImmediatePropagation();
        var data = $(ev.currentTarget).data();

        if (typeof (data.id) === 'undefined')
            return true;

        var id = parseInt(data.id, 10);
        var item = this.collection.get(id);

        if (!item)
            return true;

        App.showPage('ServiceModel', {
            item: item
        });

        return false;
    },
    editItem: function (ev) {
        console.log('views/service.js | Show Edit Vehicle Make Dalog');
        ev.preventDefault();
        ev.stopImmediatePropagation();
        
        var id = $(ev.currentTarget).parents('.item').data('id');
        var item = this.collection.get(id);

        App.showDialog('EditCarMake', {
            item: item
        });

        return false;
    },
    removeItem: function (ev) {
        ev.preventDefault();
        ev.stopImmediatePropagation();

        var id = $(ev.currentTarget).parents('.item').data('id');

        App.showDialog('RemoveListItem', {
            item: this.collection.get(id)
        });

        return false;
    },
    searchMakeModel: function (ev) {
        // this.$('#search_makemodel_button').addClass('disabled');
        // this.makemodel_search_params = this.makemodel_search_params || {};
        
        var car_make = this.$('#input_make').val();
        var car_model = this.$('#input_model').val();

        var data = {
            car_make: car_make,
            car_model: car_model
        };

        this.makemodel_search_params = {
            car_make: car_make,
            car_model: car_model
        };

        this.filterCustomised(
            data
        );

        return false;
    },
    reset_searchMakeModel: function () {
        this.makemodel_search_params = {
            car_make: '',
            car_model: ''
        };

        this.filterCustomised({
            data: {
                car_make: '',
                car_model: ''
            }
        });
    },
    filterCustomised: function (params) {
        console.log('service.js | customized filter applied');
        
        params = params || {};
    
        this.renderLoading();
        this.collection.fetch({ data: params, reset: true });
    },

});