﻿//immovablecharacteristics.js
App.Views.Pages.ImmovableCharacteristics = App.Views.Abstract.Page.extend({

    templateName: 'pages/quote/immovable/characteristics',
    category: 'immovablecharacteristics',
    events: {
        "change #input_wall_type": "wallTypeChanged",
        "change #input_roof_type": "roofTypeChanged",
        "change #input_floor_type": "floorTypeChanged",
        "submit form": "onSubmit",
    },
    title: function () {
        return 'Characteristics';
    },
    url: function () {
        if (typeof (this.model) != 'undefined' && this.model.id)
            return 'quotes/' + this.model.attributes.quote_id + '/immovables/' + this.model.id + '/characteristics';
    },
    render: function () {
        console.log('views/pages/immovablecharacteristics.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.on('render', function () {
            $('.inpage-form').validator();
        });

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });
        
        this.renderHTML({
            item: this.model.toJSON()
        });

        $('.helpericon').show();
    },
    initializeParts: function () {
        console.info('views/pages/immovablecharacteristics.js | initializing parts');
        this.parts = [];
        this.partsInitialized = true;
        this.charts = [];
    },
    wakeUp: function () {
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            that.render();
        });
    },
    initialize: function (params) {
        console.log('immovablecharacteristics.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function (user) {


            if (typeof (params.item) !== 'undefined') {
                that.model = params.item;
                that.render();
                that.listenTo(that.model, 'sync', that.render);
            } else if (typeof (params.id) !== 'undefined' && typeof (params.quote_id) !== 'undefined' ) {
                that.model = new App.Models.Immovable();
                that.model.id = params.id;
                that.model.attributes.quote_id = params.quote_id;

                that.listenTo(that.model, 'sync', that.render);
            
                that.model.fetch({
                    error: function () {
                        App.showPage('NotFound');
                    }
                });
            } else
                throw 'id or item parameters required';

        });

    },
    wallTypeChanged: function(){
        var wall_type = this.$('#input_wall_type').val();
        if(wall_type == 'other'){
            this.$('#wall_type_other_input_group').fadeIn();
            this.$('#input_wall_type_other').attr('data-validate',true);
        }else{
            this.$('#wall_type_other_input_group').fadeOut();
            this.$('#input_wall_type_other').attr('data-validate',false);
        }
        this.$('form.imm_characteristics').validator('update');
        this.$('#input_wall_type_other').trigger('focusout');
    },
    roofTypeChanged: function(){
        var roof_type = this.$('#input_roof_type').val();
        if(roof_type == 'other'){
            this.$('#roof_type_other_input_group').fadeIn();
            this.$('#input_roof_type_other').attr('data-validate',true);
        }else{
            this.$('#roof_type_other_input_group').fadeOut();
            this.$('#input_roof_type_other').attr('data-validate',false);
        }
        this.$('form.imm_characteristics').validator('update');
        this.$('#input_roof_type_other').trigger('focusout');
    },
    floorTypeChanged: function(){
        var floor_type = this.$('#input_floor_type').val();
        if(floor_type == 'other'){
            this.$('#floor_type_other_input_group').fadeIn();
            this.$('#input_floor_type_other').attr('data-validate',true);
        }else{
            this.$('#floor_type_other_input_group').fadeOut();
            this.$('#input_floor_type_other').attr('data-validate',false);
        }
        this.$('form.imm_characteristics').validator('update');
        this.$('#input_floor_type_other').trigger('focusout');
    },
    onSubmit: function(){
		var that = this;
        that.validationError = [];
        
        if(this.$('.next-step').hasClass('disabled'))
            return false;

        this.$('.next-step').button('loading');

        var on_hill = this.$('#input_on_hill').val();
        var wall_type = this.$('#input_wall_type').val();
        var wall_type_other = this.$('#input_wall_type_other').val();
        var floor_type = this.$('#input_floor_type').val();
        var floor_type_other = this.$('#input_floor_type_other').val();
        var seafront_waterway = this.$('#input_seafront_waterway').val();
        var roof_type = this.$('#input_roof_type').val();
        var roof_type_other = this.$('#input_roof_type_other').val();
        var proximity_to_nearest_building = this.$('#input_proximity_to_nearest_building').val();
        var building_age = this.$('#input_building_age').val();
        
        this.listenTo(that, 'saved', function (item) {
            //that.render;/
            //navigate next step
            console.info('views/immovablecharacteristics.js | goto next step');
            App.showPage('ImmovableSecurity', {
                item: item
            });
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.next-step').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, App.settings.errTimeout);
        });

        if (!on_hill)
            that.validationError.push({ msg: 'Please make an On Hill selection' });

        if (!seafront_waterway)
            that.validationError.push({ msg: 'Please make an Seafront or Waterway selection' });
        
        if (!wall_type)
            that.validationError.push({ msg: 'Please select a Wall Type' });

        if (wall_type == 'other' && !wall_type_other)
            that.validationError.push({ msg: 'Please enter Other Wall Type Material' });

        if (!roof_type)
            that.validationError.push({ msg: 'Please select a Roof Type' });

        if (roof_type == 'other' && !roof_type_other)
            that.validationError.push({ msg: 'Please enter Other Roof Type Material' });

        if (!floor_type)
            that.validationError.push({ msg: 'Please select a Floor Type' });

        if (floor_type == 'other' && !floor_type_other)
            that.validationError.push({ msg: 'Please enter Other Floor Type Material' });

        if (!proximity_to_nearest_building)
            that.validationError.push({ msg: 'Please make select Proximity to Nearest Building' });

        if (!building_age)
            that.validationError.push({ msg: 'Please make select Age of the Building' });
        

        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            var item = this.model;

            item.set('on_hill', on_hill);
            item.set('seafront_waterway', seafront_waterway);
            item.set('wall_type', wall_type);
            item.set('wall_type_other', wall_type_other);
            item.set('roof_type', roof_type);
            item.set('roof_type_other', roof_type_other);
            item.set('floor_type', floor_type);
            item.set('floor_type_other', floor_type_other);
            item.set('proximity_to_nearest_building', proximity_to_nearest_building);
            item.set('building_age', building_age);

            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null,{
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}
        
		return false;
	}

});