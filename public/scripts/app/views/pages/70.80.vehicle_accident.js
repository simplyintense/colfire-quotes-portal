﻿//vehicleaccident.js
App.Views.Pages.VehicleAccident = App.Views.Abstract.Page.extend({

    templateName: 'pages/quote/motor/accident',
    additionalScripts: [
        '/vendors/node_modules/moment/min/moment.min.js',
        '/vendors/node_modules/moment-timezone/moment-timezone.js',
        '/vendors/node_modules/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
    ],
    category: 'vehicleaccident',
    events: {
        "click .add-accident": "addAccident",
        "submit form": "onSubmit",
    },
    title: function () {
        return 'Accident';
    },
    url: function () {
        if (typeof (this.model) != 'undefined' && this.model.id)
            return 'quotes/' + this.model.attributes.quote_id + '/vehicles/' + this.model.id + '/accident';
    },
    render: function () {
        console.log('views/pages/vehicleaccident.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.on('render', function () {
            $('.inpage-form').validator();
        });

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });
        
        this.renderHTML({
            item: this.model.toJSON(),
            //settings: App.settings, 
            //years_list: App.settings.car_years_list()
            accidents: this.model.accidents.toJSON(),
        });

        $('.helpericon').show();
    },
    initializeParts: function () {
        console.info('views/pages/vehicleaccident.js | initializing parts');
        this.parts = [];
        
        this.accidents = this.model.getAccidents();
        this.listenTo(this.accidents, 'add update remove',this.render);
        
        this.parts.push(new App.Views.Parts.Accidents({
            id: 'accidents_container',
            model: this.model,
            collection: this.accidents,
        }));

        this.partsInitialized = true;
        this.charts = [];
    },
    wakeUp: function () {
        console.log('views/pages/vehicleaccident.js | waking up');
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            that.render();
            that.listenTo(that.model, 'change sync destroy', that.render);
            for (var k in that.parts)
                if (typeof (that.parts[k].wakeUp) === 'function')
                    that.parts[k].wakeUp();
            
            that.model.fetch();
        });
    },
    initialize: function (params) {
        console.log('vehicleaccident.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function (user) {


            if (typeof (params.item) !== 'undefined') {
                that.model = params.item;
                that.render();
                that.listenTo(that.model, 'sync', that.render);
            } else if (typeof (params.id) !== 'undefined' && typeof (params.quote_id) !== 'undefined' ) {
                that.model = new App.Models.Vehicle();
                that.model.id = params.id;
                that.model.attributes.quote_id = params.quote_id;

                that.listenTo(that.model, 'sync', that.render);
            
                that.model.fetch({
                    error: function () {
                        App.showPage('NotFound');
                    }
                });
            } else
                throw 'id or item parameters required';

        });

    },
    addAccident: function () {
        App.showDialog('AddAccident', {
            conntotable_model: this.model,
            collection: this.model.getAccidents(),
        });
        return false;
    },
    onSubmit: function(){
		var that = this;
        that.validationError = [];
        
        if(this.$('.next-step').hasClass('disabled'))
            return false;

        this.$('.next-step').button('loading');

        this.accidents = this.model.getAccidents();
        
        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.next-step').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, App.settings.errTimeout);
        });
        
        this.listenToOnce(that.accidents, 'sync', function (drivers) {
            
            if (!this.accidents || this.accidents.length == 0)
                that.validationError.push({ msg: 'Please add at least one accident' });

            if (that.validationError.length > 0) {
                that.trigger('invalid', that.validationError);
		    } else {
                //navigate next step
                console.info('views/accidents.js | goto next step');
                App.showPage('VehicleBenefits', {
                    item: that.model
                });
		    }
            
        });

        this.accidents.fetch();
        
		return false;
	}

});