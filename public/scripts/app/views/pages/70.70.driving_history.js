﻿//drivinghistory.js
App.Views.Pages.DrivingHistory = App.Views.Abstract.Page.extend({

    templateName: 'pages/quote/motor/drivinghistory',
    category: 'drivinghistory',
    events: {
        "submit form": "onSubmit",
    },
    title: function () {
        return 'Driving History';
    },
    url: function () {
        if (typeof (this.model) != 'undefined' && this.model.id)
            return 'quotes/' + this.model.attributes.quote_id + '/vehicles/' + this.model.id + '/drivinghistory';
    },
    render: function () {
        console.log('views/pages/drivinghistory.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.on('render', function () {
            $('.inpage-form').validator();
        });

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });
        
        this.renderHTML({
            item: this.model.toJSON(),
            settings: App.settings, 
            years_list: App.settings.car_years_list()
        });

        $('.helpericon').show();
    },
    initializeParts: function () {
        console.info('views/pages/drivinghistory.js | initializing parts');
        this.parts = [];
        this.partsInitialized = true;
        this.charts = [];
    },
    wakeUp: function () {
        console.log('views/pages/drivinghistory.js | waking up');
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            that.render();
            that.listenTo(that.model, 'change sync destroy', that.render);
            for (var k in that.parts)
                if (typeof (that.parts[k].wakeUp) === 'function')
                    that.parts[k].wakeUp();
            
            that.model.fetch();
        });
    },
    initialize: function (params) {
        console.log('drivinghistory.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function (user) {


            if (typeof (params.item) !== 'undefined') {
                that.model = params.item;
                that.render();
                that.listenTo(that.model, 'sync', that.render);
            } else if (typeof (params.id) !== 'undefined' && typeof (params.quote_id) !== 'undefined' ) {
                that.model = new App.Models.Vehicle();
                that.model.id = params.id;
                that.model.attributes.quote_id = params.quote_id;

                that.listenTo(that.model, 'sync', that.render);
            
                that.model.fetch({
                    error: function () {
                        App.showPage('NotFound');
                    }
                });
            } else
                throw 'id or item parameters required';

        });

    },
    onSubmit: function(){
		var that = this;
        that.validationError = [];
        
        if(this.$('.next-step').hasClass('disabled'))
            return false;

        this.$('.next-step').button('loading');

        var accidents_recently = this.$('input[type=radio][name=accidents_recently]:checked').val();
                
        this.listenTo(that, 'saved', function (item) {
            //that.render;/
            //navigate next step
            var nextPageName = '';
            console.info('views/driversexperience.js | goto next step');
            if(item.get('accidents_recently') == 'yes'){
                nextPageName = 'VehicleAccident';
            }else if(item.get('accidents_recently') == 'no'){
                nextPageName = 'VehicleBenefits';
            }

            App.showPage(nextPageName, {
                item: item
            });
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.next-step').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, App.settings.errTimeout);
        });

        if (!accidents_recently)
            that.validationError.push({ msg: 'Please make a choice' });
        

        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            var item = this.model;

            item.set('accidents_recently', accidents_recently);

            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null,{
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}
        
		return false;
	}

});