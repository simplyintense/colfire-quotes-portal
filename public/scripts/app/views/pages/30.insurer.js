﻿//insurer.js
App.Views.Pages.Insurer = App.Views.Abstract.Page.extend({

    templateName: 'pages/acquaintance/insurer',
    category: 'insurer',
    events: {
        'change input[type=radio][name=insurer]': 'changedInsurer',
        "submit form": "onSubmit",
    },
    title: function () {
        return 'Insurer';
    },
    url: function () {
        return 'insurer';
    },
    render: function () {
        console.log('views/pages/insurer.js | rendering');

        if (!this.partsInitialized)
            this.initializeParts();

        this.on('render', function () {
            $('.inpage-form').validator();
        });

        this.once('render', function () {
            for (var k in this.parts)
                this.parts[k].render();
            for (var k in this.charts)
                this.charts[k].render();
        });
        
        this.renderHTML({
            user: App.currentUser.toJSON(),
            item: this.model.toJSON()
        });

        $('.helpericon').show();

    },
    initializeParts: function () {
        console.info('views/pages/insurer.js | initializing parts');
        this.parts = [];
        this.partsInitialized = true;
        this.charts = [];
    },
    wakeUp: function () {
        this.holderReady = false;
        var that = this;
        this.requireSingedIn(function () {
            that.render();
        });
    },
    initialize: function () {
        console.log('insurer.js | initialize');
        this.renderLoading();

        var that = this;
        this.requireSingedIn(function (user) {
            that.model = App.currentUser.getProfile();
            that.render();
            that.listenTo(that.model, 'sync', that.render);
        });
    },
    changedInsurer: function(){
        var insurer = this.$('input[type=radio][name=insurer]:checked').val();
        if(insurer == 'other'){
            this.$('#other_insurer_input_group').show();
            this.$('.cur_cust_years_form_group').hide();
        }else{
            this.$('#other_insurer_input_group').hide();
            this.$('.cur_cust_years_form_group').show();
        }
    },
    onSubmit: function(){
		var that = this;
        that.validationError = [];

        if(this.$('.next-step').hasClass('disabled'))
            return false;

        this.$('.next-step').button('loading');

        var current_insurer = this.$('input[type=radio][name=insurer]:checked').val();
        var cur_cust_years = this.$('#cur_cust_years').val() || '';
        if(current_insurer == 'other')
            current_insurer = this.$('#input_current_insurer').val() || 'other';

        
        this.listenTo(that, 'saved', function (item) {
            //that.render;/
            //navigate next step
            console.info('views/insurer.js | goto next step');
            App.showPage('Insured', {
                item: App.currentUser.getProfile()
            });
        });

        this.listenTo(that, 'invalid', function (validationError) {
            var html = "";
            for (var k in validationError) html += validationError[k].msg + "<br>";
            this.$('.errors-container').html(html);
            this.$('.errors-container').slideDown();

            this.$('#input_customer_type').focus();
            this.$('.next-step').button('reset');
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, App.settings.errTimeout);
        });

        if (!current_insurer)
            that.validationError.push({ msg: 'Please select if you are Current customer' });
        if (!cur_cust_years && current_insurer == 'colfire')
            that.validationError.push({ msg: 'Please enter the number of years since you became our customer' });
        

        if (that.validationError.length > 0) {
            that.trigger('invalid', that.validationError);
		} else {
            var item = this.model; //App.currentUser;

            item.set('current_insurer', current_insurer);
            item.set('cur_cust_years', cur_cust_years);
            
            //trigger error if local model validation returns error
            this.listenTo(item, 'invalid', function (errors) {
                this.trigger('invalid', errors.validationError);
            });

            item.save(null,{
                success: function (model, data) {
                    if (typeof (data.id) !== 'undefined') {
                        that.trigger('saved', item);
                    }
                },
                error: function (model, response) {
                    //trigger error if api validation returns error
                    if (typeof (response.responseJSON) !== 'undefined' && typeof (response.responseJSON.message) !== 'undefined') {
                        if (!(that.validationError instanceof Array))
                            that.validationError = [];

                        if (typeof (response.responseJSON.message) === 'string') {
                            that.validationError.push({
                                msg: response.responseJSON.message
                            });
                        } else {
                            for (var k in response.responseJSON.message)
                                that.validationError.push({
                                    msg: response.responseJSON.message[k]
                                });
                        }
                    }
                    that.trigger('invalid', that.validationError);
                }
            });
		}

		return false;
	}

});