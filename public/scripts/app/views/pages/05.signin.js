// signin.js
App.Views.Pages.Signin = App.Views.Abstract.Page.extend({

    templateName: 'pages/index/signin',
    category: 'signin',
    events: {
        "submit form": "onSubmit",
    },
    title: function () {
        return 'Sign In';
    },
    url: function () {
        return 'signin';
    },
    render: function () {
        this.renderHTML({});
    },
    wakeUp: function () {
        if (typeof (App.currentUser) !== 'undefined' && App.currentUser && App.currentUser.isSignedIn()) {
            this.redirectTomainPage();
        } else {
            this.holderReady = false;
            this.render();
        }
    },
    initialize: function () {
        var that = this;
        if (typeof (App.currentUser) !== 'undefined' && App.currentUser && App.currentUser.isSignedIn()) {
            that.redirectTomainPage();
        }
        this.renderLoading();

        /// initialize models, collections etc. Request fetching from storage
        this.listenTo(App.currentUser, 'signedInStatusChanged', function () {
            that.redirectTomainPage();
        });

        this.render();
    },
    redirectTomainPage: function () {
        if (!App.currentUser.isDemo()) {
            //App.router.redirect('/dashboard/');
            //window.location.pathname = '/dashboard';
        } else {
            //App.router.redirect('/');
            //window.location.pathname = '/';
        }
    },
    onSubmit: function () {
        this.$('.btn-orange').button('loading');

        App.currentUser.clear();

        var username = this.$('#input_username').val();
        var password = this.$('#input_password').val();

        App.currentUser.set('login', username);
        App.currentUser.set('password', password);
        this.listenTo(App.currentUser, 'signedin', function () {
            this.$('.btn-orange').button('reset');
            //this.hide();
            if (!App.currentUser.isDemo()) {
                window.location.pathname = '/dashboard';
            }
        });
        this.listenTo(App.currentUser, 'invalid', function () {
            this.$('.btn-orange').button('reset');
            this.$('.errors-container').slideDown();
            this.$('#input_username').focus();
            var that = this;
            setTimeout(function () {
                that.$('.errors-container').slideUp();
            }, 2000);
        });

        App.currentUser.signIn(username, password);

        return false;
    },
    onResponse: function (user) {
        var that = this;
        if (user.isSignedIn()) {
            this.$('#signin_modal_form_submit').button('reset');
            //this.hide();
        } else {
            this.$('#signin_invalid_password_alert').slideDown();
            this.$('#signin_modal_form_submit').button('reset');
            this.$('#input_username').focus();

            setTimeout(function () {
                that.$('#signin_invalid_password_alert').slideUp();
            }, 1000);
        }
    }

});