// immovable.js
App.Models.Immovable = Backbone.Model.extend({
    url: function () {
        return App.settings.apiEntryPoint + 'quotes/' + this.get('quote_id') + '/immovables/' + (typeof (this.id) === 'undefined' ? '' : this.id);
    },
    getClaims: function () {
        if (typeof (this.claims) === 'undefined') {
            this.claims = new App.Collections.Claims();
            this.claims.setConnToTableParams('quotes/'+ this.get('quote_id') +'/immovables', this.id);
            var that = this;
            this.claims.fetch().done(function () { });
        }
        this.claims.setConnToTableParams('quotes/'+ this.get('quote_id') +'/immovables', this.id);
        return this.claims;
    },
});