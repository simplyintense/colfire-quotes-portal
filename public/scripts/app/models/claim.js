// claim.js
App.Models.Claim = Backbone.Model.extend({
    url: function () {
        return App.settings.apiEntryPoint + 'quotes/' + this.get('quote_id') + '/immovables/' + this.get('immovable_id') + '/claims/' + (typeof (this.id) === 'undefined' ? '' : this.id);
    },
    hide: function () {
        if (this.get('status') == 'active') {
            this.set('status', 'hidden');
            this.save();
        } else if (this.get('status') == 'hidden') {
            this.collection.remove(this);
            this.destroy();
        }
    },
});