// profile.js
App.Models.Profile = Backbone.Model.extend({
    url: function () {
        return App.settings.apiEntryPoint + 'users' + (typeof (this.get('user_id')) === 'undefined' ? '' : '/' + this.get('user_id') + '/profile');
    },
});