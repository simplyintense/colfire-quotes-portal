// accident.js
App.Models.Accident = Backbone.Model.extend({
    url: function () {
        return App.settings.apiEntryPoint + 'quotes/' + this.get('quote_id') + '/vehicles/' + this.get('vehicle_id') + '/accidents/' + (typeof (this.id) === 'undefined' ? '' : this.id);
    },
    hide: function () {
        if (this.get('status') == 'active') {
            this.set('status', 'hidden');
            this.save();
            this._coll = this.collection;
            this.collection.remove(this);
            this._coll.fetch();
        } else if (this.get('status') == 'hidden') {
            this.collection.remove(this);
            this.destroy();
        }
    },
});