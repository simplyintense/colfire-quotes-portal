// userinvite.js
App.Models.UserInvite = Backbone.Model.extend({
	url: function() {
		return App.settings.apiEntryPoint + 'users/invite';
	},
});