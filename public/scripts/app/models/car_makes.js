// car makes.js
App.Models.CarMakes = Backbone.Model.extend({
    url: function () {
        return App.settings.apiEntryPoint + 'service-lists/car-makes/'  + (typeof (this.id) === 'undefined' ? '' : this.id);
    },

});