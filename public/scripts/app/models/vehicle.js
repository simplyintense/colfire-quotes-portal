// vehicle.js
App.Models.Vehicle = Backbone.Model.extend({
    url: function () {
        return App.settings.apiEntryPoint + 'quotes/' + this.get('quote_id') + '/vehicles/' + (typeof (this.id) === 'undefined' ? '' : this.id);
    },
    getDrivers: function () {
        if (typeof (this.drivers) === 'undefined') {
            this.drivers = new App.Collections.Drivers();
            this.drivers.setConnToTableParams('quotes/'+ this.get('quote_id') +'/vehicles', this.id);
            var that = this;
            this.drivers.fetch().done(function () { });
        }
        this.drivers.setConnToTableParams('quotes/'+ this.get('quote_id') +'/vehicles', this.id);
        return this.drivers;
    },
    getAccidents: function () {
        if (typeof (this.accidents) === 'undefined') {
            this.accidents = new App.Collections.Accidents();
            this.accidents.setConnToTableParams('quotes/'+ this.get('quote_id') +'/vehicles', this.id);
            var that = this;
            this.accidents.fetch().done(function () { });
        }
        this.accidents.setConnToTableParams('quotes/'+ this.get('quote_id') +'/vehicles', this.id);
        return this.accidents;
    },
    hide: function () {
        if (this.get('status') == 'active') {
            this.set('status', 'hidden');
            this.save();
        } else if (this.get('status') == 'hidden') {
            this.collection.remove(this);
            this.destroy();
        }
    },
});