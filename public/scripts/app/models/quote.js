// quote.js
App.Models.Quote = Backbone.Model.extend({
    url: function () {
        return App.settings.apiEntryPoint + 'quotes' + (typeof (this.id) === 'undefined' ? '' : '/' + this.id);
    },
    hide: function () {
        if (this.get('status') == 'active') {
            this.set('status', 'hidden');
            this.save();
        } else if (this.get('status') == 'hidden') {
            this.destroy();
        }
    },
    getVehicles: function () {
        if (typeof (this.vehicles) === 'undefined') {
            this.vehicles = new App.Collections.Vehicles();
            this.vehicles.setConnToTableParams('quotes', this.id);
            var that = this;
            this.vehicles.fetch().done(function () { });
        }
        this.vehicles.setConnToTableParams('quotes', this.id);
        return this.vehicles;
    },
});