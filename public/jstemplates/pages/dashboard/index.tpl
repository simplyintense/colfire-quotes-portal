<div class="container">
    <ul class="breadcrumb">
      <li><a href="{$settings->site_path}">{tp}Home{/tp}</a></li>
      <li class="active">{tp}Quotes{/tp}</li>
    </ul>


    <div class="row">
        <div class="col-xs-12 col-md-9">

	        {if $items|count == 0}
		        {if $status|default:'active' == 'active'}

			        <div class="alert alert-warning" role="alert">
                {tp}You don't have any quotes yet{/tp}
              </div>

		        {else}

              <div class="alert alert-warning" role="alert">{tp}You have no deleted quotes{/tp}</div>

            {/if}
          {else}
          <div class="list-group wallet_item" id="customer_items">


            {foreach from=$items item=i}
            <a href="{$settings->site_path}/dashboard/quotes/{$i->id}" class="list-group-item item" data-id="{$i->id}" 
				        {if $i->status|default:'active' == 'hidden'}style="background: #eee"{/if}>

                        <!--
				        <div class="item_buttons hideme wallet_buttons">
					        {if $i->status|default:'active' == 'active'}
                                <button class="btn btn-default btn-xs item_button_remove"><span class="glyphicon glyphicon-trash"></span> {tp}Delete{/tp}</button>
					            <button class="btn btn-default btn-xs item_button_edit"><span class="glyphicon glyphicon-pencil"></span> {tp}Edit{/tp}</button>
					        {/if}
					        {if $i->status|default:'active' == 'hidden'}
					            <button class="btn btn-default btn-xs item_button_restore"><span class="glyphicon glyphicon-repeat"></span> {tp}Restore{/tp}</button>
					        {/if}
				        </div>
                        -->

                <h4 class="list-group-item-heading">
                    {$i->User.Profile.firstname|default:''} {$i->User.Profile.lastname|default:'-'}
                </h4>
                <h6>
                    <span class="label label-success">{$i->ref_num|escape:'html'|default:'no reference number'}</span>
                    <span class="label label-info">{$i->insurance_type|escape:'html'|default:'no reference number'}</span>
                    <span class="label label-warning">{frmtDate datetimeval=$i->createdAt|default:'' frmt=$settings->apptimezone.datetime_format}</span>
                </h6>

			        </a>
            {/foreach}

        <!--
            <nav aria-label="items-nav">
              <ul class="pager">
                <li class="previous disabled"><a href="#"><span aria-hidden="true">&larr;</span> Previous</a></li>
                <li class="next disabled"><a href="#">Next <span aria-hidden="true">&rarr;</span></a></li>
              </ul>
            </nav>
        -->

	    </div>	
	    {/if}

        </div>
		
        <div class="col-xs-12 col-md-3">

            <div class="panel panel-default">
                <div class="panel-heading">
				    <h3 class="panel-title">{tp}Search Quotes{/tp}
                    </h3>
			    </div>
		        <div class="panel-body">

                    <form method="post" action="{$settings->site_path}/quotes" role="form" id="quotes_search">
                        <fieldset>
                
                            <div class="form-group">
                                <label class="dialogtypesearch label label-lightgrey" for="input_ref_num">{t}Quote Reference Number{/t}</label>
                                <input type="text" name="input_ref_num" class="form-control" id="input_ref_num" 
                                        placeholder="{t}Quote Reference Number{/t}" value="{$search_params->ref_num|escape:'html'|default:''}">
                            </div>

                            <div class="form-group">
                                <label class="dialogtypesearch label label-lightgrey" for="input_firstname">{t}Customer First Name{/t}</label>
                                <input type="text" name="input_firstname" class="form-control" id="input_firstname" 
                                        placeholder="{t}Customer First Name{/t}" value="{$search_params->firstname|escape:'html'|default:''}">
                            </div>

                            <div class="form-group">
                                <label class="dialogtypesearch label label-lightgrey" for="input_lastname">{t}Customer Last Name{/t}</label>
                                <input type="text" name="input_lastname" class="form-control" id="input_lastname" 
                                        placeholder="{t}Customer Last Name{/t}" value="{$search_params->lastname|escape:'html'|default:''}">
                            </div>

                            <div class="form-group">
                                <label class="dialogtypesearch label label-lightgrey" for="input_email">{t}Customer Email{/t}</label>
                                <input type="text" name="input_email" class="form-control" id="input_email" 
                                        placeholder="{t}Customer Email{/t}" value="{$search_params->email|escape:'html'|default:''}">
                            </div>

                            <div class="form-group">
                                <label class="dialogtypesearch label label-lightgrey" for="input_phone_number">{t}Customer Phone{/t}</label>
                                <input type="text" name="input_phone_number" class="form-control" id="input_phone_number" 
                                        placeholder="{t}Customer Phone{/t}" value="{$search_params->phone_number|escape:'html'|default:''}">
                            </div>

                            <div class="alert alert-danger errors-container" style="display: none;">
                            </div>

                            <div class="form-group">
                                <input id="search_quotes_button" type="submit" class="btn btn-orange btn-block" value="{t}Search{/t}" data-loading-text="{t}Searching...{/t}">
                                {if $search_params}
                                    <div class="pull-right">{tp}or{/tp} <a href="#" class="action" id="reset_quotes_search_button">{tp}reset search{/tp}</a></div>
                                {/if}
                            </div>

                        </fieldset>
                    </form>
                </div>
	        </div>
               
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">{tp}Export Quotes{/tp}
					</h3>
				</div>
				<div class="panel-body">

					<form method="get" action="api/quotes/" role="form" id="quotes_search" data-toggle="validator" class="inpage-form">
						<fieldset>

							<div class="form-group">
								<label class="dialogtypesearch label label-lightgrey" for="datetimepicker_qexportstart">{t}From date{/t}</label>
								<div class='input-group date' id='datetimepicker_qexportstart'>
									<input type='text' name="startdate" class="form-control" id="startdate" required>
									<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar">
									</span>
									</span>
								</div>
							</div>
							<div class="form-group">
								<label class="dialogtypesearch label label-lightgrey" for="datetimepicker_qexportend">{t}To date{/t}</label>
								<div class='input-group date' id='datetimepicker_qexportend'>
									<input type='text' name="enddate" class="form-control" id="enddate" required>
									<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar">
									</span>
									</span>
								</div>
							</div>
							<div class="form-group">
								<label class="dialogtypesearch label label-lightgrey" for="export_type">{t}Export type{/t}</label>
									<select name="statquote" id="export_type" class="form-control" data-error="Please select export type." required>
										<option value="">Select</option>
										<option value="all">All Quotes</option>
										<option value="submitted">Submitted Quotes</option>
										<option value="active">Uncompleted Quotes</option>
									</select>
							</div>
							<div class="form-group">
								<label class="dialogtypesearch label label-lightgrey" for="export_QuoteType">{t}Export type{/t}</label>
									<select name="proptype" id="export_QuoteType" class="form-control" data-error="Please select export type." required>
										<option value="">Select</option>
										<option value="movable">Motor Quotations</option>
										<option value="immovable">Property Quotations</option>
									</select>
							</div>
							<div class="form-group">
								<input type='hidden' id="export_csv"  name="csv" value="yes">
								</select>
							</div>

							<div class="alert alert-danger errors-container" style="display: none;">
							</div>

							<div class="form-group">
								<input id="export_quotes_button" type="submit" class="btn btn-orange btn-block" value="{t}Export csv{/t}" data-loading-text="{t}Exporting...{/t}">
							</div>

						</fieldset>
					</form>
				</div>
			</div>
		</div>
    </div>
</div>