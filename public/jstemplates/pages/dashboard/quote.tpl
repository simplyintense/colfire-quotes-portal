<div class="container">
    <ul class="breadcrumb">
      <li><a href="{$settings->site_path}">{tp}Home{/tp}</a></li>
      <li><a href="{$settings->site_path}/dashboard">{tp}Dashboard{/tp}</a></li>
      <li class="active">{$item->ref_num|default:$item->id|escape:'html'}</li>
    </ul>


    <div class="row">

	    <div class="col-xs-12 col-sm-12 col-md-4 pull-right">
		
            <div class="panel panel-default">
			    <div class="panel-heading">
				    <h3 class="panel-title">Reference Number</h3>
			    </div>
			    <div class="panel-body">
			  
				    <p class="text-center text-success wallet_total"><strong>{$item->ref_num}</strong></p>
				
				    <!--<button type="button" id="add_building_unit_button_big" class="btn btn-success btn-block">{tp}Add Unit{/tp}</button>-->
				    <!--<div class="pull-right">{tp}or{/tp} <a href="#" class="action" id="manage_units_button">{tp}manage units{/tp}</a></div>-->
			    </div>
		    </div>
	
            <div class="panel panel-default">
			    <div class="panel-heading">
				    <h3 class="panel-title">{tp}Quote Details{/tp}
                      <!--
                      <div class="item_buttons wallet_buttons pull-right">
                        <button class="btn btn-default btn-xs" id="edit_building_button">
                          <span class="glyphicon glyphicon-pencil"></span> {tp}Edit{/tp}
                        </button>
                      </div>
                      -->
                    </h3>
			    </div>
			    <div class="panel-body">
                    <table class="table table-hover table-striped">
                      <tr>
                        <td><strong>{t}Status{/t}</strong></td>
                        <td>{$item->status}</td>
                      </tr>
                      <tr>
                        <td><strong>{t}Insurance Type{/t}</strong></td>
                        <td>{$item->insurance_type|default:'--'}</td>
                      </tr>
                      <tr>
                        <td><strong>{t}Created{/t}</strong></td>
                        <td>{frmtDate datetimeval=$item->createdAt|default:'' frmt=$settings->apptimezone.datetime_format}</td>
                      </tr>
                    </table>
			    </div>
		    </div>

            <div class="panel panel-default">
			    <div class="panel-heading">
				    <h3 class="panel-title">{tp}Customer Information{/tp}
                        <div class="btn-group">
                            <a type="button" id="toggle-customer-panel" class="btn toggler btn-default btn-xs btn-primary-nrg" href="#">
                              <span class="glyphicon glyphicon-chevron-up"></span>
                            </a>
                      </div>
                    </h3>
			    </div>
			    <div class="panel-body toggle-customer-panel">
                    <table class="table table-hover table-striped">
                        <tr>
                            <td><strong>{t}First Name{/t}</strong></td>
                            <td>{$item->User.Profile.firstname|default:'-'}</td>
                        </tr>
                        <tr>
                            <td><strong>{t}Last Name{/t}</strong></td>
                            <td>{$item->User.Profile.lastname|default:'-'}</td>
                        </tr>
                        <tr>
                            <td><strong>{t}Home Address{/t}</strong></td>
                            <td>{$item->User.Profile.home_address|default:'-'}</td>
                        </tr>
                        <tr>
                            <td><strong>{t}Email{/t}</strong></td>
                            <td>{$item->User.Profile.email|default:'-'}</td>
                        </tr>
                        <tr>
                            <td><strong>{t}Occupation{/t}</strong></td>
                            <td>{$item->User.Profile.occupation|default:'-'}</td>
                        </tr>
                        <tr>
                            <td><strong>{t}Phone Number{/t}</strong></td>
                            <td>{$item->User.Profile.phone_number|default:'-'}</td>
                        </tr>
                        <tr>
                            <td><strong>{t}Gender{/t}</strong></td>
                            <td>{$item->User.Profile.gender|default:'-'}</td>
                        </tr>
                        <tr>
                            <td><strong>{t}Date of Birth{/t}</strong></td>
                            <td>{frmtDate datetimeval=$item->User.Profile.dateofbirth|default:'' frmt=$settings->apptimezone.date_format}</td>
                        </tr>
                        <tr>
                            <td><strong>{t}Current Insurer{/t}</strong></td>
                            <td>{$item->User.Profile.current_insurer|default:'-'}</td>
                        </tr>
						<tr>
                            <td><strong>{t}Years a current customer{/t}</strong></td>
                            <td>{$item->User.Profile.cur_cust_years|default:'-'}</td>
                        </tr>
                    </table>
			    </div>
		    </div>


			<div class="panel panel-default">
			    <div class="panel-heading">
				    <h3 class="panel-title">{tp}Client feedback{/tp}
                      <!--
                      <div class="item_buttons wallet_buttons pull-right">
                        <button class="btn btn-default btn-xs" id="edit_building_button">
                          <span class="glyphicon glyphicon-pencil"></span> {tp}Edit{/tp}
                        </button>
                      </div>
                      -->
                    </h3>
			    </div>
			    <div class="panel-body">
                    <table class="table table-hover table-striped">
                      <tr>
                        <td><strong>{t}Rating{/t}</strong></td>
                        <td>{if $item->rating|default:0 > 0}
								{$item->rating|default:'--'} star(s)
							{else}
								Not rated
							{/if}
						</td>
                      </tr>
                      <tr>
                        <td><strong>{t}Feedback{/t}</strong></td>
                        <td>{$item->ratingFeedback|default:'--'}</td>
                      </tr>
                    </table>
			    </div>
		    </div>

        

      </div>

  
	    <div class="col-xs-12 col-sm-12 col-md-8 pull-left">
	
            <!-- ------------------------------------------- VEHICLES --------------------------------------------------- -->  
            {if $item->Vehicles|count > 0}
			    {foreach from=$item->Vehicles item=t}
			        <div class="panel panel-info">
			            <div class="panel-heading">
				            <h3 class="panel-title">Vehicle {$t@iteration}: {$t->make} {$t->model}
                                <div class="btn-group">
                                    <a type="button" id="toggle-{$t->id}-panel" class="btn toggler btn-default btn-xs btn-primary-nrg" href="#">
                                      <span class="glyphicon glyphicon-chevron-up"></span>
                                    </a>
                              </div>
                            </h3>
			            </div>
			            <div class="panel-body toggle-{$t->id}-panel">
                            <table class="table quote-tbl">
                                <tr>
                                    <th class="col-md-4"><strong class="text-orange">{t}Driver Information:{/t}</strong></th>
                                    <th class="col-md-4"></th>
                                </tr>
                                {if $t->Drivers|count > 0}
			                        {foreach from=$t->Drivers item=d}
                                        <tr>
                                            <td><strong>{t}Driver{/t} {$d@iteration}</strong></td>
                                            <td>{$d->firstname|default:''} {$d->lastname|default:''}</td>
                                        </tr>
										<tr>
                                            <td><strong>{t}Years an active driver{/t}</strong></td>
                                            <td>{$d->driving_experience|default:'-'}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>{t}Is the driver under the age of 25?{/t}</strong></td>
                                            <td>{$settings.yes_no_list[$d->young_driver_age]|default:'-'}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>{t}Is driver's permit less than 24 months old?{/t}</strong></td>
                                            <td>{$settings.yes_no_list[$d->new_driver_permit]|default:'-'}</td>
                                        </tr>
                                    {/foreach}
		                        {else}
			                        <!-- no drivers in this vehicle -->
		                        {/if}
                                <tr>
                                    <td><strong>{t}Motor insurance type{/t}</strong></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Comprehensive{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->insurance_type_comprehensive]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Third party fire and theft{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->insurance_type_third_party_fire_theft]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Third party{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->insurance_type_third_party]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}How do you plan to use vehicle?{/t}</strong></td>
                                    <td>{$settings.car_usage_types[$t->usage_type]|default:'-'}</td>
                                </tr>



                                <tr>
                                    <th><strong class="text-orange"><br>{t}Vehicle Information:{/t}</strong></th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td><strong>{t}Make{/t}</strong></td>
                                    <td>{$t->make|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Model{/t}</strong></td>
                                    <td>{$t->model|default:'-'}</td>
                                </tr>
								{if $t->model == 'Other'}
								<tr>
                                    <td><strong>{t}Custom make and model description{/t}</strong></td>
                                    <td>{$t->custom_descr_make_model|default:'-'}</td>
                                </tr>
								{/if}
                                <tr>
                                    <td><strong>{t}Year of manufacture{/t}</strong></td>
                                    <td>{$t->year|default:'-'}</td>
                                </tr>
								<tr>
                                    <td><strong>{t}Vehicle registration number{/t}</strong></td>
                                    <td>{$t->vehicle_registration_number|default:'-'}</td>
                                </tr>

								{if $t->insurance_type_third_party_fire_theft|default:'no' == 'yes' or  $t->insurance_type_comprehensive|default:'no' == 'yes'}
                                <tr>
                                    <td><strong>{t}Value of vehicle{/t}</strong></td>
                                    <td>${$t->current_value|default:'-'}</td>
                                </tr>
								{/if}

								<tr>
                                    <td><strong>{t}Type of origination{/t}</strong></td>
                                    <td>{$settings.vehicle_origin[$t->vehicle_origin]|default:'-'}</td>
                                </tr>
								<tr>
                                    <td><strong>{t}Is the vehicle kept overnight in a locked garage or fenced property?{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->vehicle_protected]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Accidents in the last 5 years?{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->accidents_recently]|default:'-'}</td>
                                </tr>


                                {if $t->accidents_recently|default:'no' == 'yes'}
									{if $t->Accidents|count > 0}
										{foreach from=$t->Accidents item=e}
											<tr>
												<th><strong class="text-orange"><br>{t}Accident history:{/t} {$e@iteration}</strong></th>
												<th></th>
											</tr>
											<tr>
												<td><strong>{t}Last accident date{/t}</strong></td>
												<td>{frmtDate datetimeval=$e->accidentdate|default:'' frmt=$settings->apptimezone.date_format}</td>
											</tr>
											<tr>
												<td><strong>{t}Were you deemed responsible{/t}</strong></td>
												<td>{$settings.yes_no_list[$e->you_deemed_responsible]|default:'-'}</td>
											</tr>
											<tr>
												<td><strong>{t}How much was paid in total to you/third party?{/t}</strong></td>
												<td>${$e->accident_cost|default:'-'}</td>
											</tr>
											<tr>
												<td><strong>{t}Description of the accident{/t}</strong></td>
												<td>{$e->accident_description|default:'-'}</td>
											</tr>
										{/foreach}
									{else}
									<!-- no vehicle accidents -->
									{/if}
                                {/if}


								{if $t->insurance_type_third_party|default:'no' == 'yes'}
									<tr>
										<th><strong class="text-orange"><br>{t}Additional benefits (Third Party):{/t}</strong></th>
										<th></th>
									</tr>
									<tr>
										<td><strong>{t}Windscreen Coverage{/t}</strong></td>
										<td>{$settings.yes_no_list[$t->windscreen_coverage_3rd_party]|default:'-'}</td>
									</tr>
									{if $t->windscreen_coverage_3rd_party == 'yes'}
									<tr>
										<td><strong>{t}Windscreen Coverage Amount{/t}</strong></td>
										<td>${$t->windscreen_coverage_amount_3rd_party|default:'-'}</td>
									</tr>
									{/if}
									<tr>
										<td><strong>{t}Emergency Roadside Assist{/t}</strong></td>
										<td>{$settings.yes_no_list[$t->emergency_roadside_assist_3rd_party]|default:'-'}</td>
									</tr>
								{/if}

								{if $t->insurance_type_third_party_fire_theft|default:'no' == 'yes'}
									<tr>
										<th><strong class="text-orange"><br>{t}Additional benefits (Third Party Fire & Theft):{/t}</strong></th>
										<th></th>
									</tr>
									<tr>
										<td><strong>{t}Windscreen Coverage{/t}</strong></td>
										<td>{$settings.yes_no_list[$t->windscreen_coverage_3rd_party_fire]|default:'-'}</td>
									</tr>
									{if $t->windscreen_coverage_3rd_party_fire == 'yes'}
									<tr>
										<td><strong>{t}Windscreen Coverage Amount{/t}</strong></td>
										<td>${$t->windscreen_coverage_amount_3rd_party_fire|default:'-'}</td>
									</tr>
									{/if}
									<tr>
										<td><strong>{t}Emergency Roadside Assist{/t}</strong></td>
										<td>{$settings.yes_no_list[$t->emergency_roadside_assist_3rd_party_fire]|default:'-'}</td>
									</tr>
								{/if}

								{if $t->insurance_type_comprehensive|default:'no' == 'yes'}
									<tr>
										<th><strong class="text-orange"><br>{t}Additional benefits (Comprehensive):{/t}</strong></th>
										<th></th>
									</tr>
									<tr>
										<td><strong>{t}Windscreen Coverage{/t}</strong></td>
										<td>{$settings.yes_no_list[$t->windscreen_coverage]|default:'-'}</td>
									</tr>
									{if $t->windscreen_coverage == 'yes'}
									<tr>
										<td><strong>{t}Windscreen Coverage Amount{/t}</strong></td>
										<td>${$t->windscreen_coverage_amount|default:'-'}</td>
									</tr>
									{/if}
									<tr>
										<td><strong>{t}Emergency Roadside Assist{/t}</strong></td>
										<td>{$settings.yes_no_list[$t->emergency_roadside_assist]|default:'-'}</td>
									</tr>

									<tr>
										<td><strong>{t}Flood Special Perils{/t}</strong></td>
										<td>{$settings.yes_no_list[$t->flood_special_perils]|default:'-'}</td>
									</tr>
									<tr>
										<td><strong>{t}Partial Waiver of Excess{/t}</strong></td>
										<td>{$settings.yes_no_list[$t->partial_waiver_of_excess]|default:'-'}</td>
									</tr>
									<tr>
										<td><strong>{t}Loss of Use{/t}</strong></td>
										<td>{$settings.yes_no_list[$t->loss_of_use]|default:'-'}</td>
									</tr>
									<tr>
										<td><strong>{t}New for Old{/t}</strong></td>
										<td>{$settings.yes_no_list[$t->new_for_old]|default:'-'}</td>
									</tr>
								{/if}

                                
                                <tr>
                                    <th><strong class="text-orange"><br>{t}Discounts information:{/t}</strong></th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td><strong>{t}Do you have a no claim discount that will be transferring from another policy?{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->no_claim_discount_transfer]|default:'-'}</td>
                                </tr>
                                {if $t->no_claim_discount_transfer == 'yes'}
                                <tr>
                                    <td><strong>{t}Number of years you've had a no claim discount{/t}</strong></td>
                                    <td>{$t->no_claim_discount_years|default:'-'}</td>
                                </tr>
                                {/if}
                                <tr>
                                    <td><strong>{t}Do you currently hold a membership at a Credit Union?{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->member_of_credit_union]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Do you live within 20km of your work?{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->live_close_to_work]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Do you have a defensive driving certificate?{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->defensive_driver_cert]|default:'-'}</td>
                                </tr>
                                {if $t->defensive_driver_cert == 'yes'}
                                <tr>
                                    <td><strong>{t}Name of the defensive driving school{/t}</strong></td>
                                    <td>{$t->defensive_driver_school|default:'-'}</td>
                                </tr>
                                {/if}
                                <tr>
                                    <td><strong>{t}Do you currently hold a loan on this vehicle?{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->loan]|default:'-'}</td>
                                </tr>
                                {if $t->loan == 'yes'}
                                <tr>
                                    <td><strong>{t}Bank name{/t}</strong></td>
                                    <td>{$settings.loan_bank_vehicle[$t->bank_name]|default:'-'}</td>
                                </tr>
									{if $t->bank_name == 'other'}
									<tr>
										<td><strong>{t}Custom bank name{/t}</strong></td>
										<td>{$t->custom_loan_bank|default:'-'}</td>
									</tr>
									{/if}
                                {/if}


								<tr>
                                    <td><strong>{t}Would you pay with a credit card?{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->credit_card]|default:'-'}</td>
                                </tr>
                                {if $t->credit_card == 'yes'}
                                <tr>
                                    <td><strong>{t}Credit Card Bank name{/t}</strong></td>
                                    <td>{$settings.credit_card_bank[$t->credit_card_bank]|default:'-'}</td>
                                </tr>
									{if $t->credit_card_bank == 'other'}
									<tr>
										<td><strong>{t}Custom Credit Card Bank name{/t}</strong></td>
										<td>{$t->credit_card_bank_custom|default:'-'}</td>
									</tr>
									{/if}
                                {/if}

                                <tr>
                                    <td><strong>{t}Would this be only you driving the vehicle?{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->sole_driver]|default:'-'}</td>
                                </tr>
                                
								<tr>
                                    <td><strong>{t}Do you have a child under 4 years?{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->have_child]|default:'-'}</td>
                                </tr>
                                
                            </table>
			            </div>
		            </div>

			    {/foreach}
		    {else}
			    <!-- no vehicles in this quote -->
		    {/if}
            <!-- ------------------------------------------- /VEHICLES --------------------------------------------------- -->

            <!-- ------------------------------------------- PROPERTY --------------------------------------------------- -->
            {if $item->Immovables|count > 0}
			    {foreach from=$item->Immovables item=t}
                    <div class="panel panel-info">
			            <div class="panel-heading">
				            <h3 class="panel-title">Property {$t@iteration}: {$t->address_line_1} {$t->address_line_2} {$t->address_line_3} {$t->city}
                                <div class="btn-group">
                                    <a type="button" id="toggle-{$t->id}-panel" class="btn toggler btn-default btn-xs btn-primary-nrg" href="#">
                                      <span class="glyphicon glyphicon-chevron-up"></span>
                                    </a>
                              </div>
                            </h3>
			            </div>
			            <div class="panel-body toggle-{$t->id}-panel">
                            <table class="table quote-tbl">
                                <tr>
                                    <th class="col-md-4"><strong class="text-orange">{t}Property Information:{/t}</strong></th>
                                    <th class="col-md-4"></th>
                                </tr>
                                <tr>
                                    <td><strong>{t}Address line 1{/t}</strong></td>
                                    <td>{$t->address_line_1|default:'-'}</td>
                                </tr>
                                {if $t->address_line_2}
                                <tr>
                                    <td><strong>{t}Address line 2{/t}</strong></td>
                                    <td>{$t->address_line_2|default:'-'}</td>
                                </tr>
                                {/if}
                                {if $t->address_line_3}
                                <tr>
                                    <td><strong>{t}Address line 3{/t}</strong></td>
                                    <td>{$t->address_line_3|default:'-'}</td>
                                </tr>
                                {/if}
                                <tr>
                                    <td><strong>{t}City{/t}</strong></td>
                                    <td>{$t->city|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Property insurance type{/t}</strong></td>
                                    <td>{$settings.immovable_insurance_types[$t->insurance_type]|default:'-'}</td>
                                </tr>

								{if $t->insurance_type && $t->insurance_type !== 'contents_only'}
								<tr>
                                    <td><strong>{t}Building insurance value{/t}</strong></td>
                                    <td>${$t->building_value|default:'-'}</td>
                                </tr>
								{/if}

								{if $t->insurance_type && $t->insurance_type !== 'building_only'}
								<tr>
                                    <td><strong>{t}Contents insurance value{/t}</strong></td>
                                    <td>${$t->contents_value|default:'-'}</td>
                                </tr>
								{/if}

                                <tr>
                                    <td><strong>{t}How do you plan to use property?{/t}</strong></td>
                                    <td>{$settings.immovable_usage_types[$t->usage_type]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Property on a hill?{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->on_hill]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Seafront of riverfront?{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->seafront_waterway]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Wall type{/t}</strong></td>
                                    <td>{$settings.wall_types[$t->wall_type]|default:'-'}</td>
                                </tr>
                                {if $t->wall_type_other}
                                <tr>
                                    <td><strong>{t}Wall type other{/t}</strong></td>
                                    <td>{$t->wall_type_other|default:'-'}</td>
                                </tr>
                                {/if}
                                <tr>
                                    <td><strong>{t}Roof type{/t}</strong></td>
                                    <td>{$settings.roof_types[$t->roof_type]|default:'-'}</td>
                                </tr>
                                {if $t->roof_type_other}
                                <tr>
                                    <td><strong>{t}Roof type other{/t}</strong></td>
                                    <td>{$t->roof_type_other|default:'-'}</td>
                                </tr>
                                {/if}
                                <tr>
                                    <td><strong>{t}Floor type{/t}</strong></td>
                                    <td>{$settings.floor_types[$t->floor_type]|default:'-'}</td>
                                </tr>
                                {if $t->floor_type_other}
                                <tr>
                                    <td><strong>{t}Floor type other{/t}</strong></td>
                                    <td>{$t->floor_type_other|default:'-'}</td>
                                </tr>
                                {/if}
                                <tr>
                                    <td><strong>{t}Proximity to nearest building{/t}</strong></td>
                                    <td>{$settings.proximity_to_nearest_building_types[$t->proximity_to_nearest_building]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Age of the building{/t}</strong></td>
                                    <td>{$settings.building_age_types[$t->building_age]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Fire extinguishers{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->fire_extinguishers]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Hose reels{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->hose_reels]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Sprinkler system{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->sprinkler_system]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Smoke detectors{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->smoke_detectors]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Alarms{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->alarms]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Security personnel{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->security_personnel]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Deadbolts / Padlocks{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->deadbolts_padlocks]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Gated community{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->gated_community]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Enclosed / Fenced{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->gated_property]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Burglar proofing{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->burglar_proofing]|default:'-'}</td>
                                </tr>
                                {if $t->claims_history}
                                <tr>
                                    <td><strong>{t}Claims history{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->claims_history]|default:'-'}</td>
                                </tr>
                                {/if}
                                {if $t->Claims|count > 0}
			                        {foreach from=$t->Claims item=d}
                                        <tr>
                                            <td><strong>{t}Claim{/t} {$d@iteration}</strong></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><strong>{t}Claim date{/t}</strong></td>
                                            <td>{frmtDate datetimeval=$d->claim_date|default:'' frmt=$settings->apptimezone.date_format}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>{t}Claim estimated value{/t}</strong></td>
                                            <td>{$d->estimated_value|default:''}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>{t}Claim description{/t}</strong></td>
                                            <td>{$d->claims_history_descr|default:''}</td>
                                        </tr>
                                    {/foreach}
		                        {else}
			                        <!-- no claims in this vehicle -->
		                        {/if}
                                
                                <tr>
                                    <th class="col-md-4"><strong class="text-orange"><br>{t}Do you want to include the following benefits?{/t}</strong></th>
                                    <th class="col-md-4"></th>
                                </tr>
                                <tr>
                                    <td><strong>{t}Flood{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->flood]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Subsidence and Landslip{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->subsidence_and_landslip]|default:'-'}</td>
                                </tr>

								{if $t->subsidence_and_landslip == 'yes'}
								<tr>
                                    <td><strong>{t}Retaining wall{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->subsidence_and_landslip_retaining_wall]|default:'-'}</td>
                                </tr>
								{/if}

								{if $t->subsidence_and_landslip == 'yes' && $t->subsidence_and_landslip_retaining_wall == 'yes'}
								<tr>
                                    <td><strong>{t}Retaining wall insured amount{/t}</strong></td>
                                    <td>${$t->subsidence_and_landslip_insured|default:'-'}</td>
                                </tr>
								{/if}
                                <tr>
                                    <td><strong>{t}Damage to external radio and TV aerials{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->damage_to_external_radio_and_tv_aerials]|default:'-'}</td>
                                </tr>

                                <tr>
                                    <td><strong>{t}Damage to underground water,gas pipes or electricity cables{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->damage_to_undergr_water_gas_pipes_or_electr_cables]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Workmen compensation for any domestic workers{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->workmen_compens_any_domestic_workers]|default:'-'}</td>
                                </tr>
                                <tr>
                                    <td><strong>{t}Removal of debris{/t}</strong></td>
                                    <td>{$settings.yes_no_list[$t->removal_debris]|default:'-'}</td>
                                </tr>
                                
                                
                            </table>
                        </div>
		            </div>
                {/foreach}
		    {else}
			    <!-- no properties in this quote -->
		    {/if}
            <!-- ------------------------------------------- PROPERTY --------------------------------------------------- -->


	    </div>

  
	    <div class="col-xs-12 col-sm-12 col-md-4 pull-right">

		    <div id="plans_container">
		    </div>
      <!--
		    <div class="panel panel-default" id="balance_canvas_container">
			    <div class="panel-heading">
				    <h3 class="panel-title">{tp}Consumptions per day trends{/tp}</h3>
			    </div>
			    <div class="panel-body">
				    <div id="balance_canvas" class="ct-chart" ></div>
			    </div>
		    </div>
	    -->
      </div>


    </div>
</div>