
<div class="container">    
    <div class="centered-wrapper">
        
        <div class="row row-eq-height row-contents-center">
            <div class="col-xs-12">
                <h1 class="heading-md heading-bold ">Are you ready to start your insurance journey?!</h1>
                <p class="heading-sm mb-50">Tell me are you a:</p>

                <form data-toggle="validator" role="form" class="inpage-form">

                    <fieldset>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                            <div class="form-group">
                    	        
                                    <div class="custom-radios div-fixed-width text-left">
                                        <div>
                                        <input type="radio" id="cur_cust" class="color-5" name="insurer" value="colfire" data-error="Please select if you are Current customer." required {if $item->current_insurer == 'colfire'}checked{/if}>
                                        <label for="cur_cust">
                                            <span></span>
                                            Current Customer
                                        </label>
                                        </div>
                                    </div>

                                    <div class="custom-radios div-fixed-width text-left">
                                        <div>
                                        <input type="radio" id="new_cust" class="color-5" name="insurer" value="other" data-error="Please select if you are Current customer." required {if $item->current_insurer != 'colfire' && $item->current_insurer|default:'' != ''}checked{/if}>
                                        <label for="new_cust">
                                            <span></span>
                                            New Customer
                                        </label>
                                        </div>
                                    </div>
                                    
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors fixed-height"></div>
                            </div>

							
                            <div class="col-xs-12 spacer-lg mb-20">

                                <div id="cur_cust_years_form_group" class="cur_cust_years_form_group" {if $item->current_insurer|default:'other' !== 'colfire'}style="display:none"{/if}>
                                    <p class="heading-sm mb-20">So happy to hear that! How long have you been with us?</p>
                                
                                    <div class="form-group has-feedback">
                    	                <input type="number" step="1" min="1" class="form-control input-lg" id="cur_cust_years" placeholder="Number of years" value="{$item->cur_cust_years|default:''}"
                                    
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        <div class="help-block fixed-height"></div>
                                    </div>
                                </div>

								<div id="other_insurer_input_group" {if $item->current_insurer|default:'colfire' == 'colfire'}style="display:none"{/if}>
                                    <p class="heading-sm mb-20">Tell us the name of your current/previous insurer</p>
                                
                                    <div class="form-group has-feedback">
                    	                <input type="text" class="form-control input-lg" id="input_current_insurer" placeholder="Insurer Name" value="{if $item->current_insurer|default:'colfire' != 'colfire'}{$item->current_insurer|default:''}{/if}">
                                    
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        <div class="help-block fixed-height">* If not applicable click Next</div>
                                    </div>
                                </div>

                            </div>

                            </div>
                        </div>
                    </fieldset>


                    <div class="form-group">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                    </div>

                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                  
                </form>


            </div>

        </div>
       
    </div>
</div>











