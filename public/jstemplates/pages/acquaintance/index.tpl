
<div class="container">    
    <div class="centered-wrapper">
        <div class="row row-eq-height row-contents-center no-gutter">
            <div class="col-xs-5 text-right visible-xs">
                <img src="/images/guy-xs.png" class="fit-image-xs" alt="Max">
            </div>
            <div class="col-xs-7 col-sm-12">
                <h1 class="heading-md-smaller-xs-screen heading-md heading-bold text-xs-left">
                    Hi, I'm Max! 
                    <span class="wrap-xs-col"></span>
                    What's your 
                    <span class="wrap-xs-col"></span>
                    name?
                </h1>
            </div>
        </div>
        <div class="row row-eq-height row-contents-center">
            <div class="col-sm-4 col-sm-offset-2 hidden-xs">
                <img src="/images/guy.png" class="img-fluid" alt="Max">
            </div>
            <div class="col-sm-3 col-xs-12">

                <form data-toggle="validator" role="form" class="inpage-form">
                    
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control input-lg" id="input_firstname" placeholder="First Name" value="{$item->firstname|default:''}"
                               minlength="2" pattern="[a-zA-Z '-]+" data-error="Please enter 2 or more letters." required>
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors fixed-height"></div>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control input-lg" id="input_lastname" placeholder="Last Name" value="{$item->lastname|default:''}"
                               minlength="2" pattern="[a-zA-Z '-]+" data-error="Please enter 2 or more letters." required>
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors fixed-height"></div>
                    </div>

                    

                    <div class="form-group">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                    </div>
                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                  
                </form>

            </div>

        </div>
       
    </div>
</div>











