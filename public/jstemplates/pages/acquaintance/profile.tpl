
<div class="container">    
    <div class="centered-wrapper">
        <h1 class="heading-md heading-bold">Great, just a few more questions about you</h1>
            <p class="heading-xs mb-20">Also, please ensure your contact information is accurate as I would need to contact you about this quote</p>
				
        <div class="row row-contents-center">
            
            <form data-toggle="validator" role="form" class="inpage-form">

                <div class="col-sm-3 col-sm-offset-3">
                
                    <div class="form-group has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon" id="map-icon-addon"><i class="fa fa-map-marker fa-lg"></i></span>
                            <input type="text" class="form-control input-lg" id="input_home_address" placeholder="Home Address" value="{$item->home_address|default:''}" aria-describedby="map-icon-addon"
                                minlength="2" data-error="Please enter 2 or more letters." required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="help-block with-errors fixed-height"></div>
                    </div>

                    <div class="form-group has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon" id="mail-icon-addon"><i class="fa fa-envelope fa-lg"></i></span>
                            <input type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]+$" class="form-control input-lg" id="input_email" placeholder="Email" value="{$item->email|default:''}" aria-describedby="mail-icon-addon"
                                data-error="Please enter correct email." required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="help-block with-errors fixed-height"></div>
                    </div>

                    <div class="form-group has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon" id="job-icon-addon"><i class="fa fa-briefcase fa-lg"></i></span>
                            <input type="text" class="form-control input-lg" id="input_occupation" placeholder="Occupation" value="{$item->occupation|default:''}" aria-describedby="job-icon-addon"
                                minlength="2" data-error="Please enter 2 or more letters." required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="help-block with-errors fixed-height"></div>
                    </div>

                </div>
                <div class="col-sm-3 col-xs-12 mb-50">

                    <div class="form-group has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon" id="phone-icon-addon"><i class="fa fa-phone fa-lg"></i></span>
                            <input type="phone" class="form-control input-lg" id="input_phone_number" placeholder="Phone number" value="{$item->phone_number|default:''}" aria-describedby="phone-icon-addon"
                                 pattern='[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]' data-error="Please enter in format 555-5555" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="help-block with-errors fixed-height"></div>
                    </div>

                    <div class="form-group has-feedback">
                        <div class="input-group" >
                            <span class="input-group-addon" id="dob-icon-addon"><i class="fa fa-birthday-cake fa-lg"></i></span>
                            <input id='datetimepicker' type="text" class="form-control input-lg" id="input_dateofbirth" placeholder="Date of Birth" aria-describedby="dob-icon-addon" 
                                   data-error="Select Date of Birth. Accepted 18+.." required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="help-block with-errors fixed-height"></div>
                    </div>

					<div class="form-group has-feedback">
						<div class="input-group">
                            <span class="input-group-addon double-icon" id="gender-icon-addon"><i class="fa fa-male fa-lg"></i><i class="fa fa-female fa-lg"></i></span>
                            <select name="input_gender" id="input_gender" class="form-control input-lg" data-error="Please make a selection." required>
								<option value="">Gender</option>
								{foreach from=$settings.gender_types item=c key=id}
								<option value="{$id}" {if $item->gender == $id}selected="selected"{/if}>{$c}</option>
								{/foreach}
							</select>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="help-block with-errors fixed-height"></div>
                    </div>

                </div>

                <div class="col-sm-12">
                    
                    <div class="form-group">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                    </div>

                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                  
                </div>

            </form>

        </div>
       
    </div>
</div>











