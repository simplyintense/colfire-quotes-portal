
<div class="container">    
    <div class="centered-wrapper">
        
        <div class="row row-eq-height row-contents-center">
            <div class="col-sm-2 col-sm-offset-1 hidden-xs">
                <img src="/images/guy.png" class="img-fluid" alt="Max">
            </div>
            <div class="col-sm-8 col-xs-12">
                <div class="row row-eq-height row-contents-center no-gutter">
                    <div class="col-xs-4 text-right visible-xs">
                        <img src="/images/guy-xs.png" class="fit-image-xs" alt="Max">
                    </div>
                    <div class="col-xs-8 col-sm-12 text-xs-left">
                        <h1 class="heading-md heading-md-smaller-xs-screen heading-bold ">
                            {$item->firstname|default:'Hi'}, 
                            <span class="wrap-xs-col visible-xs"></span>
                            nice to meet you.
                        </h1>
                    </div>
                </div>

                <div class="row row-eq-height row-contents-center">
                    <div class="col-xs-12 col-sm-12">
                        <p class="heading-sm">
                            I'm going to ensure you&nbsp;get 
                            <span class="wrap-xs-col"></span>
                            <span class="font-weight-bold">Max</span>imum savings and&nbsp;<span class="font-weight-bold">Max</span>imum 
                            <span class="wrap-xs-col"></span>
                            value with this quote.
                        </p>
                    </div>
                </div>

                <div class="col-xs-12 spacer-lg"></div>

                <form data-toggle="validator" role="form" class="inpage-form">
                    
                    <div class="form-group">
                        <button id="next_step" type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                    </div>
                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                  
                </form>

            </div>

        </div>
       
    </div>
</div>











