
<div class="container">
    <div class="centered-wrapper">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="heading-lg heading-bold ">Welcome to the COLFIRE
                    <span class="heading-second-line">Online Quotes Portal</span>
                </h1>
                <p class="heading-sm mb-20">Don't worry it's easy!</p>
                <div class="middle-content">
                    <p>This service is for new policy quotes only.</p>
                    <p>Enquiring about your renewal premium? Please call 800-SERV (7378) or <br>visit <a href="//www.colfire.com/EZy-Renew" target="_blank">EZy-Renew</a> to view your renewal notice and pay online.</p>
                </div>
                <div class="col-xs-12 spacer-lg"></div>

                <button type="button" class="btn btn-orange btn-xxl hidden-xs start-process" data-loading-text="{t}Processing...{/t}">Let's get started
                    <i class="material-icons icon-bold">arrow_forward</i>
                </button>

                <button type="submit" class="btn btn-orange btn-xl visible-xs start-process" data-loading-text="{t}Processing...{/t}">Let's get started </button>
            </div>
        </div>
    </div>
</div>

<style>
.heading-sm.mb-20 {
    color: #3a3939;
}

.middle-content {
    max-width: 705px;
    margin: 0 auto;
}

.middle-content a {
    color: #f06200;
}

.middle-content p {
    font-size: 21px;
    font-weight: 500;
    margin-bottom: 20px;
}
</style>











