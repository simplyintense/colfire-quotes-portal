<div class="container">
    <ul class="breadcrumb">
      <li><a href="{$settings->site_path}">{tp}Home{/tp}</a></li>
      <li class="active">{tp}Manage Vehicle Makes and Models{/tp}</li>
    </ul>


    <div class="row">
        <div class="col-xs-12 col-md-9">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{tp}Vehicle Make{/tp}&nbsp;
                        <div class="btn-group">
                            <a type="button" id="toggle_makes_panel" class="btn toggle-btn btn-default btn-xs" href="#makes_container">
                                <span class="glyphicon glyphicon-chevron-up"></span>
                            </a>
                        </div>

                        <div class="item_buttons pull-right">
                            <button class="btn btn-default btn-xs" id="add_make_small_button">
                                <span class="glyphicon glyphicon-plus"></span> {tp}Add{/tp}
                            </button>
                        </div>
                    </h3>
                </div>
                <div id="makes_container">

                    {if $items|count == 0}
                        {if $status|default:'active' == 'active'}
                            <div class="alert alert-warning" role="alert">
                                {tp}You don't have any Vehicle Models{/tp}
                            </div>
                        {else}
                            <div class="alert alert-warning" role="alert">{tp}You have no deleted Vehicle Makes{/tp}</div>
                        {/if}
                    {else}
                        <div class="list-group wallet_item" id="make_items">
                            {foreach from=$items item=i}
                                <a href="{$settings->site_path}/service/make/{$i->id}/model" class="list-group-item item" data-id="{$i->id}" 
                                    {if $i->status|default:'active' == 'hidden'}style="background: #eee"{/if}>
                                    
                                    <div class="item_buttons hideme wallet_buttons">
                                        {if $i->status|default:'active' == 'active'}
                                            <button class="btn btn-default btn-sm item_button_edit mt-15"><span class="glyphicon glyphicon-pencil"></span> {tp}Edit{/tp}</button>
                                            <button class="btn btn-default btn-sm item_button_remove mt-15"><span class="glyphicon glyphicon-trash"></span> {tp}Delete{/tp}</button>
                                        {/if}
                                        {if $i->status|default:'active' == 'hidden'}
                                            <button class="btn btn-default btn-sm item_button_restore mt-15"><span class="glyphicon glyphicon-repeat"></span> {tp}Restore{/tp}</button>
                                        {/if}
                                    </div>
                                            
                                    <h4 class="list-group-item-heading">
                                        {$i->make_name|escape:'html'|default:'no Make specified'}
                                    </h4>
                                    <h6>
                                        <span class="label label-primary">ID: {$i->id|escape:'html'|default:'Make ID'}</span>
                                    </h6>
                                    {* <button class="btn btn-default btn-xs item_button_remove pull-right"><span class="glyphicon glyphicon-trash"></span> {tp}Delete{/tp}</button>
                                    <button class="btn btn-default btn-xs item_button_edit pull-right"><span class="glyphicon glyphicon-pencil"></span> {tp}Edit{/tp}</button> *}
                                </a>
                            {/foreach}

                            {* <nav aria-label="items-nav">
                                <ul class="pager">
                                    <li class="previous disabled"><a href="#"><span aria-hidden="true">&larr;</span> Previous</a></li>
                                    <li class="next disabled"><a href="#">Next <span aria-hidden="true">&rarr;</span></a></li>
                                </ul>
                            </nav> *}
                        </div>
                    {/if}
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-3">
            {* <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{tp}Manage Make{/tp}
                    </h3>
                </div>
                <div class="panel-body">
                    <a href="#addMake" class="btn btn-orange btn-block" id="add_make_button">{tp}Add{/tp}</a>
                </div>
            </div> *}

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{tp}Search by Make or Model{/tp}
                    </h3>
                </div>
                <div class="panel-body">

                    <form method="get" action="{$settings->site_path}/service/make" role="form" id="makemodel_search">
                        <fieldset>

                            <div class="form-group">
                                <label class="dialogtypesearch label label-lightgrey" for="input_make">{t}Vehicle Make{/t}</label>
                                <input type="text" name="input_make" class="form-control" id="input_make" 
                                        placeholder="{t}Vehicle Make{/t}" value="{$search_params->car_make|escape:'html'|default:''}">
                            </div>

                            <div class="form-group">
                                <label class="dialogtypesearch label label-lightgrey" for="input_model">{t}Vehicle Model{/t}</label>
                                <input type="text" name="input_model" class="form-control" id="input_model" 
                                        placeholder="{t}Vehicle Model{/t}" value="{$search_params->car_model|escape:'html'|default:''}">
                            </div>
                            
                            <div class="alert alert-danger errors-container" style="display: none;">
                            </div>

                            <div class="form-group">
                                <input id="search_makemodel_button" type="submit" class="btn btn-orange btn-block" value="{t}Search{/t}" data-loading-text="{t}Searching...{/t}">
                                {if $search_params}
                                    <div class="pull-right">{tp}or{/tp} <a href="#" class="action" id="reset_makemodel_search_button">{tp}reset search{/tp}</a></div>
                                {/if}
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
  
        </div>

    </div>
</div>