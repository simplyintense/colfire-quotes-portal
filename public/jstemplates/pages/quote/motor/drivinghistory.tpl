
<div class="container">    
    <div class="centered-wrapper">
        <h1 class="heading-md heading-bold ">Tell us about your 
            <span class="heading-second-line">driving history</span>
        </h1>

        <div class="row row-contents-center">
            
                <form data-toggle="validator" role="form" class="inpage-form">

                    <fieldset>
                        <div class="row">
                            <div class="col-xs-12">

                                <p class="heading-sm mb-20">Have you or any of the other drivers had an accident in the last 5 years?</p>
                                <div class="form-group has-feedback div-fixed-width">

                                        <div class="custom-radios">
                                            <div>
                                                <input type="radio" class="color-5" id="accidents_recently_yes" name="accidents_recently" value="yes" data-error="Please make a chioce." required {if $item->accidents_recently == 'yes'}checked{/if}>
                                                <label for="accidents_recently_yes">
                                                    <span></span>
                                                    Yes
                                                </label>
                                            </div>
                                    
                                            <div class="ml-60">
                                                <input type="radio" class="color-5" id="accidents_recently_no" name="accidents_recently" value="no" data-error="Please make a chioce." required {if $item->accidents_recently == 'no'}checked{/if}>
                                                <label for="accidents_recently_no">
                                                    <span></span>
                                                    No
                                                </label>
                                            </div>
                                        </div>
                                    
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors fixed-height"></div>
                                </div>
                                
                            </div>
                        </div>
                    </fieldset>

                    <div class="col-xs-12 spacer-lg"></div>

                    

                    <div class="form-group">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                    </div>
                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                  
                </form>

        </div>
       
    </div>
</div>






















