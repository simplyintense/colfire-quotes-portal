
<div class="container">    
    <div class="centered-wrapper">
        
        <div class="row row-eq-height row-contents-center">
            <div class="col-xs-12">
                <h1 class="heading-md heading-bold ">What's the current value 
                    <span class="heading-second-line">of the vehicle?</span>
                </h1>
                <p class="heading-sm mb-20">Enter amount</p>

                <form data-toggle="validator" role="form" class="inpage-form small-notice">

                    <fieldset>
                        <div class="row">

                            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                <div class="form-group has-feedback">
                                    <div class="input-group">
                    	                <span class="input-group-addon text-bigger text-bold" id="usd-icon-addon">TT<i class="fa fa-usd"></i></span>
                                        <input type="number" step="1" lang="en" min="20000" class="form-control input-lg" id="input_current_value" placeholder="" value="{$item->current_value|default:''}" aria-describedby="usd-icon-addon"
                                               data-error="Invalid value entered. Please remove special characters ($ , ) and ensure value entered is at least&nbsp;20000." required>
                                         <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="help-block with-errors fixed-height"></div>
                                </div>
                            </div>

						<label class="heading-sm mb-20">What is your vehicle registration number?</label>

							<div class="col-xs-12 col-sm-6 col-sm-offset-3">
								<div class="form-group has-feedback">
									<div class="input-group-xl">
										<input type="text" class="form-control input-lg" id="vehicle_registration_number" placeholder="" value="{$item->vehicle_registration_number|default:''}"
												data-error="Please enter vehicle registration number.">
												<div class="help-block fixed-height">* leave blank if it`s a new vehicle</div>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>
									<div class="help-block with-errors fixed-height"></div>
								</div>
							</div>

						<label class="heading-sm mb-20">Is the vehicle kept overnight in a locked garage or fenced property?</label>

							<div class="col-xs-12 col-sm-6 col-sm-offset-3">
								<div class="form-group div-fixed-width">
									<div class="custom-radios">
										<div>
											<input type="radio" class="color-5" id="vehicle_protected_yes" name="vehicle_protected" value="yes" data-error="Please make a chioce." {if $item->vehicle_protected == 'yes'}checked{/if} required>
											<label for="vehicle_protected_yes">
												<span></span>
												Yes
											</label>
										</div>
                                    
										<div class="ml-60">
											<input type="radio" class="color-5" id="vehicle_protected_no" name="vehicle_protected" value="no" data-error="Please make a chioce." {if $item->vehicle_protected == 'no'}checked{/if} required>
											<label for="vehicle_protected_no">
												<span></span>
												No
											</label>
										</div>
									</div>
								</div>
							</div>

                        </div>
                    </fieldset>

                    <div class="col-xs-12 spacer-lg"></div>

                    

                    <div class="form-group">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                    </div>

                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                  
                </form>


            </div>

        </div>
       
    </div>
</div>











