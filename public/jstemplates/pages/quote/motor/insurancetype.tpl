
<div class="container">    
    <div class="centered-wrapper">
        
        <div class="row row-eq-height row-contents-center">
            <div class="col-xs-12">
                <h1 class="heading-md heading-bold ">Now that we have the Driver's 
                    <span class="heading-second-line">info, let's move onto the vehicle</span>
                </h1>
                <p class="heading-sm mb-50">What type of Motor insurance do you need?</p>

                <form data-toggle="validator" role="form" class="inpage-form">

                    <fieldset>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-sm-offset-3">

                                <div class="form-group">
                                        <div class="custom-radios div-fixed-width text-left">
                                            <div>
                                            <input id="insurance_type_comprehensive" type="checkbox" {if $item->insurance_type_comprehensive == 'yes'}checked{/if} class="color-5">
                                            <label for="insurance_type_comprehensive">
                                                <span></span>
                                                Comprehensive  <a href="#" data-toggle="tooltip" class="text-orange"
                                                                    title='Liability for Third Party Bodily Injury, Property Damage  
                                                                             and loss of or damage to your Motor Vehicle'
                                                                    ><i class="fa fa-question-circle-o fa-lg" aria-hidden="true"></i></a>
                                            </label>
                                            </div>
                                        </div>
                                </div>

                                <div class="form-group">
                                        <div class="custom-radios div-fixed-width text-left">
                                            <div>
                                            <input id="insurance_type_third_party_fire_theft" type="checkbox" {if $item->insurance_type_third_party_fire_theft == 'yes'}checked{/if} class="color-5">
                                            <label for="insurance_type_third_party_fire_theft">
                                                <span></span>
                                                Third Party Fire and Theft <a href="#" data-toggle="tooltip" class="text-orange"
                                                                    title="Liability for Third Party Bodily Injury and
																			  Property Damage and loss of or damage
																			  to your Motor Vehicle
																			  by Fire or Theft only"
                                                                    ><i class="fa fa-question-circle-o fa-lg" aria-hidden="true"></i></a>
                                            </label>
                                            </div>
                                        </div>
                                </div>

                                <div class="form-group">
                                        <div class="custom-radios div-fixed-width text-left">
                                            <div>
                                            <input id="insurance_type_third_party" type="checkbox" {if $item->insurance_type_third_party == 'yes'}checked{/if} class="color-5">
                                            <label for="insurance_type_third_party">
                                                <span></span>
                                                Third Party  <a href="#" data-toggle="tooltip" class="text-orange"
                                                                    title='Liability for Third Party Bodily Injury 
                                                                             and Property Damage only'
                                                                    ><i class="fa fa-question-circle-o fa-lg" aria-hidden="true"></i></a>
                                            </label>
                                            </div>
                                        </div>
                                </div>
                                
                            </div>
                        </div>
                    </fieldset>

                    

                    

                    <div class="form-group">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                    </div>

                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                  
                </form>


            </div>

        </div>
       
    </div>
</div>











