
<div class="container">    
    <div class="centered-wrapper">
        
        <div class="row row-eq-height row-contents-center">
            <div class="col-xs-12">
                <h1 class="heading-md heading-bold  mb-50">Sorry to hear that, please tell me 
                    <span class="heading-second-line">more about this accident(s)</span>
                </h1>
                
                 <div class="row">
            
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 mb-50 text-left">

                        <div class="list-group">
			                <div id="accidents_container" class="text-bigger text-bold">
                                <div class="list-group-item list-group-item-dark">
                                  {tp}Loading Accidents..{/tp}
                                </div>
			                </div>
		                </div>

                    </div>

                    <div class="col-xs-12 pt-50">
                        <button type="button" class="btn btn-xl btn-grey add-accident"><i class="fa fa-user-plus" aria-hidden="true"></i> ADD {if $accidents|count >0}ANOTHER {/if}ACCIDENT</button>
                    </div>

                    <div class="col-xs-12 spacer-lg"></div>

                    <div class="col-xs-12">
                        <form data-toggle="validator" role="form" class="inpage-form small-notice">
                    
                    

                            <div class="form-group">
                                <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                            </div>
                            <div class="alert alert-danger errors-container" style="display: none;"></div>
                    
                        </form>
                    </div>

                </div>


            </div>

        </div>
       
    </div>
</div>











