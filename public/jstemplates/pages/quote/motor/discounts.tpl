
<div class="container">    
    <div class="centered-wrapper">
        
        <div class="row row-eq-height row-contents-center">
            <div class="col-xs-12">
                <h1 class="heading-md heading-bold ">One more thing, let's see what discounts you
                    <span class="heading-second-line">can qualify for to help you save some money</span>
                </h1>
                <p class="heading-sm mb-50 medium-notice">You are entitled to your Safe Driver Discount and a maximum of 3 other discounts that you qualify for</p>

                <form data-toggle="validator" role="form" class="inpage-form">

                    <fieldset>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                
                                
                                <div class="form-group">
                                        <div class="custom-radios div-fixed-width text-left">
                                            <div>
                                            <input id="no_claim_discount_transfer" type="checkbox" {if $item->no_claim_discount_transfer == 'yes'}checked{/if} class="color-5">
                                            <label for="no_claim_discount_transfer">
                                                <span></span>
                                                I have a No claim discount that I will be transferring from another policy 
                                            </label>
                                            </div>
                                        </div>
                                </div>

                                <div class="form-group has-feedback small-notice no_claim_discount_years_form_group" {if $item->no_claim_discount_transfer|default:'no' == 'no'}style="display:none"{/if}>
                                    <input type="number" step="1" min="1" class="form-control input-lg" id="input_no_claim_discount_years" placeholder="Enter the number of years you've had this discount" value="{$item->no_claim_discount_years|default:''}"
                                            data-error="Please enter the number of years.">
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    
                                    <div class="help-block with-errors fixed-height"></div>
                                </div>

                                <div class="form-group">
                                    <div class="custom-radios div-fixed-width text-left">
                                        <div>
                                        <input id="member_of_credit_union" type="checkbox" {if $item->member_of_credit_union == 'yes'}checked{/if} class="color-5">
                                        <label for="member_of_credit_union">
                                            <span></span>
                                            I am currently a member of a credit union
                                        </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="custom-radios div-fixed-width text-left">
                                        <div>
                                        <input id="live_close_to_work" type="checkbox" {if $item->live_close_to_work == 'yes'}checked{/if} class="color-5">
                                        <label for="live_close_to_work">
                                            <span></span>
                                            I live within 20km of my workplace
                                        </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="custom-radios div-fixed-width text-left">
                                        <div>
                                        <input id="defensive_driver_cert" type="checkbox" {if $item->defensive_driver_cert == 'yes'}checked{/if} class="color-5">
                                        <label for="defensive_driver_cert">
                                            <span></span>
                                            I have a defensive driving certificate
                                        </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group has-feedback small-notice defensive_driver_school_form_group" {if $item->defensive_driver_cert|default:'no' == 'no'}style="display:none"{/if}>
                                    <input type="text" class="form-control input-lg" id="input_defensive_driver_school" placeholder="Enter the name of the defensive driving school" value="{$item->defensive_driver_school|default:''}"
                                        minlength="2" data-error="Please enter 2 or more letters.">
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    
                                    <div class="help-block with-errors fixed-height"></div>
                                </div>

                                <div class="form-group">
                                    <div class="custom-radios div-fixed-width text-left">
                                        <div>
                                        <input id="loan" type="checkbox" {if $item->loan == 'yes'}checked{/if} class="color-5">
                                        <label for="loan">
                                            <span></span>
                                            I have a loan on this vehicle
                                        </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group has-feedback small-notice bank_name_form_group" {if $item->loan|default:'no' == 'no'}style="display:none"{/if}>

									<select name="usage_type" id="input_bank_name" class="form-control input-lg" data-error="Please enter 2 or more letters.">
                                        <option value="">Select the name of the bank</option>
                                        {foreach from=$settings.loan_bank_vehicle item=c key=id}
                                        <option value="{$id}" {if $item->bank_name == $id}selected="selected"{/if}>{$c}</option>
                                        {/foreach}
                                    </select>

                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors fixed-height"></div>

									<div class="form-group has-feedback small-notice custom_loan_bank_form_group" {if $item->bank_name|default:'' !== 'other'}style="display:none"{/if}>
										<div class="form-group has-feedback">
											<input type="text" class="form-control input-lg" id="custom_loan_bank" placeholder="Type your bank" value="{$item->custom_loan_bank|default:''}"
													data-error="Please enter your bank.">
												<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
												<div class="help-block with-errors fixed-height"></div>
										</div>
									</div>
									
                                </div>



								<div class="form-group">
                                    <div class="custom-radios div-fixed-width text-left">
                                        <div>
                                        <input id="credit_card" type="checkbox" {if $item->credit_card == 'yes'}checked{/if} class="color-5">
                                        <label for="credit_card">
                                            <span></span>
                                            I will be paying with a credit card
                                        </label>
                                        </div>
                                    </div>
                                </div>

								
                                <div class="form-group has-feedback small-notice credit_card_form_group" {if $item->credit_card|default:'no' == 'no'}style="display:none"{/if}>

									<select name="credit_card_bank" id="credit_card_bank" class="form-control input-lg" data-error="Please enter 2 or more letters.">
                                        <option value="">Select the name of the bank</option>
                                        {foreach from=$settings.credit_card_bank item=c key=id}
                                        <option value="{$id}" {if $item->credit_card_bank == $id}selected="selected"{/if}>{$c}</option>
                                        {/foreach}
                                    </select>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors fixed-height"></div>

									<div class="form-group has-feedback small-notice custom_credit_card_bank_form_group" {if $item->bank_name|default:'' !== 'other'}style="display:none"{/if}>
										<div class="form-group has-feedback">
											<input type="text" class="form-control input-lg" id="custom_credit_card_bank" placeholder="Type your bank" value="{$item->credit_card_bank_custom|default:''}"
													data-error="Please enter your bank.">
												<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
												<div class="help-block with-errors fixed-height"></div>
										</div>
									</div>
									
                                </div>

                                <div class="form-group">
                                    <div class="custom-radios div-fixed-width text-left">
                                        <div>
                                        <input id="sole_driver" type="checkbox" {if $item->sole_driver == 'yes'}checked{/if} class="color-5">
                                        <label for="sole_driver">
                                            <span></span>
                                            I will be the sole driver of the vehicle
                                        </label>
                                        </div>
                                    </div>
                                </div>

								<div class="form-group">
                                    <div class="custom-radios div-fixed-width text-left">
                                        <div>
                                        <input id="have_child" type="checkbox" {if $item->have_child == 'yes'}checked{/if} class="color-5">
                                        <label for="have_child">
                                            <span></span>
                                            I have a child under 4 years of age
                                        </label>
                                        </div>
                                    </div>
                                </div>

                                
                            </div>
                        </div>
                    </fieldset>

                    
                    <div class="col-xs-12 mb-50"></div>
                    

                    <div class="form-group">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                    </div>

                    <div class="alert alert-danger errors-container " style="display: none;"></div>
                  
                </form>


            </div>

        </div>
       
    </div>
</div>
