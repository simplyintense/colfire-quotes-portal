
<div class="container">    
    <div class="centered-wrapper">
        
        <div class="row row-eq-height row-contents-center">
            <div class="col-xs-12">
                <h1 class="heading-md heading-bold ">Got it. Can you tell me  
                    <span class="heading-second-line">about the vehicle?</span>
                </h1>
                <p class="heading-sm mb-50"></p>

                <form data-toggle="validator" role="form" class="inpage-form small-notice" id="car_make_model_form">

                    <fieldset>
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
								<div id="input_make_form_group" class="form-group has-feedback">
                                    {* Replaced by car_make Part View after the data is fetched *}
                                    <select name="input_make" id="input_make" class="form-control input-lg" data-error="Please select make.">
                                        <option value="">Loading Makes...</option>
                                    </select>
                                    <div class="help-block with-errors fixed-height"></div>
                                </div>

                                <div  id="input_model_form_group" class="form-group has-feedback">
                                    {* Replaced by car_model Part View after the data is fetched *}
                                    <select name="input_model" id="input_model" class="form-control input-lg" data-error="Please select model." disabled>
                                        <option value="">Loading Models...</option>
                                    </select>
                                    <div class="help-block with-errors fixed-height"></div>
                                </div>

								<div class="form-group has-feedback small-notice custom_descr_make_model_form_group" {if $item->model|default:'' !== 'OTHER'}style="display:none"{/if}>
                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control input-lg" id="custom_descr_make_model" placeholder="Enter your make & model" value="{$item->custom_descr_make_model|default:''}"
                                                data-error="Please enter custom vehicle make and model.">
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
											<div class="help-block with-errors fixed-height"></div>
									</div>
								</div>

                                <div class="form-group has-feedback">
                                    <select name="input_year" id="input_year" class="form-control input-lg" data-error="Please select manufacturing year." required>
                                        <option value="">Year of Manufacture</option>
                                        {foreach from=$years_list item=c key=id}
                                        <option value="{$c}" {if $item->year == $c}selected="selected"{/if}>{$c}</option>
                                        {/foreach}
                                    </select>
                                    <div class="help-block with-errors fixed-height"></div>
                                </div>

								<div class="form-group has-feedback">
                                    <select name="vehicle_origin" id="vehicle_origin" class="form-control input-lg" data-error="Please select vehicle origin type." required>
                                        <option value="">Type</option>
                                        {foreach from=$settings.vehicle_origin item=c key=id}
                                        <option value="{$id}" {if $item->vehicle_origin == $id}selected="selected"{/if}>{$c}</option>
                                        {/foreach}
                                    </select>
                                    <div class="help-block with-errors fixed-height"></div>
                                </div>

                            </div>
                        </div>
                    </fieldset>

                    <div class="col-xs-12 spacer-md"></div>

                    

                    <div class="form-group">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                    </div>

                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                  
                </form>


            </div>

        </div>
       
    </div>
</div>











