
<div class="container">    
    <div class="centered-wrapper">
        
        <div class="row row-eq-height row-contents-center">
            <div class="col-xs-12">
                <h1 class="heading-md heading-bold ">How do you plan to use 
                    <span class="heading-second-line">this vehicle?</span>
                </h1>
                <p class="heading-sm mb-50"></p>

                <form data-toggle="validator" role="form" class="inpage-form small-notice">

                    <fieldset>
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                                
                                <div class="form-group has-feedback">
                                    <select name="usage_type" id="usage_type" class="form-control input-lg" data-error="Please make a selection." required>
                                        <option value="">Select</option>
                                        {foreach from=$settings.car_usage_types item=c key=id}
                                        <option value="{$id}" {if $item->usage_type == $id}selected="selected"{/if}>{$c}</option>
                                        {/foreach}
                                    </select>
                                    <div class="help-block with-errors fixed-height"></div>
                                </div>
                                
                            </div>
                        </div>
                    </fieldset>

                    <div class="col-xs-12 spacer-lg"></div>

                    

                    <div class="form-group">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                    </div>

                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                  
                </form>


            </div>

        </div>
       
    </div>
</div>











