
<div class="container">    
    <div class="centered-wrapper">
        
        <div class="row row-eq-height row-contents-center">
            <div class="col-xs-12">
                <h1 class="heading-md heading-bold ">Do you want to include any
                    <span class="heading-second-line">of these additional benefits?</span>
                </h1>
                <p class="heading-sm mb-50"></p>

                <form data-toggle="validator" role="form" class="inpage-form">

                    <fieldset>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-sm-offset-3">

								<!-- Third Party options -->
								<div class="form-group" {if $item->insurance_type_third_party|default:'no' == 'no'}style="display:none"{/if}>
									
									<p class="heading-sm small-notice text-left" {if $item->insurance_type_comprehensive|default:'no' == 'no' && $item->insurance_type_third_party_fire_theft|default:'no' == 'no'}style="display:none"{/if}>
										Third Party:
									</p>
																	   									 								  

									<div class="form-group">
											<div class="custom-radios div-fixed-width text-left">
												<div>
												<input id="windscreen_coverage_3rd_party" type="checkbox" {if $item->windscreen_coverage_3rd_party == 'yes'}checked{/if} class="color-5">
												<label for="windscreen_coverage_3rd_party">
													<span></span>
													Windscreen Coverage <a href="#" data-toggle="tooltip" class="text-orange"
																		title="COLFIRE will repair the breakage of your
																		windscreen, windows and sunroof to the
																		extent of the limit requested"
																		><i class="fa fa-question-circle-o fa-lg" aria-hidden="true"></i></a>
												</label>
												</div>
											</div>
									</div>

									<div class="form-group has-feedback small-notice windscreen_coverage_amount_3rd_party_form_group" {if $item->windscreen_coverage_3rd_party|default:'no' == 'no'}style="display:none"{/if}>
										<input type="number" step="1" min="1" lang="en" class="form-control input-lg" id="input_windscreen_coverage_amount_3rd_party" placeholder="Enter amount of coverage" value="{$item->windscreen_coverage_amount_3rd_party|default:''}"
												data-error="Please enter coverage value without special characters ($,)" {if $item->windscreen_coverage_3rd_party == 'yes'}required{/if}>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    
										<div class="help-block with-errors fixed-height"></div>
									</div>

									<div class="form-group">
											<div class="custom-radios div-fixed-width text-left">
												<div>
												<input id="emergency_roadside_assist_3rd_party" type="checkbox" {if $item->emergency_roadside_assist_3rd_party == 'yes'}checked{/if} class="color-5">
												<label for="emergency_roadside_assist_3rd_party">
													<span></span>
													Emergency Roadside Assistance <a href="#" data-toggle="tooltip" class="text-orange"
																		title='24-hour emergency assistance should your vehicle require towing, 
																				 battery boost, refueling, tyre change or locksmith services. 
																				 A maximum of three (3) "call outs" is allowed during any one period of insurance'
																		><i class="fa fa-question-circle-o fa-lg" aria-hidden="true"></i></a>
												</label>
												</div>
											</div>
									</div>

								</div>
								<!-- /Third Party options -->


								<!-- Third Party Fire & Theft options -->
								<div class="form-group" {if $item->insurance_type_third_party_fire_theft|default:'no' == 'no'}style="display:none"{/if}>
									
									<p class="heading-sm small-notice mt-30 text-left" {if $item->insurance_type_comprehensive|default:'no' == 'no' && $item->insurance_type_third_party|default:'no' == 'no'}style="display:none"{/if}>
										Third Party Fire & Theft:
									</p>
																	   									 								  

									<div class="form-group">
											<div class="custom-radios div-fixed-width text-left">
												<div>
												<input id="windscreen_coverage_3rd_party_fire" type="checkbox" {if $item->windscreen_coverage_3rd_party_fire == 'yes'}checked{/if} class="color-5">
												<label for="windscreen_coverage_3rd_party_fire">
													<span></span>
													Windscreen Coverage <a href="#" data-toggle="tooltip" class="text-orange"
																		title="COLFIRE will repair the breakage of your
																		windscreen, windows and sunroof to the
																		extent of the limit requested"
																		><i class="fa fa-question-circle-o fa-lg" aria-hidden="true"></i></a>
												</label>
												</div>
											</div>
									</div>

									<div class="form-group has-feedback small-notice windscreen_coverage_amount_3rd_party_fire_form_group" {if $item->windscreen_coverage_3rd_party_fire|default:'no' == 'no'}style="display:none"{/if}>
										<input type="number" step="1" min="1" lang="en" class="form-control input-lg" id="input_windscreen_coverage_amount_3rd_party_fire" placeholder="Enter amount of coverage" value="{$item->windscreen_coverage_amount_3rd_party_fire|default:''}"
												data-error="Please enter coverage value without special characters ($,)" {if $item->windscreen_coverage_3rd_party_fire == 'yes'}required{/if}>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    
										<div class="help-block with-errors fixed-height"></div>
									</div>

									<div class="form-group">
											<div class="custom-radios div-fixed-width text-left">
												<div>
												<input id="emergency_roadside_assist_3rd_party_fire" type="checkbox" {if $item->emergency_roadside_assist_3rd_party_fire == 'yes'}checked{/if} class="color-5">
												<label for="emergency_roadside_assist_3rd_party_fire">
													<span></span>
													Emergency Roadside Assistance <a href="#" data-toggle="tooltip" class="text-orange"
																		title='24-hour emergency assistance should your vehicle require towing, 
																				 battery boost, refueling, tyre change or locksmith services. 
																				 A maximum of three (3) "call outs" is allowed during any one period of insurance'
																		><i class="fa fa-question-circle-o fa-lg" aria-hidden="true"></i></a>
												</label>
												</div>
											</div>
									</div>

								</div>
								<!-- /Third Party Fire & Theft options -->

                                
                                <!-- Comprehensive options -->
								<div class="form-group" {if $item->insurance_type_comprehensive|default:'no' == 'no'}style="display:none"{/if}>
									
									<p class="heading-sm small-notice mt-30 text-left" {if $item->insurance_type_third_party_fire_theft|default:'no' == 'no' && $item->insurance_type_third_party|default:'no' == 'no'}style="display:none"{/if}>
										Comprehensive:
									</p>

									<div class="form-group">
											<div class="custom-radios div-fixed-width text-left">
												<div>
												<input id="windscreen_coverage" type="checkbox" {if $item->windscreen_coverage == 'yes'}checked{/if} class="color-5">
												<label for="windscreen_coverage">
													<span></span>
													Windscreen Coverage <a href="#" data-toggle="tooltip" class="text-orange"
																		title="COLFIRE will repair the breakage of your
																		windscreen, windows and sunroof to the
																		extent of the limit requested"
																		><i class="fa fa-question-circle-o fa-lg" aria-hidden="true"></i></a>
												</label>
												</div>
											</div>
									</div>

									<div class="form-group has-feedback small-notice windscreen_coverage_amount_form_group" {if $item->windscreen_coverage|default:'no' == 'no'}style="display:none"{/if}>
										<input type="number"  step="1" min="3000" lang="en" class="form-control input-lg" id="input_windscreen_coverage_amount" placeholder="Enter amount of coverage" value="{$item->windscreen_coverage_amount|default:''}"
												data-error="Invalid value entered. Please remove special characters ($ , . ) and ensure the value entered is at least 3000." {if $item->windscreen_coverage == 'yes'}required{/if}>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    
										<div class="help-block with-errors fixed-height" text align="left"></div>
									</div>

									<div class="form-group">
											<div class="custom-radios div-fixed-width text-left">
												<div>
												<input id="emergency_roadside_assist" type="checkbox" {if $item->emergency_roadside_assist == 'yes'}checked{/if} class="color-5">
												<label for="emergency_roadside_assist">
													<span></span>
													Emergency Roadside Assistance <a href="#" data-toggle="tooltip" class="text-orange"
																		title='24-hour emergency assistance should your vehicle require towing, 
																				 battery boost, refueling, tyre change or locksmith services. 
																				 A maximum of three (3) "call outs" is allowed during any one period of insurance'
																		><i class="fa fa-question-circle-o fa-lg" aria-hidden="true"></i></a>
												</label>
												</div>
											</div>
									</div>

									<div class="form-group">
											<div class="custom-radios div-fixed-width text-left">
												<div>
												<input id="flood_special_perils" type="checkbox" {if $item->flood_special_perils == 'yes'}checked{/if} class="color-5">
												<label for="flood_special_perils">
													<span></span>
													Flood Special Perils <a href="#" data-toggle="tooltip" class="text-orange"
																		title="Cover in the event your vehicle is affected by flood or another natural disaster"
																		><i class="fa fa-question-circle-o fa-lg" aria-hidden="true"></i></a>
												</label>
												</div>
											</div>
									</div>

									<div class="form-group">
											<div class="custom-radios div-fixed-width text-left">
												<div>
												<input id="partial_waiver_of_excess" type="checkbox" {if $item->partial_waiver_of_excess == 'yes'}checked{/if} class="color-5">
												<label for="partial_waiver_of_excess">
													<span></span>
													Partial Waiver of Excess <a href="#" data-toggle="tooltip" class="text-orange"
																		title="COLFIRE will waive the excess to be paid by either 
																				you or your spouse in the event of an accidental collision."
																		><i class="fa fa-question-circle-o fa-lg" aria-hidden="true"></i></a>
												</label>
												</div>
											</div>
									</div>

									<div class="form-group">
											<div class="custom-radios div-fixed-width text-left">
												<div>
												<input id="loss_of_use" type="checkbox" {if $item->loss_of_use == 'yes'}checked{/if} class="color-5">
												<label for="loss_of_use">
													<span></span>
													Loss of Use <a href="#" data-toggle="tooltip" class="text-orange"
																		title="In the event of an accidental collision and you are unable to use your vehicle, 
																			COLFIRE will pay you the value of the benefit for the number of days required 
																			to effect repairs caused directly as a result of the accident - to a maximum of 15 days"
																		><i class="fa fa-question-circle-o fa-lg" aria-hidden="true"></i></a>
												</label>
												</div>
											</div>
									</div>

									<div class="form-group" {if $item->insurance_type_comprehensive|default:'no' == 'no' || $item->vehicle_age > 5}style="display:none"{/if}>
											<div class="custom-radios div-fixed-width text-left">
												<div>
												<input id="new_for_old" type="checkbox" {if $item->new_for_old == 'yes'}checked{/if} class="color-5">
												<label for="new_for_old">
													<span></span>
													New for Old <a href="#" data-toggle="tooltip" class="text-orange"
																		title="In the event of a claim, COLFIRE will waive the depreciation 
																			   cost to be paid by you for replacing damaged parts with new parts. 
																			   This is only applicable to vehicles 5 years and under from the year of manufacturer"
																		><i class="fa fa-question-circle-o fa-lg" aria-hidden="true"></i></a>
												</label>
												</div>
											</div>
									</div>

								</div>
								<!-- /Comprehensive options -->
                                
                            </div>
                        </div>
                    </fieldset>

                    
                    <div class="col-xs-12 mb-50"></div>

					<div class="col-xs-12 spacer-md"></div>
                    

                    <div class="form-group">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                    </div>

                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                  
                </form>


            </div>

        </div>
       
    </div>
</div>
