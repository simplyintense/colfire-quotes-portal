
<div class="container">    
    <div class="centered-wrapper">
        <h1 class="heading-md heading-bold mb-50">Here are the vehicles
            <span class="heading-second-line">on this quote:</span>
        </h1>

        <div class="row">
            
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 mb-50 text-left">

                <div class="list-group">
			        <div id="vehicles_container" class="text-bigger text-bold">
                        <div class="list-group-item list-group-item-dark">
                          {tp}Loading Vehicles..{/tp}
                        </div>
			        </div>
		        </div>

            </div>

            <div class="col-xs-12">
                <button type="button" class="btn btn-xl btn-grey add-vehicle"><i class="fa fa-plus" aria-hidden="true"></i>  ADD ANOTHER VEHICLE</button>
            </div>

            <div class="col-xs-12 spacer-lg"></div>

            <div class="col-xs-12">
                <form data-toggle="validator" role="form" class="inpage-form small-notice">
                    
                    

                    <div class="form-group">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                    </div>
                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                    
                </form>
            </div>

        </div>
       
    </div>
</div>











