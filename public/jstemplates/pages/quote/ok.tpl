
<div class="container">    
    <div class="centered-wrapper">
        
        <div class="row row-contents-center">
            <div class="col-xs-12">
                <h1 class="heading-lg heading-bold heading-fixed-max-width">
                    Awesome! Now that I know a little more about you, let's get some information on the 
                    {if $item->insurance_type|default:'' == 'motor'}vehicle and drivers{/if}
                    {if $item->insurance_type|default:'' == 'property'}property{/if}
                </h1>
                
                <div class="col-xs-12 spacer-lg"></div>

                <form data-toggle="validator" role="form" class="inpage-form">
                    
                    <div class="form-group">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">LET'S GO</button>
                    </div>

                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                  
                </form>

            </div>

        </div>
       
    </div>
</div>











