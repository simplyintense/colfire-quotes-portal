
<div class="container">    
    <div class="centered-wrapper">

        <div class="mb-50"></div>

        <div class="row row-eq-height row-contents-center">
            <div class="col-sm-2 col-sm-offset-1 hidden-xs">
                <img src="/images/guy.png" class="img-fluid" alt="Max">
            </div>
            <div class="col-xs-12 col-sm-8">
                <div class="row row-eq-height row-contents-center no-gutter">
                    <div class="col-xs-3 text-right visible-xs">
                        <img src="/images/guy-xs.png" class="fit-image-xs" alt="Max">
                    </div>
                    <div class="col-xs-9 col-sm-12 text-xs-left">
                        <h1 class="heading-lg heading-md-smaller-xs-screen heading-md-smaller-xxs-screen heading-bold padding-xxs-col padding-xxxs-col padding-xs-col">
                            Alright! 
                            <span class="wrap-xs-col visible-xs"></span>
                            Now let's get&nbsp;you 
                            <span class="wrap-xs-col"></span>
                            that&nbsp;quote.
                        </h1>
                    </div>
                </div>
                <div class="">&ndash;</div>
                <div class="row row-eq-height row-contents-center">
                    <div class="col-xs-12">
                        <p class="heading-sm">
                            Hit 'Submit' and you will receive 
                            <span class="wrap-xs-col"></span>
                            an email or call from us within one 
                            <span class="wrap-xs-col"></span>
                            (1) hour during regular business hours.
                        </p>
                    </div>
                </div>

                <div class="col-xs-12 mb-50"></div>

                <form data-toggle="validator" role="form" class="inpage-form">
                    
                    <div class="form-group">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">SUBMIT</button>
                    </div>

                    <div class="col-xs-12 mb-50"></div>

                    <div class="col-xs-12">
                        <div class="alert alert-danger errors-container" style="display: none;"></div>
                    </div>

                </form>

            </div>

        </div>
       
    </div>
</div>
