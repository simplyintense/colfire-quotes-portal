
<div class="container">    
    <div class="centered-wrapper">

        <div class="mb-50"></div>

        <div class="row row-eq-height row-contents-center">
            <div class="col-sm-2 col-sm-offset-1 hidden-xs">
                <img src="/images/guy.png" class="img-fluid" alt="Max">
            </div>
            <div class="col-sm-8 col-xs-12">
                <h2 class="heading-lg-smaller heading-bold mb-50">Thank you {$profile->firstname|default:''}, for considering COLFIRE as your Insurer of Choice. Your Quote has been submitted.</h2>
                <h2 class="heading-bold mb-50">Reference number: {$item->ref_num}</h2>
                <p class="heading-sm">
                    You will be hearing from us shortly, if you would like any further information you can contact us with the reference number above toll free on 800-QUOT(e).
                </p>

				<form data-toggle="validator" role="form" class="inpage-form xsmall-notice" {if $item->rating|default:0 > 0} style="display: none;"{/if}> 
                    
					<div class="form-group">
						<label class=" label label-default" for="service_rating">{t}Please rate this service{/t}</label>
						<div class="col-xs-12 mb-50" id="service_rating">
							<i class="fa fa-star-o" id="star_1" data-id="1"></i>
							<i class="fa fa-star-o" id="star_2" data-id="2"></i>
							<i class="fa fa-star-o" id="star_3" data-id="3"></i>
							<i class="fa fa-star-o" id="star_4" data-id="4"></i>
							<i class="fa fa-star-o" id="star_5" data-id="5"></i>
							<input type="hidden" value="0" id="input_rating"></input>
						</div>				 
					</div>


					<div class="form-group">
						<label class="dialogtypeadd label label-default" for="rating_description">{t}{/t}</label>
						<textarea rows="4" style="resize: vertical;" maxlength="800" name="rating_description" class="form-control" id="rating_description" placeholder="{t}Please type your feedback{/t}"></textarea>
					</div>

					<div class="form-group" style="display:none;" id="submit_rating_form_group">
                        <a href="#" class="btn btn-orange btn-form" id="submit_rating" data-loading-text="{t}Processing...{/t}">Rate</a>
                    </div>

                </form>

				<div class="col-xs-12 mb-50">
					<div class="alert alert-success success-container" {if $item->rating|default:0 == 0} style="display: none;"{/if}>
						Thank you for leaving your feedback.
					</div>
				</div>
				



				

				<div class="col-xs-12 mb-50"></div>

                <form data-toggle="validator" role="form" class="inpage-form">
                    
                    <div class="form-group">
                        <a href="/insurance" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">GET NEW QUOTE</a>
                    </div>

                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                  
                </form>

            </div>

        </div>
       
    </div>
</div>
