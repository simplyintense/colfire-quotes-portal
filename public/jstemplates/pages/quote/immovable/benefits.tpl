
<div class="container">    
    <div class="centered-wrapper">
        
        <div class="row row-eq-height row-contents-center">
            <div class="col-xs-12">
                <h1 class="heading-md heading-bold ">Do you want to include any
                    <span class="heading-second-line">of these additional benefits?</span>
                </h1>
                <p class="heading-sm mb-50"></p>

                <form data-toggle="validator" role="form" class="inpage-form">

                    <fieldset>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                
                                
                                <div class="form-group">
                                        <div class="custom-radios div-fixed-width text-left">
                                            <div>
                                            <input id="flood" type="checkbox" {if $item->flood == 'yes'}checked{/if} class="color-5">
                                            <label for="flood">
                                                <span></span>
                                                Flood
                                            </label>
                                            </div>
                                        </div>
                                </div>

                                <div class="form-group">
                                        <div class="custom-radios div-fixed-width text-left">
                                            <div>
                                            <input id="subsidence_and_landslip" type="checkbox" {if $item->subsidence_and_landslip == 'yes'}checked{/if} class="color-5">
                                            <label for="subsidence_and_landslip">
                                                <span></span>
                                                Subsidence and Landslip
                                            </label>
                                            </div>
                                        </div>
                                </div>

								<div class="form-group has-feedback small-notice retaining_wall text-left" {if $item->subsidence_and_landslip|default:'no' == 'no'}style="display:none"{/if}>
									
										<div class="heading-xs ml-35">
											<label for="retaining_wall">
												<span></span>
												Is there a retaining wall on the property?
											</label>
										</div>
 
										<div class="form-group div-fixed-width">
											<div class="custom-radios ml-35">
												<div>
													<input type="radio" class="color-5" id="subsidence_and_landslip_yes" name="subsidence_and_landslip_retaining_wall" value="yes" data-error="Please make a chioce." {if $item->subsidence_and_landslip_retaining_wall == 'yes'}checked{/if}>
													<label for="subsidence_and_landslip_yes">
														<span></span>
														Yes
													</label>
												</div>
                                    
												<div class="ml-60">
													<input type="radio" class="color-5" id="subsidence_and_landslip_no" name="subsidence_and_landslip_retaining_wall" value="no" data-error="Please make a chioce." {if $item->subsidence_and_landslip_retaining_wall == 'no'}checked{/if}>
													<label for="subsidence_and_landslip_no">
														<span></span>
														No
													</label>
												</div>
											</div>

										</div>
									

                                    <div class="form-group has-feedback small-notice subsidence_and_landslip_insured_form_group text-left" {if $item->subsidence_and_landslip_retaining_wall|default:'no' == 'no'}style="display:none"{/if}>
                                        <label class="heading-xs ml-35">Sum insured on retaining wall</label>
										
                                        <div class="input-group ml-35">
                    	                    <span class="input-group-addon text-bigger text-bold" id="usd-icon-addon">TT<i class="fa fa-usd"></i></span>
                                            <input type="number" step="1" min="1" class="form-control input-lg" id="subsidence_and_landslip_insured" placeholder="Enter sum insured on retaining wall" value="{$item->subsidence_and_landslip_insured|default:''}" aria-describedby="usd-icon-addon"
                                                   data-error="Please enter retaining wall insured amount.">
                                             <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        </div>
                                                
										<div class="help-block with-errors fixed-height ml-35"></div>
									</div>

								</div>

                                <div class="form-group">
                                        <div class="custom-radios div-fixed-width text-left">
                                            <div>
                                            <input id="damage_to_external_radio_and_tv_aerials" type="checkbox" {if $item->damage_to_external_radio_and_tv_aerials == 'yes'}checked{/if} class="color-5">
                                            <label for="damage_to_external_radio_and_tv_aerials">
                                                <span></span>
                                                Damage to External Radio and TV Aerials
                                            </label>
                                            </div>
                                        </div>
                                </div>

                                <div class="form-group">
                                        <div class="custom-radios div-fixed-width text-left">
                                            <div>
                                            <input id="damage_to_undergr_water_gas_pipes_or_electr_cables" type="checkbox" {if $item->damage_to_undergr_water_gas_pipes_or_electr_cables == 'yes'}checked{/if} class="color-5">
                                            <label for="damage_to_undergr_water_gas_pipes_or_electr_cables">
                                                <span></span>
                                                Damage to Underground Water, Gas Pipes or Electricity cables
                                            </label>
                                            </div>
                                        </div>
                                </div>

                                <div class="form-group">
                                        <div class="custom-radios div-fixed-width text-left">
                                            <div>
                                            <input id="workmen_compens_any_domestic_workers" type="checkbox" {if $item->workmen_compens_any_domestic_workers == 'yes'}checked{/if} class="color-5">
                                            <label for="workmen_compens_any_domestic_workers">
                                                <span></span>
                                                Workmen's Compensation for any Domestic Workers
                                            </label>
                                            </div>
                                        </div>
                                </div>

                                <div class="form-group">
                                        <div class="custom-radios div-fixed-width text-left">
                                            <div>
                                            <input id="removal_debris" type="checkbox" {if $item->removal_debris == 'yes'}checked{/if} class="color-5">
                                            <label for="removal_debris">
                                                <span></span>
                                                Removal of Debris
                                            </label>
                                            </div>
                                        </div>
                                </div>
                                
                            </div>
                        </div>
                    </fieldset>

                    
                    <div class="col-xs-12 mb-50"></div>
                    

                    <div class="form-group">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                    </div>

                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                  
                </form>


            </div>

        </div>
       
    </div>
</div>
