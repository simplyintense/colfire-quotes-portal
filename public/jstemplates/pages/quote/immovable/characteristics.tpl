
<div class="container">    
    <div class="centered-wrapper">
        <h1 class="heading-md heading-bold">Great, I just need a few  
		<span class="heading-second-line">more details</span>
		</h1>

        <div class="heading-xs mb-20">Ensure you make a selection for each item below</div>
        
        <div class="row row-contents-center">
            
            <form data-toggle="validator" role="form" class="inpage-form imm_characteristics">

                <div class="col-sm-4 col-sm-offset-2 col-xs-12">
                
                    <div class="form-group has-feedback">
                        <select name="input_on_hill" id="input_on_hill" class="form-control input-lg" data-error="Please make a selection." required>
                            <option value="">Property on a hill</option>
                            {foreach from=$settings.yes_no_list item=c key=id}
                            <option value="{$id}" {if $item->on_hill == $id}selected="selected"{/if}>{$c}</option>
                            {/foreach}
                        </select>
                        <div class="help-block with-errors fixed-height"></div>
                    </div>

                    <div class="form-group has-feedback">
                        <select name="input_wall_type" id="input_wall_type" class="form-control input-lg" data-error="Please make a selection." required>
                            <option value="">Wall Type</option>
                            {foreach from=$settings.wall_types item=c key=id}
                            <option value="{$id}" {if $item->wall_type == $id}selected="selected"{/if}>{$c}</option>
                            {/foreach}
                        </select>
                        <div class="help-block with-errors fixed-height"></div>
                    </div>

                    <div class="form-group has-feedback">
                        <select name="input_floor_type" id="input_floor_type" class="form-control input-lg" data-error="Please make a selection." required>
                            <option value="">Floor Type</option>
                            {foreach from=$settings.floor_types item=c key=id}
                            <option value="{$id}" {if $item->floor_type == $id}selected="selected"{/if}>{$c}</option>
                            {/foreach}
                        </select>
                        <div class="help-block with-errors fixed-height"></div>
                    </div>

                </div>
                <div class="col-sm-4 col-xs-12">

                    <div class="form-group has-feedback">
                        <select name="input_seafront_waterway" id="input_seafront_waterway" class="form-control input-lg" data-error="Please make a selection." required>
                            <option value="">Seafront or Riverfront</option>
                            {foreach from=$settings.yes_no_list item=c key=id}
                            <option value="{$id}" {if $item->seafront_waterway == $id}selected="selected"{/if}>{$c}</option>
                            {/foreach}
                        </select>
                        <div class="help-block with-errors fixed-height"></div>
                    </div>

                    <div class="form-group has-feedback">
                        <select name="input_roof_type" id="input_roof_type" class="form-control input-lg" data-error="Please make a selection." required>
                            <option value="">Roof Type</option>
                            {foreach from=$settings.roof_types item=c key=id}
                            <option value="{$id}" {if $item->roof_type == $id}selected="selected"{/if}>{$c}</option>
                            {/foreach}
                        </select>
                        <div class="help-block with-errors fixed-height"></div>
                    </div>

                    <div class="form-group has-feedback">
                        <select name="input_proximity_to_nearest_building" id="input_proximity_to_nearest_building" class="form-control input-lg" data-error="Please make a selection." required>
                            <option value="">Proximity to Nearest Building</option>
                            {foreach from=$settings.proximity_to_nearest_building_types item=c key=id}
                            <option value="{$id}" {if $item->proximity_to_nearest_building == $id}selected="selected"{/if}>{$c}</option>
                            {/foreach}
                        </select>
                        <div class="help-block with-errors fixed-height"></div>
                    </div>

                </div>

                <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                    
                    <div class="form-group has-feedback">
                        <select name="input_building_age" id="input_building_age" class="form-control input-lg" data-error="Please make a selection." required>
                            <option value="">Age of the Building</option>
                            {foreach from=$settings.building_age_types item=c key=id}
                            <option value="{$id}" {if $item->building_age == $id}selected="selected"{/if}>{$c}</option>
                            {/foreach}
                        </select>
                        <div class="help-block with-errors fixed-height"></div>
                    </div>
                  
                </div>

                <div class="col-xs-12 col-sm-8 col-sm-offset-2  mb-50">
                    <div id="wall_type_other_input_group" {if $item->wall_type|default:'' !== 'other'}style="display:none"{/if}>
                        <p class="heading-sm mb-20">Tell me the type of material on your wall</p>
                        <div class="form-group has-feedback">
                    	    <input type="text" class="form-control input-lg" id="input_wall_type_other" placeholder="Enter wall type" value="{$item->wall_type_other|default:''}"
                                   minlength="2" pattern="[a-zA-Z '-]+" data-error="Please enter 2 or more letters.">       
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors fixed-height"></div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-8 col-sm-offset-2  mb-50">
                    <div id="roof_type_other_input_group" {if $item->roof_type|default:'' !== 'other'}style="display:none"{/if}>
                        <p class="heading-sm mb-20">Tell me the type of material on your roof</p>
                        <div class="form-group has-feedback">
                    	    <input type="text" class="form-control input-lg" id="input_roof_type_other" placeholder="Enter roof type" value="{$item->roof_type_other|default:''}"
                                   minlength="2" pattern="[a-zA-Z '-]+" data-error="Please enter 2 or more letters.">       
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors fixed-height"></div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-8 col-sm-offset-2  mb-50">
                    <div id="floor_type_other_input_group" {if $item->floor_type|default:'' !== 'other'}style="display:none"{/if}>
                        <p class="heading-sm mb-20">Tell me the type of material on your floor</p>
                        <div class="form-group has-feedback">
                    	    <input type="text" class="form-control input-lg" id="input_floor_type_other" placeholder="Enter floor type" value="{$item->floor_type_other|default:''}"
                                   minlength="2" pattern="[a-zA-Z '-]+" data-error="Please enter 2 or more letters.">       
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors fixed-height"></div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12">
                    
                    <div class="form-group">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                    </div>

                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                  
                </div>

            </form>

        </div>
       
    </div>
</div>











