
<div class="container">    
    <div class="centered-wrapper">
        <h1 class="heading-md heading-bold mb-50">Tell me {$profile->firstname|default:''}, does your property have 
            <span class="heading-second-line">any of these features?</span>
        </h1>

        <div class="row row-contents-center">
            
            <form data-toggle="validator" role="form" class="inpage-form">

                <div class="col-sm-4 col-sm-offset-3">
                
                    <div class="form-group">
                        <div class="custom-radios div-fixed-width text-left">
                            <div>
                            <input id="fire_extinguishers" type="checkbox" {if $item->fire_extinguishers == 'yes'}checked{/if} class="color-5">
                            <label for="fire_extinguishers">
                                <span></span>
                                Fire Extinguishers
                            </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="custom-radios div-fixed-width text-left">
                            <div>
                            <input id="hose_reels" type="checkbox" {if $item->hose_reels == 'yes'}checked{/if} class="color-5">
                            <label for="hose_reels">
                                <span></span>
                                Hose Reels
                            </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="custom-radios div-fixed-width text-left">
                            <div>
                            <input id="sprinkler_system" type="checkbox" {if $item->sprinkler_system == 'yes'}checked{/if} class="color-5">
                            <label for="sprinkler_system">
                                <span></span>
                                Sprinkler System
                            </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="custom-radios div-fixed-width text-left">
                            <div>
                            <input id="smoke_detectors" type="checkbox" {if $item->smoke_detectors == 'yes'}checked{/if} class="color-5">
                            <label for="smoke_detectors">
                                <span></span>
                                Smoke Detectors
                            </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="custom-radios div-fixed-width text-left">
                            <div>
                            <input id="alarms" type="checkbox" {if $item->alarms == 'yes'}checked{/if} class="color-5">
                            <label for="alarms">
                                <span></span>
                                Alarms
                            </label>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-sm-4 col-xs-12">

                    <div class="form-group">
                        <div class="custom-radios div-fixed-width text-left">
                            <div>
                            <input id="security_personnel" type="checkbox" {if $item->security_personnel == 'yes'}checked{/if} class="color-5">
                            <label for="security_personnel">
                                <span></span>
                                Security Personnel
                            </label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="custom-radios div-fixed-width text-left">
                            <div>
                            <input id="deadbolts_padlocks" type="checkbox" {if $item->deadbolts_padlocks == 'yes'}checked{/if} class="color-5">
                            <label for="deadbolts_padlocks">
                                <span></span>
                                Deadbolts / Padlocks
                            </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="custom-radios div-fixed-width text-left">
                            <div>
                            <input id="gated_community" type="checkbox" {if $item->gated_community == 'yes'}checked{/if} class="color-5">
                            <label for="gated_community">
                                <span></span>
                                Gated Community
                            </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="custom-radios div-fixed-width text-left">
                            <div>
                            <input id="gated_property" type="checkbox" {if $item->gated_property == 'yes'}checked{/if} class="color-5">
                            <label for="gated_property">
                                <span></span>
                                Enclosed / Fenced
                            </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="custom-radios div-fixed-width text-left">
                            <div>
                            <input id="burglar_proofing" type="checkbox" {if $item->burglar_proofing == 'yes'}checked{/if} class="color-5">
                            <label for="burglar_proofing">
                                <span></span>
                                Burglar Proofing
                            </label>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-xs-12 mb-50"></div>

                <div class="col-xs-12 col-sm-5 col-sm-offset-4">
                    <p class="heading-sm mb-20">Have you had any losses or insurance claims on this property?</p>
                    <div class="form-group has-feedback div-fixed-width">

                            <div class="custom-radios">
                                <div>
                                    <input type="radio" class="color-5" id="claims_history_yes" name="claims_history" value="yes" data-error="Please make a chioce." required {if $item->claims_history == 'yes'}checked{/if}>
                                    <label for="claims_history_yes">
                                        <span></span>
                                        Yes
                                    </label>
                                </div>
                                    
                                <div class="ml-60">
                                    <input type="radio" class="color-5" id="claims_history_no" name="claims_history" value="no" data-error="Please make a chioce." required {if $item->claims_history == 'no'}checked{/if}>
                                    <label for="claims_history_no">
                                        <span></span>
                                        No
                                    </label>
                                </div>
                            </div>
                                    
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors fixed-height"></div>
                    </div>
                  
                </div>
                <div class="col-xs-12 col-sm-7 col-sm-offset-3 mb-50 text-left">
                    <div id="claims_history_descr_input_group" {if $item->claims_history|default:'no' == 'no'}style="display:none"{/if}>   
                        <div class="list-group">
			                <div id="claims_container" class="text-bigger text-bold">
                                <div class="list-group-item list-group-item-dark">
                                  {tp}Loading Claims..{/tp}
                                </div>
			                </div>
		                </div>
                        <div class="col-xs-12 mb-20 text-center">
                            <button type="button" class="btn btn-xl btn-grey add-claim"><i class="fa fa-plus" aria-hidden="true"></i> ADD CLAIM DESCRIPTION</button>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-7 col-sm-offset-3">
                    
                    <div class="form-group">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                    </div>

                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                  
                </div>

            </form>

        </div>
       
    </div>
</div>











