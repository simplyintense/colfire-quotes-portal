
<div class="container">    
    <div class="centered-wrapper">
        
        <div class="row row-eq-height row-contents-center">
            <div class="col-xs-12">
                <h1 class="heading-md heading-bold ">Where is this property located?</h1>
                <p class="heading-sm mb-50"></p>

                <form data-toggle="validator" role="form" class="inpage-form">

                    <fieldset>
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                                
                                <div class="form-group has-feedback">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="map-icon-addon"><i class="fa fa-map-marker fa-lg"></i></span>
                                        <input type="text" class="form-control input-lg" id="input_address_line_1" placeholder="Address Line 1" value="{$item->address_line_1|default:''}" aria-describedby="map-icon-addon"
                                            minlength="2" data-error="Please enter 2 or more letters." required>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="help-block with-errors fixed-height"></div>
                                </div>

                                <div class="form-group has-feedback">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="map-icon-addon-2"><i class="fa fa-map-marker fa-lg"></i></span>
                                        <input type="text" class="form-control input-lg" id="input_address_line_2" placeholder="Address Line 2" value="{$item->address_line_2|default:''}" aria-describedby="map-icon-addon-2"
                                            minlength="2" data-error="Please enter 2 or more letters.">
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="help-block with-errors fixed-height"></div>
                                </div>

                                <div class="form-group has-feedback">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="map-icon-addon-3"><i class="fa fa-map-marker fa-lg"></i></span>
                                        <input type="text" class="form-control input-lg" id="input_address_line_3" placeholder="Address Line 3" value="{$item->address_line_3|default:''}" aria-describedby="map-icon-addon-3"
                                            minlength="2" data-error="Please enter 2 or more letters.">
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="help-block with-errors fixed-height"></div>
                                </div>

                                <div class="form-group has-feedback">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="map-icon-addon-4"><i class="fa fa-map-marker fa-lg"></i></span>
                                        <input type="text" class="form-control input-lg" id="input_city" placeholder="City" value="{$item->city|default:''}" aria-describedby="map-icon-addon-4"
                                            minlength="2" data-error="Please enter 2 or more letters." required>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="help-block with-errors fixed-height"></div>
                                </div>
                                                                
                            </div>
                        </div>
                    </fieldset>

                    <div class="col-xs-12 mb-50"></div>

                    

                    <div class="form-group">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                    </div>

                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                  
                </form>


            </div>

        </div>
       
    </div>
</div>











