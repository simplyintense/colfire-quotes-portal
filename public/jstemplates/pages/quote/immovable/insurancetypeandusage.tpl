
<div class="container">    
    <div class="centered-wrapper">
        
        <div class="row row-eq-height row-contents-center">
            <div class="col-xs-12">
                <h1 class="heading-md heading-bold ">Tell me, what type of property 
                    <span class="heading-second-line">insurance do you require?</span>
                </h1>
                <p class="heading-sm mb-50"></p>

                <form data-toggle="validator" role="form" class="inpage-form">

                    <fieldset>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                
                                <div class="form-group has-feedback xsmall-notice">
                                    <select name="input_insurance_type" id="input_insurance_type" class="form-control input-lg" data-error="Please select Insurance type." required>
                                        <option value="">Select</option>
                                        {foreach from=$settings.immovable_insurance_types item=c key=id}
                                        <option value="{$id}" {if $item->insurance_type == $id}selected="selected"{/if}>{$c}</option>
                                        {/foreach}
                                    </select>
                                    <div class="help-block with-errors fixed-height"></div>
                                </div>

                                <div class="form-group has-feedback small-notice building_value_form_group" {if $item->insurance_type|default:'' == '' || $item->insurance_type|default:'' == 'contents_only'}style="display:none"{/if}>
                                    <p class="heading-sm mb-20">What is the value of the Building you would like to insure?</p>
                                </div>
                                <div class="form-group has-feedback xsmall-notice building_value_form_group" {if $item->insurance_type|default:'' == '' || $item->insurance_type|default:'' == 'contents_only'}style="display:none"{/if}>   
                                    <div class="input-group">
                    	                <span class="input-group-addon text-bigger text-bold" id="usd-icon-addon">TT<i class="fa fa-usd"></i></span>
                                        <input type="number" step="1" min="1" class="form-control input-lg" id="input_building_value" placeholder="" value="{$item->building_value|default:''}" aria-describedby="usd-icon-addon"
                                                data-error="Please enter the value of the Building you would like to insure.">
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="help-block with-errors fixed-height"></div>
                                </div>

                                <div class="form-group has-feedback small-notice contents_value_form_group" {if $item->insurance_type|default:'' == '' || $item->insurance_type|default:'' == 'building_only'}style="display:none"{/if}>
                                    <p class="heading-sm mb-20">What is the value of the Contents you would like to insure? </p>
                                </div>
                                <div class="form-group has-feedback xsmall-notice contents_value_form_group" {if $item->insurance_type|default:'' == '' || $item->insurance_type|default:'' == 'building_only'}style="display:none"{/if}>   
                                    <div class="input-group">
                    	                <span class="input-group-addon text-bigger text-bold" id="usd-icon-addon">TT<i class="fa fa-usd"></i></span>
                                        <input type="number" step="1" min="1" class="form-control input-lg" id="input_contents_value" placeholder="" value="{$item->contents_value|default:''}" aria-describedby="usd-icon-addon"
                                                data-error="Please enter the value of the Contents you would like to insure.">
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="help-block with-errors fixed-height"></div>
                                </div>

                                <p class="heading-sm mb-20">How will the property be used?</p>

                                <div class="form-group has-feedback xsmall-notice">
                                    <select name="input_usage_type" id="input_usage_type" class="form-control input-lg" data-error="Please select Propery usage." required>
                                        <option value="">Select</option>
                                        {foreach from=$settings.immovable_usage_types item=c key=id}
                                        <option value="{$id}" {if $item->usage_type == $id}selected="selected"{/if}>{$c}</option>
                                        {/foreach}
                                    </select>
                                    <div class="help-block with-errors fixed-height"></div>
                                </div>
                                
                            </div>
                        </div>
                    </fieldset>

                    <div class="col-xs-12 spacer-lg"></div>

                    

                    <div class="form-group">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                    </div>

                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                  
                </form>


            </div>

        </div>
       
    </div>
</div>











