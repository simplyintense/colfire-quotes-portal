
<div class="container">    
    <div class="centered-wrapper">
        
        <h1 class="heading-md heading-bold">
            Now {$item->firstname|default:''}, tell me, what type
            <span class="heading-second-line">of insurance you are looking for?</span>
        </h1>
        <p class="heading-sm mb-20">
            
        </p>

        
        <form data-toggle="validator" role="form" class="inpage-form">

            <div class="row row-contents-center">

                <button id="input_insurance_property" data-id="input_insurance_property" type="button" class="btn btn-insurance" value="property">
                    <img class="btn-img" src="/images/property.png" data-id="input_insurance_property">
                    <br>
                    <span class="btn-text" data-id="input_insurance_property">Property Insurance</span>
                    <i class="fa fa-check-circle fa-2x" aria-hidden="true"></i>
                </button>

                <button id="input_insurance_motor" data-id="input_insurance_motor" type="button" class="btn btn-insurance" value="motor">
                    <img class="btn-img" src="/images/motor.png" data-id="input_insurance_motor">
                    <br>
                    <span class="btn-text" data-id="input_insurance_motor">Motor Insurance</span>
                    <i class="fa fa-check-circle fa-2x" aria-hidden="true"></i>
                </button>

            </div>

            <div class="row row-contents-center">
                <div class="col-xs-12">

                    <p class="heading-xs mb-20 small-notice">Don't worry, if you want to do both you will be prompted 
                    at the end of the process to start the next quote. All 
                    information will be saved of course.
                    </p>

                    <div class="form-group hidden">
                        <button type="submit" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">Next</button>
                    </div>

                    <div class="alert alert-danger errors-container" style="display: none;"></div>

                </div>
            </div>
                  
        </form>
       
    </div>
</div>











