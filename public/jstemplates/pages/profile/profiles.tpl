<div class="container">
    <ul class="breadcrumb">
      <li><a href="{$settings->site_path}">{tp}Home{/tp}</a></li>
      <li class="active">{tp}Profiles{/tp}</li>
    </ul>


    <div class="row">
    <div class="col-xs-12 col-md-9">

	    {if $items|count == 0}
		    {if $status|default:'active' == 'active'}

			    <div class="alert alert-warning" role="alert">
            {tp}You don't have any users yet{/tp}
          </div>

		    {else}

          <div class="alert alert-warning" role="alert">{tp}You have no deleted users{/tp}</div>

        {/if}
      {else}
      <div class="list-group wallet_item" id="customer_items">


        {foreach from=$items item=i}
        <a href="{$settings->site_path}/profiles/{$i->id}" class="list-group-item item" data-id="{$i->id}" 
				    {if $i->status|default:'active' == 'hidden'}style="background: #eee"{/if}>
                    <!--
				    <div class="item_buttons hideme wallet_buttons">
					    {if $i->status|default:'active' == 'active'}
                            <button class="btn btn-default btn-xs item_button_remove"><span class="glyphicon glyphicon-trash"></span> {tp}Delete{/tp}</button>
					        <button class="btn btn-default btn-xs item_button_edit"><span class="glyphicon glyphicon-pencil"></span> {tp}Edit{/tp}</button>
					    {/if}
					    {if $i->status|default:'active' == 'hidden'}
					        <button class="btn btn-default btn-xs item_button_restore"><span class="glyphicon glyphicon-repeat"></span> {tp}Restore{/tp}</button>
					    {/if}
				    </div>
                    -->
            <h4 class="list-group-item-heading">
              {$i->login|escape:'html'|default:'no login'}
            </h4>
            <h6>
                <span class="label label-success">{$i->email|escape:'html'|default:'no email'}</span>
            </h6>

			    </a>
        {/foreach}

        <nav aria-label="items-nav">
          <ul class="pager">
            <li class="previous disabled"><a href="#"><span aria-hidden="true">&larr;</span> Previous</a></li>
            <li class="next disabled"><a href="#">Next <span aria-hidden="true">&rarr;</span></a></li>
          </ul>
        </nav>
    
	</div>	
	{/if}

    </div>
    <div class="col-xs-12 col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">
				<h3 class="panel-title">{tp}Manage Users{/tp}
                </h3>
			</div>
		    <div class="panel-body">
                <a href="#adduser" class="btn btn-orange btn-block" id="add_user_button">{tp}Add{/tp}</a>
		    </div>
	    </div>

        <div class="panel panel-default">
            <div class="panel-heading">
				<h3 class="panel-title">{tp}Search Users{/tp}
                </h3>
			</div>
		    <div class="panel-body">

                <form method="post" action="{$settings->site_path}/users" role="form" id="users_search">
                    <fieldset>
                
                        <div class="form-group">
                            <label class="dialogtypesearch label label-lightgrey" for="input_login">{t}User Login{/t}</label>
                            <input type="text" name="input_login" class="form-control" id="input_login" 
                                    placeholder="{t}User Login{/t}" value="{$search_params->login|escape:'html'|default:''}">
                        </div>

                        <div class="form-group">
                            <label class="dialogtypesearch label label-lightgrey" for="input_email">{t}User Email{/t}</label>
                            <input type="text" name="input_email" class="form-control" id="input_email" 
                                    placeholder="{t}Customer Email{/t}" value="{$search_params->email|escape:'html'|default:''}">
                        </div>
                        
                        <div class="alert alert-danger errors-container" style="display: none;">
                        </div>

                        <div class="form-group">
                            <input id="search_users_button" type="submit" class="btn btn-orange btn-block" value="{t}Search{/t}" data-loading-text="{t}Searching...{/t}">
                            {if $search_params}
                                <div class="pull-right">{tp}or{/tp} <a href="#" class="action" id="reset_users_search_button">{tp}reset search{/tp}</a></div>
                            {/if}
                        </div>

                    </fieldset>
                </form>

            </div>
	    </div>
  

    </div>
    </div>
</div>