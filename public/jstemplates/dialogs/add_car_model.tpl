<div class="modal-dialog">
  <div class="modal-content">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title" id="dialog_label">{t}Add Vehicle Model{/t}</h4>
    </div>

    <div class="modal-body modal-body-default" style="padding-bottom: 0;">
      <form method="post" action="#" role="form" id="add_unit_modal_form">
        <fieldset>

          <div class="form-group">
            <label class="label label-default" for="input_car_model">{t}Vehicle model{/t}</label>
            <input type="text" name="input_car_model" class="form-control" id="input_car_model" placeholder="{t}Enter new vehicle model{/t}">
          </div>
      
          <div class="alert alert-danger errors-container" style="display: none;">
          </div>
          
          <div class="form-group">
            <input type="submit" class="btn btn-orange pull-right" value="{t}Save{/t}" data-loading-text="{t}Saving...{/t}" id="add_model_modal_form_submit">
          </div>

        </fieldset>
      </form>
    </div>
    <div class="modal-body modal-body-success" style="display: none;"></div>
    <div class="modal-footer">
      <div class="pull-right">
      </div>
    </div>
    
  </div>
</div>

