<div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="dialog_label">{t}Edit Driver{/t}</h4>
      </div>
      <div class="modal-body modal-body-default" style="padding-bottom: 0;">

      <form method="post" action="#" role="form" id="add_unit_modal_form">
      <fieldset>

        <div class="form-group">
          <label class="label label-default" for="input_firstname">{t}Driver First Name{/t}</label>
          <input type="text" name="input_firstname" class="form-control" id="input_firstname" placeholder="{t}Driver First Name{/t}" value="{$item->firstname|escape:'html'}">
        </div>

        <div class="form-group">
          <label class="label label-default" for="input_lastname">{t}Driver Last Name{/t}</label>
          <input type="text" name="input_lastname" class="form-control" id="input_lastname" placeholder="{t}Driver Last Name{/t}" value="{$item->lastname|escape:'html'}">
        </div>

        <div class="form-group">
          <label class="label label-default" for="driving_experience">{t}How many years have you been actively driving?{/t}</label>
		  <input type="number" step="1" min="0" name="driving_experience" class="form-control" id="driving_experience" placeholder="{t}Number of years{/t}" value="{$item->driving_experience|escape:'html'}">
		</div>
        
        <div class="form-group">
          <label class="label label-default" for="young_driver_age">{t}Is the driver under the age of 25?{/t}</label>
          <select name="young_driver_age" id="young_driver_age" class="form-control">
            <option value="">{t}Select option{/t}</option>
            {foreach from=$settings.yes_no_list item=c key=id}
            <option value="{$id}" {if $item->young_driver_age == $id}selected="selected"{/if} >{$c}</option>
            {/foreach}
          </select>
        </div>

        <div class="form-group">
          <label class="label label-default" for="new_driver_permit">{t}Is driver's permit less than 24 months old?{/t}</label>
          <select name="new_driver_permit" id="new_driver_permit" class="form-control">
            <option value="">{t}Select option{/t}</option>
            {foreach from=$settings.yes_no_list item=c key=id}
            <option value="{$id}" {if $item->new_driver_permit == $id}selected="selected"{/if} >{$c}</option>
            {/foreach}
          </select>
        </div>
        
        <div class="form-group" style="display:none">
          <label class="dialogtypeadd label label-default" for="input_notes">{t}Notes{/t}</label>
          <textarea rows="2" style="resize: vertical;" maxlength="800" name="input_notes" class="form-control" id="input_notes" placeholder="{t}Notes{/t}">{$item->notes|escape:'html'}</textarea>
        </div>
    
        <div class="alert alert-danger errors-container" style="display: none;">
        </div>
        
        <div class="form-group">
          <input type="submit" class="btn btn-orange pull-right" value="{t}Save{/t}" data-loading-text="{t}Saving...{/t}" id="add_driver_modal_form_submit">
        </div>

      </fieldset>
      </form>

      </div>
      <div class="modal-body modal-body-success" style="display: none;">
      </div>
      <div class="modal-footer">
        <div class="pull-right">
        </div>
      </div>
    </form>
  </div>
</div>

