<div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="dialog_label">{t}Edit Accident{/t}</h4>
      </div>
      <div class="modal-body modal-body-default" style="padding-bottom: 0;">

      <form method="post" action="#" role="form" id="add_unit_modal_form">
      <fieldset>

        <div class="form-group">
            <label class="label label-default" for="input_accidentdate">{t}Date of Accident{/t}</label>
            <div class='input-group date' id='datetimepicker_accidentdate'>
              <input type='text' name="input_accidentdate" class="form-control" id="input_accidentdate" 
                     placeholder="{t}Date of Accident{/t}">
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar">
                </span>
              </span>
            </div>
        </div>

        <div class="form-group">
          <label class="label label-default" for="you_deemed_responsible">{t}Were you deemed responsible for the  accident?{/t}</label>
          <select name="you_deemed_responsible" id="you_deemed_responsible" class="form-control">
            <option value="">{t}Select option{/t}</option>
            {foreach from=$settings.yes_no_list item=c key=id}
            <option value="{$id}" {if $item->you_deemed_responsible == $id}selected="selected"{/if}>{$c}</option>
            {/foreach}
          </select>
        </div>

        <div class="form-group">
            <label class="label label-default" for="accident_cost">{t}How much was paid in total to you and/or any third parties, $TT{/t}</label>
            <input type="number" step="1" min="1" name="accident_cost" class="form-control" id="accident_cost" placeholder="{t}How much was paid in total to you and/or any third parties, $TT{/t}" value="{$item->accident_cost|escape:'html'}" />
        </div>

        <div class="form-group">
          <label class="dialogtypeadd label label-default" for="accident_description">{t}Brief description of accident{/t}</label>
          <textarea rows="2" style="resize: vertical;" maxlength="800" name="accident_description" class="form-control" id="accident_description" placeholder="{t}Brief description of accident{/t}">{$item->accident_description|escape:'html'}</textarea>
        </div>

        <div class="alert alert-danger errors-container" style="display: none;">
        </div>
        
        <div class="form-group">
          <input type="submit" class="btn btn-orange pull-right" value="{t}Save{/t}" data-loading-text="{t}Saving...{/t}" id="add_accident_modal_form_submit">
        </div>

      </fieldset>
      </form>

      </div>
      <div class="modal-body modal-body-success" style="display: none;">
      </div>
      <div class="modal-footer">
        <div class="pull-right">
        </div>
      </div>
    </form>
  </div>
</div>

