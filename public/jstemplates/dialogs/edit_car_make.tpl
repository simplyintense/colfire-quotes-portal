<div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="dialog_label">{t}Edit Vehicle Make{/t}</h4>
      </div>
      <div class="modal-body modal-body-default" style="padding-bottom: 0;">

      <form method="post" action="#" role="form" id="add_unit_modal_form">
        <fieldset>

          <div class="form-group">
            <label class="label label-default" for="input_make_name">{t}Vehicle Make{/t}</label>
            <input type="text" name="input_make_name" class="form-control" id="input_make_name" placeholder="{t}Vehicle Make{/t}" value="{$item->make_name|escape:'html'}">
          </div>
      
          <div class="alert alert-danger errors-container" style="display: none;">
          </div>
          
          <div class="form-group">
            <input type="submit" class="btn btn-orange pull-right" value="{t}Save{/t}" data-loading-text="{t}Saving...{/t}" id="add_make_modal_form_submit">
          </div>

        </fieldset>
      </form>

      </div>
      <div class="modal-body modal-body-success" style="display: none;">
      </div>
      <div class="modal-footer">
        <div class="pull-right">
        </div>
      </div>
    </form>
  </div>
</div>

