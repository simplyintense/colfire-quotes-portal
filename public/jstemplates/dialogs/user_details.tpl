<div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="dialog_label">{t}User Details{/t} {$item->Modbusdevice.deviceserialnumber|default:''}</h4>
      </div>
      <div class="modal-body modal-body-default" style="padding-bottom: 0;">

        <table class="table table-hover table-striped">
          <tr>
            <td><strong>{t}Login{/t}</strong></td>
            <td>{$item->login|escape:'html'|default:'&nbsp;'}</td>
          </tr>
          <tr>
            <td><strong>{t}Email{/t}</strong></td>
            <td>{$item->email|escape:'html'|default:'&nbsp;'}</td>
          </tr>
          <tr>
            <td><strong>{t}Role{/t}</strong></td>
            <td>{if $item->is_admin == 1}Administrator{else}Regular user{/if}</td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <!--
        <div class="pull-left">
          <a href="#" class="btn btn-default btn-sm" id="remove_user_button"><span class="glyphicon glyphicon-trash"></span>  {t}Remove{/t}</a>
        </div>
        -->
      </div>
    
  </div>
</div>
