<div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="dialog_label">
          {if $item->status|default:'active' == 'active'}
            {t}Are you sure that you want to delete{/t} {t}this entry{/t}?
          {else}
            {t}Are you sure that you want to remove{/t} {t}this entry{/t}?
          {/if}
        </h4>
      </div>
      <form method="post" action="{$settings->site_path}/service-lists/car-makes" role="form">
        <div class="modal-body modal-body-default" style="padding-bottom: 0;">
  
          {if $item->status|default:'active' == 'active'}
            <p class="text-success"><b>{t}You won't be able to undo the changes{/t}</b></p>
          {else}
            <p class="text-danger"><b>{t}All data will be lost{/t}</b></p>
          {/if}

          <div class="alert alert-danger errors-container" style="display: none;">
          </div>

        </div>
        <div class="modal-footer">

          <div class="form-group">
            <input type="button" class="process_button btn btn-danger pull-left" value="{if $item->status|default:'active' == 'active'}{t}Yes, Delete It{/t}{else}{t}Yes, Remove{/t}{/if}" data-loading-text="{t}Removing...{/t}">
            <input type="submit" class="btn btn-primary pull-left" value="{t}Cancel{/t}" data-loading-text="{t}Canceling...{/t}">
          </div>

        </div>
      </form>
  </div>
</div>

