<div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="dialog_label">{t}Edit Claim{/t}</h4>
      </div>
      <div class="modal-body modal-body-default" style="padding-bottom: 0;">

      <form method="post" action="#" role="form" id="add_unit_modal_form">
      <fieldset>

        <div class="form-group">
            <label class="label label-default" for="input_claimdate">{t}Date of loss/claim{/t}</label>
            <div class='input-group date' id='datetimepicker_claimdate'>
              <input type='text' name="input_claimdate" class="form-control" id="input_claimdate" 
                     placeholder="{t}Date of loss/claim{/t}">
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar">
                </span>
              </span>
            </div>
        </div>

        <div class="form-group">
            <label class="label label-default" for="input_estimated_value">{t}Estimated value of loss/claim $TT{/t}</label>
            <input type="number" step="1" min="1" name="input_estimated_value" class="form-control" id="input_estimated_value" placeholder="{t}Estimated value of loss/claim $TT{/t}" value="{$item->estimated_value|escape:'html'}" />
        </div>

        <div class="form-group">
          <label class="dialogtypeadd label label-default" for="input_claims_history_descr">{t}Brief description of loss/claim{/t}</label>
          <textarea rows="2" style="resize: vertical;" maxlength="800" name="input_claims_history_descr" class="form-control" id="input_claims_history_descr" placeholder="{t}Brief description of loss/claim{/t}">{$item->claims_history_descr|escape:'html'}</textarea>
        </div>

        <div class="alert alert-danger errors-container" style="display: none;">
        </div>
        
        <div class="form-group">
          <input type="submit" class="btn btn-orange pull-right" value="{t}Save{/t}" data-loading-text="{t}Saving...{/t}" id="add_claim_modal_form_submit">
        </div>

      </fieldset>
      </form>

      </div>
      <div class="modal-body modal-body-success" style="display: none;">
      </div>
      <div class="modal-footer">
        <div class="pull-right">
        </div>
      </div>
    </form>
  </div>
</div>

