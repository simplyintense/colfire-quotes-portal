<div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="dialog_label">{t}Invite User{/t}</h4>
      </div>
      <div class="modal-body modal-body-default registration_dialog" style="padding-bottom: 0;">

      <form method="post" action="{$settings->site_path}/user/invite" role="form" id="invite_modal_form">
      <fieldset>
        <div class="form-group">
          <label class="sr-only" for="input_login">{t}Username{/t}</label>
          <input type="text" name="login" class="form-control" id="input_invite_login" placeholder="{t}Username{/t}">
        </div>
        
        <div class="form-group">
          <label class="sr-only" for="input_email">{t}Email{/t}</label>
          <input type="email" name="email" class="form-control" id="input_invite_email" placeholder="{t}Email{/t}">
        </div>

        <div class="alert alert-danger errors-container" id="registration_invalid_password_alert" style="display: none;">
          {t}Invalid username or password{/t}
        </div>

        <div class="form-group">
          <input type="submit" class="btn btn-orange pull-left" value="{t}Invite{/t}" data-loading-text="{t}Processing...{/t}" id="registration_modal_form_submit">
        </div>

      </fieldset>
      </form>

      </div>
      <div class="modal-body modal-body-success" style="display: none;">
        <div class="alert alert-info" role="alert">{t}Thank you! Invitation was sent to user email{/t}.</div>
      </div>
      <div class="modal-footer">
       
      </div>
    </form>
  </div>
</div>

