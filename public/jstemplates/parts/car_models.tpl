{if $state == 'loading'}
	<select name="input_model" id="input_model" class="form-control input-lg" data-error="Please select model." {if $item->model|default:'' == ''}disabled{/if} required>
		<option value="">Loading Models...</option>
	</select>
	<div class="help-block with-errors fixed-height"></div>
{else}
	{if $models_list|count > 0}
		<select name="input_model" id="input_model" form="car_make_model_form" class="form-control input-lg" {if $item->make|default:'' == ''}disabled{/if} data-error="Please select model." required>
			<option value="">Model</option>
			{foreach from=$models_list item=car_model}
				<option 
					data-id="{$car_model->attributes->id}"
					value="{$car_model->attributes->model_name}" 
					{if $item->model == $car_model->attributes->model_name}selected="selected"{/if}
				>
					{$car_model->attributes->model_name}
				</option>
			{/foreach}
		</select>
		<div class="help-block with-errors fixed-height"></div>
	{else}
		<select name="input_model" id="input_model" class="form-control input-lg" data-error="Please select model." {if $item->model|default:'' == ''}disabled{/if} required>
			<option value="">Model</option>
		</select>
		<div class="help-block with-errors fixed-height"></div>
	{/if}
{/if}