{if $state == 'loading'}
	<div class="list-group-item list-group-item-dark">
		<div class="page_loading"></div>
	</div>
{else}

		{if $drivers|count > 0}
			{foreach from=$drivers item=t}
			<a href="#/{$t->id}" class="list-group-item list-group-item-dark item" data-id="{$t->id}" 
				{if $t->status|default:'active' == 'hidden'}style="display:none"{/if}>
				

				<div class="item_buttons wallet_buttons pull-right">
					<span class="item_button_edit btn-inline-edit btn-inline"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></span>
                    <span class="item_button_remove btn-inline-remove btn-inline"><i class="fa fa-user-times fa-lg" aria-hidden="true"></i></span>
				</div>


				
				<i class="fa fa-id-card fa-lg" aria-hidden="true"></i>  {$t->firstname|escape:'html'|default:''} {$t->lastname|escape:'html'|default:''}

			</a>
			{/foreach}
		{else}
		<div class="list-group-item">
			{tp}Click below to add a driver..{/tp}
		</div>	
		{/if}
{/if}