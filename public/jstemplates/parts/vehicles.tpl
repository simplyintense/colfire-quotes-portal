{if $state == 'loading'}
	<div class="list-group-item list-group-item-dark">
		<div class="page_loading"></div>
	</div>
{else}

		{if $vehicles|count > 0}
			{foreach from=$vehicles item=t}
			<a href="#/{$t->id}" class="list-group-item list-group-item-dark item" data-id="{$t->id}" 
				{if $t->status|default:'active' == 'hidden'}style="display:none"{/if}>
				

				<div class="item_buttons wallet_buttons pull-right">
					<span class="item_button_edit btn-inline-edit btn-inline"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></span>
                    <span class="item_button_remove btn-inline-remove btn-inline"><i class="fa fa-times fa-lg" aria-hidden="true"></i></span>
				</div>


				
				<i class="fa fa-car fa-lg" aria-hidden="true"></i>  {$t->make|escape:'html'|default:''} {$t->model|escape:'html'|default:''}

			</a>
			{/foreach}
		{else}
		<div class="list-group-item">
			{tp}No vehicles{/tp}
		</div>	
		{/if}
{/if}