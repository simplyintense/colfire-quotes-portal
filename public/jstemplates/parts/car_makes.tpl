{if $state == 'loading'}
	<select name="input_make" id="input_make" class="form-control input-lg" data-error="Please select make." required>
		<option value="">Loading Makes...</option>
	</select>
	<div class="help-block with-errors fixed-height"></div>
{else}
	{if $makes_list|count > 0}
		<select name="input_make" id="input_make" form="car_make_model_form" class="form-control input-lg" data-error="Please select make." required>
			<option value="">Make</option>
			{foreach from=$makes_list item=car_make}
				<option 
					data-id="{$car_make->attributes->id}" 
					value="{$car_make->attributes->make_name}"
					{if $item->make == $car_make->attributes->make_name}selected="selected"{/if}
				>
					{$car_make->attributes->make_name}
				</option>
			{/foreach}
		</select>
		<div class="help-block with-errors fixed-height"></div>
	{else}
		<select name="input_make" id="input_make" class="form-control input-lg" data-error="Please select make." required>
			<option value="">Make</option>
		</select>
		<div class="help-block with-errors fixed-height"></div>
	{/if}
{/if}