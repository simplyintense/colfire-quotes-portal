
<div class="container">    
    <div class="centered-wrapper">
        
        <div class="row row-eq-height row-contents-center">
            <div class="col-xs-12">
                <h1 class="heading-md heading-bold ">Your Session Has Expired</h1>
                <p class="heading-sm mb-50">Please navigate to Home page and start over</p>

                <form data-toggle="validator" role="form" class="inpage-form heading-fixed-max-width">

                    <div class="col-xs-12 mb-20"></div>

                    <div class="form-group">
                        <a href="/#" class="btn btn-orange btn-xl btn-form next-step" data-loading-text="{t}Processing...{/t}">HOME</a>
                    </div>

                    <div class="alert alert-danger errors-container" style="display: none;"></div>
                  
                </form>


            </div>

        </div>
       
    </div>
</div>











