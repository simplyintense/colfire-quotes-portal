﻿module.exports = function (sequelize, DataTypes) {
    var CarMakesList = sequelize.define('CarMakesList', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        
        make_name: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 1, max: 255 }
                    ],
                    msg: "Make should have from 1 to 255 characters"
                }
            },
            allowNull: false
        },
        
    }, {
            timestamps: true,
            indexes: [],
            freezeTableName: true,
            tableName: 'car_makes',
            classMethods: {

            },
            instanceMethods: {
                
            }
        });

    return CarMakesList;

};