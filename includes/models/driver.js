﻿module.exports = function (sequelize, DataTypes) {
    var Driver = sequelize.define('Driver', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        status: {
            type: DataTypes.ENUM('active', 'hidden'),
            defaultValue: 'active',
            validate: {
                isIn: {
                    args: [
                        ['active', 'hidden']
                    ],
                    msg: "Invalid driver status"
                }
            }
        },
        firstname: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 1, max: 255 }
                    ],
                    msg: "First Name should have from 1 to 255 characters"
                }
            }
        },
        lastname: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 1, max: 255 }
                    ],
                    msg: "Last Name should have from 1 to 255 characters"
                }
            }
        },
        young_driver_age: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for question: Any drivers under the age of 25?"
                }
            }
        },
        driving_experience: {
            type: DataTypes.INTEGER,
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 100 }
                    ],
                    msg: "Number of years of driving experience should be between 0 and 100"
                }
            }
        },
        new_driver_permit: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for question: Any drivers who has driver permit for less then 24 month?"
                }
            }
        },
        notes: {
            type: DataTypes.STRING(900),
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 900 }
                    ],
                    msg: "Notes can not be longer 900 characters"
                }
            }
        }
    }, {
            timestamps: true,
            indexes: [],
            freezeTableName: true,
            tableName: 'drivers',
            classMethods: {

            },
            instanceMethods: {
                
            }
        });

    return Driver;

};