﻿module.exports = function (sequelize, DataTypes) {
    var Claim = sequelize.define('Claim', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        status: {
            type: DataTypes.ENUM('active', 'hidden'),
            defaultValue: 'active',
            validate: {
                isIn: {
                    args: [
                        ['active', 'hidden']
                    ],
                    msg: "Invalid claim status"
                }
            }
        },
        claim_date: {
            type: DataTypes.DATE,
            validate: {
                isDate: {
                    msg: "Invalid claim date"
                }
            }
        },
        estimated_value: DataTypes.DECIMAL(18, 2),
        claims_history_descr: {
            type: DataTypes.STRING(900),
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 900 }
                    ],
                    msg: "Claims history description can not be longer 900 characters"
                }
            }
        },
    }, {
            timestamps: true,
            indexes: [],
            freezeTableName: true,
            tableName: 'claims',
            classMethods: {

            },
            instanceMethods: {
                
            }
        });

    return Claim;

};