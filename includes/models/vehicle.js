﻿module.exports = function (sequelize, DataTypes) {
    var Vehicle = sequelize.define('Vehicle', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        status: {
            type: DataTypes.ENUM('active', 'hidden'),
            defaultValue: 'active',
            validate: {
                isIn: {
                    args: [
                        ['active', 'hidden']
                    ],
                    msg: "Invalid vehicle status"
                }
            }
        },
        insurance_type_comprehensive: {
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid insurance type"
                }
            }
        },
        insurance_type_third_party_fire_theft: {
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid insurance type"
                }
            }
        },
        insurance_type_third_party: {
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid insurance type"
                }
            }
        },
        current_value: DataTypes.DECIMAL(18, 2),
        make: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 1, max: 255 }
                    ],
                    msg: "Make should have from 1 to 255 characters"
                }
            }
        },

        vehicle_registration_number: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 255 }
                    ],
                    msg: "Vehicle registration number should have from 1 to 255 characters"
                }
            }
        },
        
        vehicle_protected: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid vehicle storage type"
                }
            }
        },


        model: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 1, max: 255 }
                    ],
                    msg: "Model should have from 1 to 255 characters"
                }
            }
        },
        year: {
            type: DataTypes.INTEGER,
            validate: {
                isInt: {
                    args: [
                        { min: 1900, max: 2050 }
                    ],
                    msg: "Wrong vehicle year"
                }
            }
        },

        vehicle_age: {
            type: DataTypes.INTEGER,
            validate: {
                isInt: {
                    args: [
                        { min: 0, max: 200 }
                    ],
                    msg: "Wrong vehicle age"
                }
            }
        },

        vehicle_origin: {
            type: DataTypes.ENUM('new_from_dealership', 'foreign_used', 'locally_used'),
            validate: {
                isIn: {
                    args: [
                        ['new_from_dealership', 'foreign_used', 'locally_used']
                    ],
                    msg: "Invalid vehicle origin type"
                }
            }
        },

        custom_descr_make_model: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 255 }
                    ],
                    msg: "Custom description of make and model should have from 1 to 255 characters"
                }
            }
        },

        usage_type: {
            type: DataTypes.ENUM('agriculture','company','private','racing','carrying_own_goods','carrying_goods_for_others','rental','taxi','other'),
            validate: {
                isIn: {
                    args: [
                        ['agriculture','company','private','racing','carrying_own_goods','carrying_goods_for_others','rental','taxi','other']
                    ],
                    msg: "Invalid insurance type"
                }
            }
        },
        accidents_recently: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for question: Have you had an acccident in the last 5 years?"
                }
            }
        },
        windscreen_coverage: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for Windscreen Coverage option"
                }
            }
        },
        windscreen_coverage_amount: DataTypes.DECIMAL(18, 2),
        emergency_roadside_assist: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for Emergency Roadside Assistance"
                }
            }
        },
        windscreen_coverage_3rd_party: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for Windscreen Coverage 3rd Party option"
                }
            }
        },
        windscreen_coverage_amount_3rd_party: DataTypes.DECIMAL(18, 2),
        emergency_roadside_assist_3rd_party: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for Emergency Roadside Assistance 3rd party"
                }
            }
        },
        windscreen_coverage_3rd_party_fire: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for Windscreen Coverage 3rd Party Fire and Theft option"
                }
            }
        },
        windscreen_coverage_amount_3rd_party_fire: DataTypes.DECIMAL(18, 2),
        emergency_roadside_assist_3rd_party_fire: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for Emergency Roadside Assistance 3rd Party Fire and Theft"
                }
            }
        },
        flood_special_perils: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for Flood and Special Perils option"
                }
            }
        },
        partial_waiver_of_excess: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for Partial Waiver of Excess option"
                }
            }
        },
        loss_of_use: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for Loss of Use option"
                }
            }
        },
        new_for_old: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for New for Old option"
                }
            }
        },
        no_claim_discount_transfer: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for option: I have a No claim discount.. "
                }
            }
        },
        no_claim_discount_years: {
            type: DataTypes.INTEGER,
            validate: {
                isInt: {
                    args: [
                        { min: 0 }
                    ],
                    msg: "Wrong No Claim discount years value"
                }
            }
        },
        member_of_credit_union: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for option: I am currently a member of a credit union"
                }
            }
        },
        live_close_to_work: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for option: I live within 20km of my workplace"
                }
            }
        },
        defensive_driver_cert: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for option: I have a defensive driver certificate"
                }
            }
        },
        defensive_driver_school: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 255 }
                    ],
                    msg: "Defensive Driver School Name should have from 1 to 255 characters"
                }
            }
        },
        loan: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for option: I have a loan on this vehicle"
                }
            }
        },
        bank_name: {
        type: DataTypes.ENUM('first_citizens', 'republic_bank_limited', 'scotiabank_limited', 'rbc_royal_bank_limited', 'jmmb', 'other',''),
        validate: {
            isIn: {
                args: [
                    ['first_citizens', 'republic_bank_limited', 'scotiabank_limited', 'rbc_royal_bank_limited', 'jmmb', 'other', '']
                ],
                msg: "Invalid credit card bank entry"
                }
            }
        },
        credit_card: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for option: I will be paying with a credit card"
                }
            }
        },
        custom_loan_bank: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 255 }
                    ],
                    msg: "Bank Name should have from 1 to 255 characters"
                }
            }
        },
        credit_card_bank: {
            type: DataTypes.ENUM('first_citizens', 'republic_bank_limited', 'scotiabank_limited', 'rbc_royal_bank_limited', 'jmmb', 'other', ''),
            validate: {
                isIn: {
                    args: [
                        ['first_citizens', 'republic_bank_limited', 'scotiabank_limited', 'rbc_royal_bank_limited', 'jmmb', 'other', '']
                    ],
                    msg: "Invalid credit card bank entry"
                }
            }
        },
        credit_card_bank_custom: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 255 }
                    ],
                    msg: "Bank Name should have from 1 to 255 characters (credit card)"
                }
            }
        },
        have_child: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for option: I have a child under 4 years of age"
                }
            }
        },
        sole_driver: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for option: I will be the sole driver of the vehicle"
                }
            }
        },
        notes: {
            type: DataTypes.STRING(900),
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 900 }
                    ],
                    msg: "Notes can not be longer 900 characters"
                }
            }
        }
    }, {
            timestamps: true,
            indexes: [],
            freezeTableName: true,
            tableName: 'vehicles',
            classMethods: {

            },
            instanceMethods: {
                
            }
        });

    return Vehicle;

};