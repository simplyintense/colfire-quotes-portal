var crypto = require('crypto');
var rfr = require('rfr');
var mailer = rfr('includes/mailer.js');
var moment = require('moment');


module.exports = function(sequelize, DataTypes) {
	var User = sequelize.define('User', {
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
		email: {
			type: DataTypes.STRING(255),
			validate: {
				isEmail: {
					msg: "Invalid email"
				},
				isUnique: function(value, next) {
					var that = this;
					sequelize.db.User.findOne({
							where: {
								email: value,
								id: {
									$ne: this.id
								}
							},
							attributes: ['id']
						})
						.then(function(foundUser) {
							if (foundUser)
								throw new Error('Email is already in use');
							next();
						}).catch(function(err) {
							throw new Error('Email is already in use');
						});
				}
			}
		},
		type: {
			type: DataTypes.STRING(20),
			defaultValue: 'default',
			validate: {
				isIn: {
					args: [
						['default', 'facebook']
					],
					msg: "Invalid user type"
				}
			}
		},
		password: DataTypes.STRING(50),
		login: {
			type: DataTypes.STRING(255),
			validate: {
				isUnique: function(value, next) {
					var that = this;
					sequelize.db.User.findOne({
							where: {
								login: value,
								id: {
									$ne: this.id
								}
							},
							attributes: ['id']
						})
						.then(function(foundUser) {
							if (foundUser)
								throw new Error('This username is already in use');
							next();
						}).catch(function(err) {
							throw new Error('This username is already in use');
						});
				}
			}
		},
		is_demo: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			allowNull: false
		},
		is_admin: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			allowNull: false
		},
		registration_date: DataTypes.INTEGER,
		activity_date: DataTypes.INTEGER,
		registration_ip: DataTypes.STRING(20),
		activity_ip: DataTypes.STRING(20),
		confirmation_code: DataTypes.STRING(255),
		remove_account_code: DataTypes.STRING(255),
		password_restore_code: DataTypes.STRING(255),
		is_banned: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			allowNull: false
		}
	}, {
		timestamps: false,
		freezeTableName: true,
		tableName: 'users',
		classMethods: {
			hashPassword: function(password) {
				return crypto.createHash('md5').update(password + 'password_salt').digest("hex");
			},
			register: function(params) {
				var params = params || {};
				var type = params.type || 'default';
				var login = params.login || '';
				var password = params.password || '';
				var email = params.email || '';
				var ip = params.ip || null;
				var is_demo = 1;

				password = this.hashPassword(password);

				return new sequelize.Promise(function(resolve, reject) {
					var user = sequelize.db.User.build({
						type: type,
						email: email,
						login: login,
						password: password
					});
					user.registration_date = Date.now() / 1000 | 0;
					user.registration_ip = ip;
					user.is_demo = is_demo;

					user.save().then(function(user) {
						resolve(user);
					}, function(err) {
						reject(err);
					});
				});

			},
			updatePassword: function(code, hash, password) {
				return new sequelize.Promise(function(resolve, reject) {
					if (password.length < 6)
						return reject('Password is too short');

					sequelize.db.User.findOne({
						where: {
							password_restore_code: code
						}
					}).then(function(user) {
						if (!user)
							return reject('Invalid reset password code');
						var restore_password_hash = sequelize.db.User.hashPassword(user.id + user.password_restore_code);
						if (hash != restore_password_hash)
							return reject('Invalid reset password code');

						password = sequelize.db.User.hashPassword(password);
						user.password = password;
						return user.save();
					}).then(function(user) {
						resolve(user);
					}, function(err) {
						reject(err);
					});
				});
			},
			resetPassword: function(email,template_name) {
                var template_name = template_name || 'restore_password';
				var restore_password_hash = '';
				return new sequelize.Promise(function(resolve, reject) {
					sequelize.db.User.findOne({
						where: {
							email: email
						}
					}).then(function(user) {
						if (!user)
							return reject('Can not find this email in our database');
						user.password_restore_code = sequelize.db.User.hashPassword("" + user.id + Math.random());
						restore_password_hash = sequelize.db.User.hashPassword(user.id + user.password_restore_code);
						return user.save();
					}).then(function(user) {
						mailer.sendTemplate(template_name, user.email, {
							login: user.login,
							password_restore_code: user.password_restore_code,
							password_restore_hash: restore_password_hash
						});
						return resolve(user);
					}).catch(function(e) {
						return reject('Can not find this email in our database');
					});
				});
			},
			getByAuthCode: function(params, callback) {
				var params = params || {};
				var ip = params.ip || null;
				var auth_code = params.auth_code || null;

				return new sequelize.Promise(function(resolve, reject) {
					sequelize.db.Authentication.findOne({
						where: {
							auth_code: auth_code
						}
					}).then(function(authentication) {
						if (!authentication)
							return reject('Invalid auth code');
						return authentication.getUser();
					}).then(function(user) {
						user.activity_ip = ip;
						user.activity_date = Date.now() / 1000 | 0;
						user.save();
						return resolve(user);
					}).catch(function(e) {
						return reject('Invalid auth code');
					});
				});
			},
			signIn: function(params) {
				var params = params || {};
				var username = params.username || '';
				var password = params.password || '';

				password = this.hashPassword(password);

				return sequelize.db.User.findOne({
					where: {
						$or: [{
							login: username
						}, {
							email: username
						}],
						password: password
					}
				});
			}
		},
		instanceMethods: {
			auth: function(params) {
				var md5sum = crypto.createHash('md5');
				var hash = '' + md5sum.update('' + Math.random() + this.id).digest('hex');
				return this.createAuthentication({
					auth_code: hash
				});
			},
			signOut: function() {
				return sequelize.query("SELECT * FROM `authentications` WHERE user_id = :user_id", {
					replacements: {
						user_id: this.id
					}
				});
			},
			removeAccount: function() {
				var user = this;
				return new sequelize.Promise(function(resolve, reject) {
					user.remove_account_code = sequelize.db.User.hashPassword("del" + user.id + Math.random());
					user.save().then(function(user) {
						mailer.sendTemplate('remove_account', user.email, {
							login: user.login,
							remove_account_code: user.remove_account_code
						});
						return resolve(user);
					});
				});
			},
			removeAccountConfirm: function(code) {
				var user = this;
				return new sequelize.Promise(function(resolve, reject) {
					if (user.remove_account_code && user.remove_account_code == code)
						return user.destroy().then(function() {
							return resolve(true);
						});
					else
						return resolve(false);
				});
			},
			fillProfile: function(params) {
				var params = params || {};
				var login = params.login || '';
				var email = params.email || '';
				var password = params.password || '';
				var ip = params.ip || null;

				password = sequelize.db.User.hashPassword(password);

				return false;

				//return this.save();
			},
			update: function(params) {
				var params = params || {};
				var password = params.password || '';
				var current_password = params.current_password || '';

				current_password = sequelize.db.User.hashPassword(current_password);
				if (current_password == this.password) {
					password = sequelize.db.User.hashPassword(password);
					this.password = password;
				} else {
					return false;
				}
				return this.save();
			}
			
		}
	});

	return User;
};
