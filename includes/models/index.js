var fs = require('fs');
var path = require('path');
var Sequelize = require('sequelize');
var rfr = require('rfr');
var basename = path.basename(module.filename);
var config = rfr('includes/config.js');
var db = {};
db.moment = require('moment-timezone'); //require('moment');
var moment = db.moment;
console.log('Set api timezone to America/Port_of_Spain and locale to en-tt');
db.moment.tz.setDefault('America/Port_of_Spain');
db.moment.locale('en-tt');

if (config.sequelizeLogging) {
    var options = {
        logging: function (str) {
            console.log('---- ----');
            console.log(str);
        }
    }
} else {
    var options = {
        logging: false
    }
}



if (config.database.use_env_variable) {
  var sequelize = new Sequelize(process.env[config.database.use_env_variable], options);
} else {
  options.host = config.database.host || null;
  options.dialect = config.database.dialect || null;
  options.storage = config.database.storage || null;

  if (config.database.dialectOptions)
    options.dialectOptions = config.database.dialectOptions;

  if (options.storage)
    options.storage = path.join(rfr.root, options.storage);

  if (config.database.dialectModulePath)
    options.dialectModulePath = path.join(rfr.root, config.database.dialectModulePath);

    var sequelize = new Sequelize(config.database.database, config.database.username, config.database.password, options);
}
/*
var sequelize = new Sequelize('nrgdatabase', 'moneybudgetuser', '340$Uuxwp7Mcxo7Khy', {
    host: 'localhost',
    dialect: 'mssql'
});
*/
/*
	sequelize
	  .authenticate()
	  .then(function(err) {
		console.log('Connection has been established successfully.');
	  },function (err) {
		console.log('Unable to connect to the database:', err);
	  })
	  .catch(function (err) {
		console.log('Unable to connect to the database:', err);
	  });
*/


fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== basename);
  })
  .forEach(function(file) {
    if (file.slice(-3) !== '.js') return;
    var model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

sequelize.db = db;
db.sequelize = sequelize;
db.Sequelize = Sequelize;

//// Foreign keys
db['Authentication'].belongsTo(db['User'], {
  foreignKey: 'user_id',
  constraints: false
});
db['User'].hasMany(db['Authentication'], {
  foreignKey: 'user_id',
  constraints: false,
  onDelete: 'CASCADE'
});

db['Profile'].belongsTo(db['User'], {
  foreignKey: 'user_id',
  constraints: false
});
db['User'].hasOne(db['Profile'], {
  foreignKey: 'user_id',
  constraints: false,
  onDelete: 'CASCADE'
});


db['Quote'].belongsTo(db['User'], {
  foreignKey: 'user_id',
  constraints: false
});
db['User'].hasMany(db['Quote'], {
  foreignKey: 'user_id',
  constraints: false,
  onDelete: 'CASCADE'
});

db['Vehicle'].belongsTo(db['Quote'], {
  foreignKey: 'quote_id',
  constraints: false
});
db['Quote'].hasMany(db['Vehicle'], {
  foreignKey: 'quote_id',
  constraints: false,
  onDelete: 'CASCADE'
});

db['Driver'].belongsTo(db['Vehicle'], {
  foreignKey: 'vehicle_id',
  constraints: false
});
db['Vehicle'].hasMany(db['Driver'], {
  foreignKey: 'vehicle_id',
  constraints: false,
  onDelete: 'CASCADE'
});

db['Immovable'].belongsTo(db['Quote'], {
  foreignKey: 'quote_id',
  constraints: false
});
db['Quote'].hasMany(db['Immovable'], {
  foreignKey: 'quote_id',
  constraints: false,
  onDelete: 'CASCADE'
});

db['Claim'].belongsTo(db['Immovable'], {
  foreignKey: 'immovable_id',
  constraints: false
});
db['Immovable'].hasMany(db['Claim'], {
  foreignKey: 'immovable_id',
  constraints: false,
  onDelete: 'CASCADE'
});

db['Accident'].belongsTo(db['Vehicle'], {
  foreignKey: 'vehicle_id',
  constraints: false
});
db['Vehicle'].hasMany(db['Accident'], {
  foreignKey: 'vehicle_id',
  constraints: false,
  onDelete: 'CASCADE'
});

db['CarMakesList'].hasMany(db['CarModelsList'], {
  foreignKey: 'car_make_id',
  constraints: false,
  onDelete: 'CASCADE'
});
db['CarModelsList'].belongsTo(db['CarMakesList'], {
  foreignKey: 'car_make_id',
  constraints: false
});


module.exports = db;