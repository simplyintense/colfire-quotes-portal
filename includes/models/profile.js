﻿module.exports = function (sequelize, DataTypes) {
    var Profile = sequelize.define('Profile', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        status: {
            type: DataTypes.ENUM('active', 'hidden'),
            defaultValue: 'active',
            validate: {
                isIn: {
                    args: [
                        ['active', 'hidden']
                    ],
                    msg: "Invalid profile status"
                }
            }
        },
        firstname: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 1, max: 255 }
                    ],
                    msg: "First Name should have from 1 to 255 characters"
                }
            }
        },
        lastname: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 1, max: 255 }
                    ],
                    msg: "Last Name should have from 1 to 255 characters"
                }
            }
        },
        home_address: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 1, max: 255 }
                    ],
                    msg: "Home address should have from 1 to 255 characters"
                }
            }
        },
        email: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 1, max: 255 }
                    ],
                    msg: "Email should have from 1 to 255 characters"
                }
            }
        },
        occupation: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 1, max: 255 }
                    ],
                    msg: "Occupation should have from 1 to 255 characters"
                }
            }
        },
        phone_number: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 1, max: 255 }
                    ],
                    msg: "Phone number should have from 1 to 255 characters"
                }
            }
        },
        gender: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 1, max: 255 }
                    ],
                    msg: "Gender should have from 1 to 255 characters"
                }
            }
        },
        dateofbirth: {
            type: DataTypes.DATE,
            validate: {
                isDate: {
                    msg: "Invalid  date of birth"
                }
            }
        },
        current_insurer: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 1, max: 255 }
                    ],
                    msg: "Current Insurer Name should have from 1 to 255 characters"
                }
            }
        },
        cur_cust_years: {
            type: DataTypes.INTEGER,
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 100 }
                    ],
                    msg: "Number of years should be between 0 and 100"
                }
            }
        },
        notes: {
            type: DataTypes.STRING(900),
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 900 }
                    ],
                    msg: "Notes can not be longer 900 characters"
                }
            }
        }
    }, {
            timestamps: true,
            indexes: [],
            freezeTableName: true,
            tableName: 'profiles',
            classMethods: {

            },
            instanceMethods: {
                
            }
        });

    return Profile;

};