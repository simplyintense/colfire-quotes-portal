﻿var rfr = require('rfr');
var mailer = rfr('includes/mailer.js');
var config = rfr('includes/config.js');

module.exports = function (sequelize, DataTypes) {
    var Quote = sequelize.define('Quote', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        ref_num: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 1, max: 255 }
                    ],
                    msg: "Refference number should have from 1 to 255 characters"
                }
            }
        },
        status: {
            type: DataTypes.ENUM('active','submitted','hidden'),
            defaultValue: 'active',
            validate: {
                isIn: {
                    args: [
                        ['active','submitted','hidden']
                    ],
                    msg: "Invalid quote status"
                }
            }
        },
        insurance_type: {
            type: DataTypes.ENUM('property', 'motor'),
            validate: {
                isIn: {
                    args: [
                        ['property', 'motor']
                    ],
                    msg: "Invalid insurance type"
                }
            }
        },
        notes: {
            type: DataTypes.STRING(900),
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 900 }
                    ],
                    msg: "Notes can not be longer 900 characters"
                }
            }
        },
        rating: {
            type: DataTypes.INTEGER,
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 5 }
                    ],
                    msg: "Rating value should be between 0 and 5"
                }
            }
        },
        ratingFeedback: {
            type: DataTypes.STRING(900),
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 900 }
                    ],
                    msg: "Feedback can not be longer 900 characters"
                }
            }
        }
    }, {
            timestamps: true,
            indexes: [],
            freezeTableName: true,
            tableName: 'quotes',
            classMethods: {
                sendConfirmationEmail: function (params) {
                    var params = params || {};
                    var quote_id = params.quote_id || 0;

                    var admin_emails = 'quotations@colfire.com';

                    //var admin_emails = 'ameeta@simplyintense.com';
                    //admin_emails += ',' + 'ashraffali@colfire.com';

                    //var admin_emails = 'mylawacad@gmail.com';
                    //var admin_emails = 'skoval.my@gmail.com';

                    var includedTables = [];
                    
                    includedTables.push(
                        {
                            model: sequelize.db.User,
                            attributes: ['id'],
                            include: {
                                model: sequelize.db.Profile
                            }
                        },
                        {
                            model: sequelize.db.Vehicle,
                            where: {
                                status:'active',
                            },
                            required: false,
                            include:[
                                {
                                    model: sequelize.db.Driver,
                                    where: {
                                        status:'active',
                                    },
                                    required: false,
                                },
                                {
                                    model: sequelize.db.Accident,
                                    where: {
                                        status:'active',
                                    },
                                    required: false,
                                }
                            ]
                        },
                        {
                            model: sequelize.db.Immovable,
                            include:{
                                model: sequelize.db.Claim,
                                where: {
                                    status:'active',
                                },
                                required: false,
                            }
                        }
                    );
                    
                    //sequelize.db.User.findAll({
                    //    attributes: ['email'],
                    //    where: {
                    //        is_admin: 1
                    //    }
                    //}).then(function(users){

                    //    users.forEach(function(singleUser){
                    //        var email = singleUser.get('email');

                    //        if(admin_emails == ''){
                    //            admin_emails += email;   
                    //        }else{
                    //            admin_emails += ',' + email;   
                    //        }
                    //    });

                    //    return sequelize.db.Quote.findOne({
                    //        where: {
                    //            id: quote_id
                    //        },
                    //        include: includedTables
                    //    });
                    //})

                    sequelize.db.Quote.findOne({
                        where: {
                            id: quote_id
                        },
                        include: includedTables
                    }).then(function (quote) {
                        if(!quote) throw new errors.NotFoundError();
                        
                        //property email to admin
                        mailer.sendTemplate('insurance_admin', admin_emails, {
                            
                            firstname: quote.User.Profile.firstname,
                            lastname: quote.User.Profile.lastname,
                            ref_num: quote.ref_num,
                            home_address: quote.User.Profile.home_address,
                            email: quote.User.Profile.email,
                            occupation: quote.User.Profile.occupation,
                            phone: quote.User.Profile.phone_number,
                            gender: quote.User.Profile.gender,
                            dob: sequelize.db.moment(quote.User.Profile.dateofbirth).format(config.date_format),
                            current_insurer: quote.User.Profile.current_insurer,
                            cur_cust_years: quote.User.Profile.cur_cust_years,

			            }, { item: quote });

                        //property email to customer
                        mailer.sendTemplate('insurance_customer', quote.User.Profile.email, {
                            firstname: quote.User.Profile.firstname,
                            lastname: quote.User.Profile.lastname,
                            ref_num: quote.ref_num
			            }, { item: quote });

                    });

                
                }
            },
            instanceMethods: {
                
            }
        });

    return Quote;

};