﻿module.exports = function (sequelize, DataTypes) {
    var Immovable = sequelize.define('Immovable', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        status: {
            type: DataTypes.ENUM('active', 'hidden'),
            defaultValue: 'active',
            validate: {
                isIn: {
                    args: [
                        ['active', 'hidden']
                    ],
                    msg: "Invalid immovable status"
                }
            }
        },
        insurance_type: {
            type: DataTypes.ENUM('building_only','contents_only','building_and_contents'),
            validate: {
                isIn: {
                    args: [
                        ['building_only','contents_only','building_and_contents']
                    ],
                    msg: "Invalid insurance type"
                }
            }
        },
        building_value: DataTypes.DECIMAL(18, 2),
        contents_value: DataTypes.DECIMAL(18, 2),
        usage_type: {
            type: DataTypes.ENUM('rent','owner_occupied','commercial','residential'),
            validate: {
                isIn: {
                    args: [
                        ['rent','owner_occupied','commercial','residential']
                    ],
                    msg: "Invalid insurance type"
                }
            }
        },
        address_line_1: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 1, max: 255 }
                    ],
                    msg: "Address line 1 should have from 1 to 255 characters"
                }
            }
        },
        address_line_2: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 255 }
                    ],
                    msg: "Address line 2 should have from 1 to 255 characters"
                }
            }
        },
        address_line_3: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 255 }
                    ],
                    msg: "Address line 3 should have from 1 to 255 characters"
                }
            }
        },
        city: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 1, max: 255 }
                    ],
                    msg: "City should have from 1 to 255 characters"
                }
            }
        },
        on_hill: {
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for property on a hill type"
                }
            }
        },
        seafront_waterway: {
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for seafront or riverfront type"
                }
            }
        },
        wall_type: {
            type: DataTypes.ENUM('brick','asphalt','mixed','stone','hollow_clay_blocks','concrete','timber','other'),
            validate: {
                isIn: {
                    args: [
                        ['brick','asphalt','mixed','stone','hollow_clay_blocks','concrete','timber','other']
                    ],
                    msg: "Invalid wall type"
                }
            }
        },
        wall_type_other: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 255 }
                    ],
                    msg: "Wall type other should have from 1 to 255 characters"
                }
            }
        },
        roof_type: {
            type: DataTypes.ENUM('slates','asphalt','galvanized_iron_sheets','tiles','metal','concrete','other'),
            validate: {
                isIn: {
                    args: [
                        ['slates','asphalt','galvanized_iron_sheets','tiles','metal','concrete','other']
                    ],
                    msg: "Invalid roof type"
                }
            }
        },
        roof_type_other: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 255 }
                    ],
                    msg: "Roof type other should have from 1 to 255 characters"
                }
            }
        },
        floor_type: {
            type: DataTypes.ENUM('stone','timber','concrete','tiles','other'),
            validate: {
                isIn: {
                    args: [
                        ['stone','timber','concrete','tiles','other']
                    ],
                    msg: "Invalid floor type"
                }
            }
        },
        floor_type_other: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 255 }
                    ],
                    msg: "Floor type other should have from 1 to 255 characters"
                }
            }
        },
        proximity_to_nearest_building: {
            type: DataTypes.ENUM('0','5','50','100','500','1000+'),
            validate: {
                isIn: {
                    args: [
                        ['0','5','50','100','500','1000+']
                    ],
                    msg: "Invalid floor type"
                }
            }
        },
        building_age: {
            type: DataTypes.ENUM('0y','5y','10y','20y','30y','50y','70y','100y+'),
            validate: {
                isIn: {
                    args: [
                        ['0y','5y','10y','20y','30y','50y','70y','100y+']
                    ],
                    msg: "Invalid floor type"
                }
            }
        },
        fire_extinguishers: {
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for fire extinguishers type"
                }
            }
        },
        hose_reels: {
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for hose reels type"
                }
            }
        },
        sprinkler_system: {
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for sprinkler system"
                }
            }
        },
        smoke_detectors: {
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for smoke detectors"
                }
            }
        },
        alarms: {
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for alarms type"
                }
            }
        },
        security_personnel: {
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for security personnel"
                }
            }
        },
        deadbolts_padlocks: {
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for deadbolts / padlocks"
                }
            }
        },
        gated_community: {
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for gated community"
                }
            }
        },
        gated_property: {
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for Enclosed/Fenced"
                }
            }
        },
        burglar_proofing: {
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for burglar proofing"
                }
            }
        },
        claims_history: {
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for claims history"
                }
            }
        },
        flood: {
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for flood"
                }
            }
        },
        subsidence_and_landslip:{
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for subsidence and landslip"
                }
            }
        },

        subsidence_and_landslip_insured: DataTypes.DECIMAL(18, 2),

        subsidence_and_landslip_retaining_wall: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for subsidence and landslip"
                }
            }
        },


        damage_to_external_radio_and_tv_aerials:{
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for damage to external radio and tv aerials"
                }
            }
        },

        damage_to_undergr_water_gas_pipes_or_electr_cables:{
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for damage to underground water, gas pipes or electricity cables"
                }
            }
        },
        workmen_compens_any_domestic_workers:{
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for workmens compensation for any domestic workers"
                }
            }
        },
        removal_debris:{
            type: DataTypes.ENUM('yes','no'),
            validate: {
                isIn: {
                    args: [
                        ['yes','no']
                    ],
                    msg: "Invalid status for removal of debris"
                }
            }
        },
        notes: {
            type: DataTypes.STRING(900),
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 900 }
                    ],
                    msg: "Notes can not be longer 900 characters"
                }
            }
        }
    }, {
            timestamps: true,
            indexes: [],
            freezeTableName: true,
            tableName: 'immovables',
            classMethods: {

            },
            instanceMethods: {
                
            }
        });

    return Immovable;

};