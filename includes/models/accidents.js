﻿module.exports = function (sequelize, DataTypes) {
    var Accident = sequelize.define('Accident', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        status: {
            type: DataTypes.ENUM('active', 'hidden'),
            defaultValue: 'active',
            validate: {
                isIn: {
                    args: [
                        ['active', 'hidden']
                    ],
                    msg: "Invalid claim status"
                }
            }
        },
        accidentdate: {
            type: DataTypes.DATE,
            validate: {
                isDate: {
                    msg: "Invalid accident date"
                }
            }
        },
        you_deemed_responsible: {
            type: DataTypes.ENUM('yes', 'no'),
            validate: {
                isIn: {
                    args: [
                        ['yes', 'no']
                    ],
                    msg: "Invalid status for question: Have you had an acccident in the last 5 years?"
                }
            }
        },
        accident_cost: DataTypes.DECIMAL(18, 2),
        accident_description: {
            type: DataTypes.STRING(900),
            validate: {
                isLength: {
                    args: [
                        { min: 0, max: 900 }
                    ],
                    msg: "Accident description can not be longer 900 characters"
                }
            }
        },
    }, {
            timestamps: true,
            indexes: [],
            freezeTableName: true,
            tableName: 'accidents',
            classMethods: {

            },
            instanceMethods: {
                
            }
        });

    return Accident;

};