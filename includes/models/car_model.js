﻿module.exports = function (sequelize, DataTypes) {
    var CarModelsList = sequelize.define('CarModelsList', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        
        car_make_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

        model_name: {
            type: DataTypes.STRING(255),
            validate: {
                isLength: {
                    args: [
                        { min: 1, max: 255 }
                    ],
                    msg: "Model should have from 1 to 255 characters"
                }
            },
            allowNull: false
        },
        
    }, {
            timestamps: true,
            indexes: [],
            freezeTableName: true,
            tableName: 'car_models',
            classMethods: {

            },
            instanceMethods: {
                
            }
        });

    return CarModelsList;

};