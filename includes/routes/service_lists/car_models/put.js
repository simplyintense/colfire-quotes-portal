var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/service-lists/car-makes/:make_id/car-models/:model_id';
exports.method = 'put';

exports.handler = function (req, res, next) {

    var model_name = api.getParam(req, 'model_name', undefined);
    var make_id = api.getParam(req, 'make_id', undefined);
    var model_id = api.getParam(req, 'model_id', undefined);

    api.requireSignedIn(req, function (user) {
        
        if ( user.is_admin !== 1 ) throw new Error('Check your permissions');

        db.CarMakesList.findOne({
            where: {
                id: make_id
            }
        }).then(function (make_instance) {
            if (!make_instance) throw new errors.HaveNoRightsError();

            return db.CarModelsList.findOne({
                where: {
                    id: model_id,
                    car_make_id: make_instance.id,
                }
            });
        }).then(function(model_instance) {
            if (!model_instance) throw new errors.HaveNoRightsError();

            if (model_name) model_instance.model_name = model_name;

            return model_instance.save();
        }).then(function(model) {
            /// loads it from db, to be sure:
            return db.CarModelsList.findOne({
                where: {
                    id: model.id,
                }
            });
		}).then(function(resModel) {
            res.send(resModel);
			next();
		});

    });
};