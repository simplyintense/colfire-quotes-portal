var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/service-lists/car-makes/:make_id/car-models/:model_id';
exports.method = 'del';

exports.handler = function (req, res, next) {

    var make_id = api.getParam(req, 'make_id', undefined);
    var model_id = api.getParam(req, 'model_id', undefined);

    api.requireSignedIn(req, function (user) {
        
        if ( user.is_admin !== 1 ) throw new Error('Check your permissions');

        db.CarMakesList.findOne({
            where: {
                id: make_id
            }
        }).then(function (make_instance) {
            if (!make_instance) throw new errors.NotFoundError();

            return db.CarModelsList.findOne({
                where: {
                    id: model_id
                }
            });
        }).then(function(model_instance) {
            if (!model_instance) throw new errors.NotFoundError();

            return model_instance.destroy();
        }).then(function () {
            res.send(true);
            next();
        });
    });
};