var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/service-lists/car-makes/:make_id/car-models';
exports.method = 'post';

exports.handler = function (req, res, next) {

    var model_name = api.getParam(req, 'model_name', 0);
    var make_id = api.getParam(req, 'make_id', 0);

    api.requireSignedIn(req, function (user) {
        
        if ( user.is_admin !== 1) throw new Error('Check your permissions');
        if (!model_name) throw new Error('Parameter model_name is required');

        db.CarMakesList.findOne({
            where: { id: make_id }
        }).then(function (make_instance) {
            if (!make_instance) throw new errors.HaveNoRightsError();

            return make_instance.createCarModelsList({
                model_name: model_name
            });

        }).then(function (resVehicle) {
            if (!resVehicle) throw new errors.NotFoundError();

            res.send(resVehicle);
            next();
        });

    });

};