var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/service-lists/car-makes/:make_id/car-models';
exports.method = 'get';

exports.handler = function (req, res, next) {

    var car_model = api.getParam(req, 'car_model', undefined);
    var car_make = api.getParam(req, 'car_make', undefined);
    var make_id = api.getParam(req, 'make_id', undefined);

    api.requireSignedIn(req, function (user) {
        // if (user.is_admin !== 1 && (car_make && car_model)) throw new Error('Check your permissions');
        var outerWhere = {};
        var innerWhere = {};

        if (car_make) {
            outerWhere.where = {
                id: make_id
            };
        } else {
            outerWhere.where = {
                $or: [
                    { id: make_id },
                    { make_name: make_id },
                ]
            };
        }
        
        db.CarMakesList.findOne({
            attributes: ['id', 'make_name'],
            where: outerWhere.where
        }).then(function (make_instance) {
            if (!make_instance) throw new errors.NotFoundError();
            
            if (car_model) {
                innerWhere.where = {
                    $and: [
                        { model_name: { $like: '%' + car_model + '%' } },
                        { car_make_id:  make_id }
                    ]
                };
            } else {
                innerWhere.where = { 
                    car_make_id: make_instance.id 
                };
            }

            return db.CarModelsList.findAll({
                where: innerWhere.where,
                order: [
                    ['model_name', 'ASC'],
                ],
            });
        }).then(function (car_models) {
            if (!car_models) throw new errors.NotFoundError();

            res.send(car_models);
            next();
        });

    });
    
};