var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/service-lists/car-makes';
exports.method = 'post';

exports.handler = function (req, res, next) {

    var make_name = api.getParam(req, 'make_name', 0);

    api.requireSignedIn(req, function (user) {
        if ( user.is_admin !== 1) throw new Error('Check your permissions');
        if (!make_name) throw new Error('Parameter make_name is required');
        
        db.CarMakesList.findOrCreate({
            where: { make_name: make_name }
        }).spread((make, created) => {
        //   console.log( make.get() );
        //   console.log(created);
          return make;
        }).then(function(make){
            res.send(make);
            next();
        });

    });

};