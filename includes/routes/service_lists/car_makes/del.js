var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/service-lists/car-makes/:make_id';
exports.method = 'del';

exports.handler = function (req, res, next) {

    var make_id = api.getParam(req, 'make_id', undefined);

    api.requireSignedIn(req, function (user) {
        if ( user.is_admin !== 1 ) throw new Error('Check your permissions');

        db.CarMakesList.findOne({
            where: {
                id: make_id
            }
        }).then(function (make_instance) {
            if (!make_instance) throw new errors.NotFoundError();
            
            return make_instance.destroy();
        }).then(function () {
            res.send(true);
            next();
        });
    });
};