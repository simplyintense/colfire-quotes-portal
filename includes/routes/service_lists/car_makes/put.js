var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/service-lists/car-makes/:make_id';
exports.method = 'put';

exports.handler = function (req, res, next) {

    var make_id = api.getParam(req, 'make_id', undefined);
    var make_name = api.getParam(req, 'make_name', undefined);

    api.requireSignedIn(req, function (user) {
        if ( user.is_admin !== 1 ) throw new Error('Check your permissions');

        db.CarMakesList.findOne({
            where: {
                id: make_id
            }
        }).then(function (make_instance) {
            if (!make_instance) throw new errors.HaveNoRightsError();

            if (make_name) make_instance.make_name = make_name;
 
            return make_instance.save();
        }).then(function(make) {
            /// loads it from db, to be sure:
            return db.CarMakesList.findOne({
                where: {
                    id: make.id,
                }
            });
		}).then(function(resMake) {
            res.send(resMake);
			next();
		});
    });
};