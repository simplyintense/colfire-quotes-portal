var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/service-lists/car-makes';
exports.method = 'get';

exports.handler = function (req, res, next) {

    var car_make = api.getParam(req, 'car_make', undefined);
    var car_model = api.getParam(req, 'car_model', undefined);

    var outerWhere = {};
    var includeCarModels = [];

    if (car_make) {
        outerWhere.where = { make_name: { $like: '%' + car_make + '%' } };
    } else {
        outerWhere.where = { make_name: { $ne: ''} };
    }

    if (car_model) {
        includeCarModels.include = [{
            model: db.CarModelsList,
            attributes: ['id', 'model_name'],
            required: true,
            where: {
                model_name: { $like: '%' + car_model + '%' }
            }
        }];
    }


    api.requireSignedIn(req, function (user) {
        if ( user.is_admin !== 1 && (car_make && car_model)) throw new Error('Check your permissions');
        
        db.CarMakesList.findAll({
            attributes: ['id', 'make_name'],
            where: outerWhere.where,
            include: includeCarModels.include
        }).then(function (make_list) {
            if (!make_list) throw new errors.NotFoundError();

            res.send(make_list);
            next();
        });
    });
};