var rfr = require('rfr');
var db = rfr('includes/models');
var ValidationError = rfr('includes/errors.js');
var api = rfr('includes/api.js');
var shortid = require('shortid');
shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');

exports.route = '/api/users/invite';
exports.method = 'post';

exports.handler = function(req, res, next) {
    
	var login = api.getParam(req, 'login', '');
	var type = api.getParam(req, 'type', 'default');
    var is_admin = 1;
    var is_demo = 0;
	var email = api.getParam(req, 'email', '');
    var password = shortid.generate();
    var ip = api.getVisitorIp(req);

    if(!login) throw new Error('No login provided');
    if(!email) throw new Error('No email provided');


    api.requireSignedIn(req, function (user) {
        if( user.is_admin !== 1) throw new Error('Check your permissions');

	    db.User.create({
		    login: login,
		    type: type,
		    password: password,
		    email: email,
            is_admin: is_admin,
            is_demo: is_demo,
		    ip: ip
	    }).then(function(newuser) {
            if(!newuser) throw new Error('Please check enetered values');

		    return db.User.findOne({
                where: { id: newuser.id },
                attributes: ['id','login','email'],
            });
	    }).then(function(resUser) {
		    if(!resUser) throw new errors.NotFoundError();

            //email to new user with a set new password link
            db.User.resetPassword(resUser.email,'invite');

		    res.send(resUser);
		    next();
	    }).catch(function(e) {
		    throw e;
	    });

    });
};