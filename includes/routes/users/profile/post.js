﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/users/:user_id/profile';
exports.method = 'post';


exports.handler = function(req, res, next) {
    
    var user_id = parseInt(req.params.user_id || 0, 10);

    var data = {
        status: api.getParam(req, 'status', 'active'),
        firstname: api.getParam(req, 'firstname', ''),
        lastname: api.getParam(req, 'lastname', ''),
        current_insurer: api.getParam(req, 'current_insurer', null),
        cur_cust_years: api.getParam(req, 'cur_cust_years', null),
        home_address: api.getParam(req, 'home_address', null),
        email: api.getParam(req, 'email', null),
        occupation: api.getParam(req, 'occupation', null),
        phone_number: api.getParam(req, 'phone_number', null),
        gender: api.getParam(req, 'gender', null),
        dateofbirth: api.getParam(req, 'dateofbirth', null),
        notes: api.getParam(req, 'notes', ''),
	};

    if (data.firstname == '')
        throw new Error('Please enter First Name');

    if (data.lastname == '')
        throw new Error('Please enter Last Name');
    
    
    api.requireSignedIn(req, function (user) {

        if(user.id !== user_id && user.is_admin == 0)
            throw new errors.HaveNoRightsError();

        db.User.findOne({
            where: {
                id: user_id
            }
        }).then(function (user) {
            if (!user) throw new errors.HaveNoRightsError();
            return db.Profile.findOne({
                where: {
                    user_id: user.id
                },
                include: { 
                    model: db.User,
                    attributes: ['email'], 
                }
            });
        }).then(function (profile) {
            if (!profile){
                return user.createProfile(data);
            }else{
                throw new Error('This user already has a profile. Use PUT.');
            }
        }).then(function (resProfile) {
            if (!resProfile) throw new errors.NotFoundError();
            res.send(resProfile);
            next();
        });


	});
    
};