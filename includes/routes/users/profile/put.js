﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/users/:user_id/profile';
exports.method = 'put';


exports.handler = function(req, res, next) {

    var user_id = parseInt(req.params.user_id || 0, 10);

    var data = {
        status: api.getParam(req, 'status', null),
        firstname: api.getParam(req, 'firstname', null),
        lastname: api.getParam(req, 'lastname', null),
        current_insurer: api.getParam(req, 'current_insurer', null),
        cur_cust_years: api.getParam(req, 'cur_cust_years', null),
        home_address: api.getParam(req, 'home_address', null),
        email: api.getParam(req, 'email', null),
        occupation: api.getParam(req, 'occupation', null),
        phone_number: api.getParam(req, 'phone_number', null),
        gender: api.getParam(req, 'gender', null),
        dateofbirth: api.getParam(req, 'dateofbirth', null),
        notes: api.getParam(req, 'notes', null),
	};

    
    api.requireSignedIn(req, function (user) {

        if(user.id !== user_id && user.is_admin == 0)
            throw new errors.HaveNoRightsError();

        db.User.findOne({
            where: {
                id: user_id
            }
        }).then(function (user) {
            if (!user) throw new errors.HaveNoRightsError();
            return db.Profile.findOne({
                where: {
                    user_id: user.id
                },
                include: { 
                    model: db.User,
                    attributes: ['email'], 
                }
            });
        }).then(function (profile) {
            if (!profile)
               throw new Error('This user has no profile yet. Use POST.');
            
            if (data.status !== null) profile.status = data.status;
            if (data.firstname !== null) profile.firstname = data.firstname || 'Undefined';
            if (data.lastname !== null) profile.lastname = data.lastname || 'Undefined';
            if (data.current_insurer !== null) profile.current_insurer = data.current_insurer || 'other';
            if (data.cur_cust_years !== null) profile.cur_cust_years = data.cur_cust_years || 'Undefined';
            if (data.home_address !== null) profile.home_address = data.home_address;
            if (data.email !== null) profile.email = data.email;
            if (data.occupation !== null) profile.occupation = data.occupation;
            if (data.phone_number !== null) profile.phone_number = data.phone_number;
            if (data.gender !== null) profile.gender = data.gender;
            if (data.dateofbirth !== null) profile.dateofbirth = data.dateofbirth;
            if (data.notes !== null) profile.notes = data.notes;
            
            return profile.save();
            
        }).then(function (resProfile) {
            if (!resProfile) throw new errors.NotFoundError();
            res.send(resProfile);
            next();
        });


	});
    
};