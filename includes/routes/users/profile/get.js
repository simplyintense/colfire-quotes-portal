var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/users/:user_id/profile';
exports.method = 'get';


exports.handler = function (req, res, next) {

    var user_id = parseInt(req.params.user_id || 0, 10);

    api.requireSignedIn(req, function (user) {

        if(user.id !== user_id && user.is_admin == 0)
            throw new errors.HaveNoRightsError();

        db.User.findOne({
            where: {
                id: user_id
            }
        }).then(function (user) {
            if (!user) throw new errors.HaveNoRightsError();
            return db.Profile.findOne({
                where: {
                    user_id: user.id
                },
                //include: { 
                //    model: db.User,
                //    attributes: ['email'], 
                //}
            });
        }).then(function (profile) {
            if (!profile) throw new errors.NotFoundError();
            res.send(profile);
            next();
        });
    });
};