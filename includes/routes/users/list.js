﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/userprofiles';
exports.method = 'get';


exports.handler = function (req, res, next) {
    
    var data = {
        email: api.getParam(req, 'email', ''),
        type: api.getParam(req, 'type', ''),
        login: api.getParam(req, 'login', ''),
        is_demo: api.getParam(req, 'is_demo', ''),
        is_admin: api.getParam(req, 'is_admin', ''),
    };
    
    for (var propName in data) {
        if (data[propName] === '') {
            delete data[propName];
        } else {
            if(data[propName] !== 'id') 
                data[propName] = { $like : '%' + data[propName] + '%' };
        }
    }


    api.requireSignedIn(req, function (user) {
        console.log(user.is_admin);
        if( user.is_admin == 0)
            data.id = user.id;

        db.User.findAll({
            where: data
        }).then(function(users){
            
            res.send(users);
            next();

        });
        
    });
};