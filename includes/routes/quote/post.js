﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');
var shortid = require('shortid');

shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');

exports.route = '/api/quotes';
exports.method = 'post';


exports.handler = function(req, res, next) {

    var data = {
        status: api.getParam(req, 'status', 'active'),
        insurance_type: api.getParam(req, 'insurance_type', ''),
        notes: api.getParam(req, 'notes', ''),
        rating: api.getParam(req, 'rating', ''),
        ratingFeedback: api.getParam(req, 'ratingFeedback', ''),
	};

    if (data.insurance_type == '')
        throw new Error('Please enter insurance type');

    //var prepend = '';
    //if (data.insurance_type == 'property')
    //    prepend = 'WQ-PROP-';

    //if (data.insurance_type == 'motor')
    //    prepend = 'WQ-MOT-';

    //data.ref_num = prepend + shortid.generate();
    
 
    api.requireSignedIn(req, function (user) {

        user.createQuote(data)
        .then(function (quote) {
            if (!quote) throw new errors.NotFoundError();

            var prepend = '';
            if (data.insurance_type == 'property')
                prepend = 'WQ-PROP-';

            if (data.insurance_type == 'motor')
                prepend = 'WQ-MOT-';

            var size = 3;
            var id_formatted = quote.id + "";
            while (id_formatted.length < size) id_formatted = "0" + id_formatted;
            
            quote.ref_num = prepend + id_formatted;

            return quote.save();

        }).then(function (resQuote) {
            if (!resQuote) throw new errors.NotFoundError();
            res.send(resQuote);
            next();
        });

	});
    
};