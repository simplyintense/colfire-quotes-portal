﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/quotes/:quote_id/immovables/:immovable_id/claims';
exports.method = 'get';

exports.handler = function (req, res, next) {
    var quote_id = parseInt(req.params.quote_id || 0, 10);
    var immovable_id = parseInt(req.params.immovable_id || 0, 10);

    var data = {
        immovable_id: immovable_id,
        status: api.getParam(req, 'status', 'active'), //we want to get only active claims |  all the other - means deleted
        claim_date: api.getParam(req, 'claim_date', null),
        estimated_value: api.getParam(req, 'estimated_value', null),
        claims_history_descr: api.getParam(req, 'claims_history_descr', null),
    };

    for (var propName in data) {
        if (data[propName] === null) {
            delete data[propName];
        } else {
            data[propName] = { $like: '%' + data[propName] + '%' };
        }
    }

    api.requireSignedIn(req, function (user) {
        
        db.Quote.findOne({
            where: {
                id: quote_id
            }
        }).then(function (quote) {
            if (!quote) throw new errors.HaveNoRightsError();

            if(quote.user_id !== user.id && user.is_admin == 0)
                throw new errors.HaveNoRightsError();

            return db.Immovable.findOne({
                where:{
                    id: immovable_id,
                    quote_id: quote.id 
                }
            });
        }).then(function (immovable) {
            if (!immovable) throw new errors.NotFoundError();

            return db.Claim.findAll({
                where: data
            });

        }).then(function (resClaims) {
            if (!resClaims) throw new errors.NotFoundError();
            res.send(resClaims);
            next();
        });

    });

};