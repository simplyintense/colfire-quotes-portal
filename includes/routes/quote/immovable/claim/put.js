﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/quotes/:quote_id/immovables/:immovable_id/claims/:claim_id';
exports.method = 'put';


exports.handler = function(req, res, next) {
    
    var quote_id = parseInt(req.params.quote_id || 0, 10);
    var immovable_id = parseInt(req.params.immovable_id || 0, 10);
    var claim_id = parseInt(req.params.claim_id || 0, 10);

    var data = {
        status: api.getParam(req, 'status', null),
        claim_date: api.getParam(req, 'claim_date', null),
        estimated_value: api.getParam(req, 'estimated_value', null),
        claims_history_descr: api.getParam(req, 'claims_history_descr', null),
	};

    
    api.requireSignedIn(req, function (user) {

        db.Quote.findOne({
            where: {
                id: quote_id
            }
        }).then(function(quote){
            if(!quote) throw new errors.NotFoundError();

            if(user.id !== quote.user_id && user.is_admin == 0)
                throw new errors.HaveNoRightsError();

            return db.Immovable.findOne({
                where: {
                    id: immovable_id,
                    quote_id: quote.id
                }
            });
        }).then(function(immovable){
            if (!immovable) throw new errors.NotFoundError();

            return db.Claim.findOne({
                where: {
                    id: claim_id,
                    immovable_id: immovable.id
                }
            });
        }).then(function(claim){
            if (!claim) throw new errors.NotFoundError();

            if (data.status !== null) claim.status = data.status;
            if (data.claim_date !== null) claim.claim_date = data.claim_date;
            if (data.estimated_value !== null) claim.estimated_value = data.estimated_value;
            if (data.claims_history_descr !== null) claim.claims_history_descr = data.claims_history_descr;
            
            return claim.save();

        }).then(function(resClaim){
            res.send(resClaim);
            next();
        });


	});
    
};