﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/quotes/:quote_id/immovables/:immovable_id';
exports.method = 'put';


exports.handler = function(req, res, next) {
    
    var quote_id = parseInt(req.params.quote_id || 0, 10);
    var immovable_id = parseInt(req.params.immovable_id || 0, 10);

    var data = {
        status: api.getParam(req, 'status', 'active'),
        insurance_type: api.getParam(req, 'insurance_type', null),
        building_value: api.getParam(req, 'building_value', null),
        contents_value: api.getParam(req, 'contents_value', null),
        usage_type: api.getParam(req, 'usage_type', null),
        address_line_1: api.getParam(req, 'address_line_1', null),
        address_line_2: api.getParam(req, 'address_line_2', null),
        address_line_3: api.getParam(req, 'address_line_3', null),
        city: api.getParam(req, 'city', null),
        on_hill: api.getParam(req, 'on_hill', null),
        seafront_waterway: api.getParam(req, 'seafront_waterway', null),
        wall_type: api.getParam(req, 'wall_type', null),
        wall_type_other: api.getParam(req, 'wall_type_other', null),
        roof_type: api.getParam(req, 'roof_type', null),
        roof_type_other: api.getParam(req, 'roof_type_other', null),
        floor_type: api.getParam(req, 'floor_type', null),
        floor_type_other: api.getParam(req, 'floor_type_other', null),
        proximity_to_nearest_building: api.getParam(req, 'proximity_to_nearest_building', null),
        building_age: api.getParam(req, 'building_age', null),
        fire_extinguishers: api.getParam(req, 'fire_extinguishers', null),
        hose_reels: api.getParam(req, 'hose_reels', null),
        sprinkler_system: api.getParam(req, 'sprinkler_system', null),
        smoke_detectors: api.getParam(req, 'smoke_detectors', null),
        alarms: api.getParam(req, 'alarms', null),
        security_personnel: api.getParam(req, 'security_personnel', null),
        deadbolts_padlocks: api.getParam(req, 'deadbolts_padlocks', null),
        gated_community: api.getParam(req, 'gated_community', null),
        gated_property: api.getParam(req, 'gated_property', null),
        burglar_proofing: api.getParam(req, 'burglar_proofing', null),
        claims_history: api.getParam(req, 'claims_history', null),
        flood: api.getParam(req, 'flood', null),
        subsidence_and_landslip: api.getParam(req, 'subsidence_and_landslip', null),
        subsidence_and_landslip_retaining_wall: api.getParam(req, 'subsidence_and_landslip_retaining_wall', null),
        subsidence_and_landslip_insured: api.getParam(req, 'subsidence_and_landslip_insured', null),
        damage_to_external_radio_and_tv_aerials: api.getParam(req, 'damage_to_external_radio_and_tv_aerials', null),
        damage_to_undergr_water_gas_pipes_or_electr_cables: api.getParam(req, 'damage_to_undergr_water_gas_pipes_or_electr_cables', null),
        workmen_compens_any_domestic_workers: api.getParam(req, 'workmen_compens_any_domestic_workers', null),
        removal_debris: api.getParam(req, 'removal_debris', null),
        notes: api.getParam(req, 'notes', null),
	};

    
    api.requireSignedIn(req, function (user) {

        db.Quote.findOne({
            where: {
                id: quote_id
            }
        }).then(function(quote){
            if(!quote) throw new errors.NotFoundError();

            if(user.id !== quote.user_id && user.is_admin == 0)
                throw new errors.HaveNoRightsError();

            return db.Immovable.findOne({
                where: {
                    id: immovable_id,
                    quote_id: quote.id
                }
            });
        }).then(function(immovable){
            if (!immovable) throw new errors.NotFoundError();

            if (data.status !== null) immovable.status = data.status;
            if (data.insurance_type !== null) immovable.insurance_type = data.insurance_type;
            if (data.building_value !== null) immovable.building_value = data.building_value;
            if (data.contents_value !== null) immovable.contents_value = data.contents_value;
            if (data.usage_type !== null) immovable.usage_type = data.usage_type;
            if (data.address_line_1 !== null) immovable.address_line_1 = data.address_line_1;
            if (data.address_line_2 !== null) immovable.address_line_2 = data.address_line_2;
            if (data.address_line_3 !== null) immovable.address_line_3 = data.address_line_3;
            if (data.city !== null) immovable.city = data.city;
            if (data.on_hill !== null) immovable.on_hill = data.on_hill;
            if (data.seafront_waterway !== null) immovable.seafront_waterway = data.seafront_waterway;
            if (data.wall_type !== null) immovable.wall_type = data.wall_type;
            if (data.wall_type_other !== null) immovable.wall_type_other = data.wall_type_other;
            if (data.roof_type !== null) immovable.roof_type = data.roof_type;
            if (data.roof_type_other !== null) immovable.roof_type_other = data.roof_type_other;
            if (data.floor_type !== null) immovable.floor_type = data.floor_type;
            if (data.floor_type_other !== null) immovable.floor_type_other = data.floor_type_other;
            if (data.proximity_to_nearest_building !== null) immovable.proximity_to_nearest_building = data.proximity_to_nearest_building;
            if (data.building_age !== null) immovable.building_age = data.building_age;
            if (data.fire_extinguishers !== null) immovable.fire_extinguishers = data.fire_extinguishers;
            if (data.hose_reels !== null) immovable.hose_reels = data.hose_reels;
            if (data.sprinkler_system !== null) immovable.sprinkler_system = data.sprinkler_system;
            if (data.smoke_detectors !== null) immovable.smoke_detectors = data.smoke_detectors;
            if (data.alarms !== null) immovable.alarms = data.alarms;
            if (data.security_personnel !== null) immovable.security_personnel = data.security_personnel;
            if (data.deadbolts_padlocks !== null) immovable.deadbolts_padlocks = data.deadbolts_padlocks;
            if (data.gated_community !== null) immovable.gated_community = data.gated_community;
            if (data.gated_property !== null) immovable.gated_property = data.gated_property;
            if (data.burglar_proofing !== null) immovable.burglar_proofing = data.burglar_proofing;
            if (data.claims_history !== null) immovable.claims_history = data.claims_history;
            if (data.flood !== null) immovable.flood = data.flood;
            if (data.subsidence_and_landslip !== null) immovable.subsidence_and_landslip = data.subsidence_and_landslip;
            if (data.subsidence_and_landslip_retaining_wall !== null) immovable.subsidence_and_landslip_retaining_wall = data.subsidence_and_landslip_retaining_wall;
            if (data.subsidence_and_landslip_insured !== null) immovable.subsidence_and_landslip_insured = data.subsidence_and_landslip_insured;
            if (data.damage_to_external_radio_and_tv_aerials !== null) immovable.damage_to_external_radio_and_tv_aerials = data.damage_to_external_radio_and_tv_aerials;
            if (data.damage_to_undergr_water_gas_pipes_or_electr_cables !== null) immovable.damage_to_undergr_water_gas_pipes_or_electr_cables = data.damage_to_undergr_water_gas_pipes_or_electr_cables;
            if (data.workmen_compens_any_domestic_workers !== null) immovable.workmen_compens_any_domestic_workers = data.workmen_compens_any_domestic_workers;
            if (data.removal_debris !== null) immovable.removal_debris = data.removal_debris;
            if (data.notes !== null) immovable.notes = data.notes;
            
            return immovable.save();

        }).then(function(resImmovable){
            res.send(resImmovable);
            next();
        });


	});
    
};