﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/quotes/:quote_id/immovables/:immovable_id';
exports.method = 'get';


exports.handler = function (req, res, next) {

    var quote_id = parseInt(req.params.quote_id || 0, 10);
    var immovable_id = parseInt(req.params.immovable_id || 0, 10);

    api.requireSignedIn(req, function (user) {

        db.Quote.findOne({
            where: {
                id: quote_id
            }
        }).then(function(quote){
            if(!quote) throw new errors.NotFoundError();

            if(user.id !== quote.user_id && user.is_admin == 0)
                throw new errors.HaveNoRightsError();

            return db.Immovable.findOne({
                where: {
                    id: immovable_id,
                    quote_id: quote.id
                }
            });

        }).then(function(immovable){
            if(!immovable) throw new errors.NotFoundError();
            res.send(immovable);
            next();
        });
        
    });
};