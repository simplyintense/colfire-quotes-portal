﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/quotes/:quote_id/immovables';
exports.method = 'post';


exports.handler = function(req, res, next) {
    
    var quote_id = parseInt(req.params.quote_id || 0, 10);

    var data = {
        status: api.getParam(req, 'status', 'active'),
        insurance_type: api.getParam(req, 'insurance_type', null),
        building_value: api.getParam(req, 'building_value', null),
        contents_value: api.getParam(req, 'contents_value', null),
        usage_type: api.getParam(req, 'usage_type', null),
        address_line_1: api.getParam(req, 'address_line_1', null),
        address_line_2: api.getParam(req, 'address_line_2', null),
        address_line_3: api.getParam(req, 'address_line_3', null),
        city: api.getParam(req, 'city', null),
        on_hill: api.getParam(req, 'on_hill', null),
        seafront_waterway: api.getParam(req, 'seafront_waterway', null),
        wall_type: api.getParam(req, 'wall_type', null),
        wall_type_other: api.getParam(req, 'wall_type_other', null),
        roof_type: api.getParam(req, 'roof_type', null),
        roof_type_other: api.getParam(req, 'roof_type_other', null),
        floor_type: api.getParam(req, 'floor_type', null),
        floor_type_other: api.getParam(req, 'floor_type_other', null),
        proximity_to_nearest_building: api.getParam(req, 'proximity_to_nearest_building', null),
        building_age: api.getParam(req, 'building_age', null),
        fire_extinguishers: api.getParam(req, 'fire_extinguishers', null),
        hose_reels: api.getParam(req, 'hose_reels', null),
        sprinkler_system: api.getParam(req, 'sprinkler_system', null),
        smoke_detectors: api.getParam(req, 'smoke_detectors', null),
        alarms: api.getParam(req, 'alarms', null),
        security_personnel: api.getParam(req, 'security_personnel', null),
        deadbolts_padlocks: api.getParam(req, 'deadbolts_padlocks', null),
        gated_community: api.getParam(req, 'gated_community', null),
        gated_property: api.getParam(req, 'gated_property', null),
        burglar_proofing: api.getParam(req, 'burglar_proofing', null),
        claims_history: api.getParam(req, 'claims_history', null),
        flood: api.getParam(req, 'flood', null),
        subsidence_and_landslip: api.getParam(req, 'subsidence_and_landslip', null),
        subsidence_and_landslip_retaining_wall: api.getParam(req, 'subsidence_and_landslip_retaining_wall', null),
        subsidence_and_landslip_insured: api.getParam(req, 'subsidence_and_landslip_insured', null),
        damage_to_external_radio_and_tv_aerials: api.getParam(req, 'damage_to_external_radio_and_tv_aerials', null),
        damage_to_undergr_water_gas_pipes_or_electr_cables: api.getParam(req, 'damage_to_undergr_water_gas_pipes_or_electr_cables', null),
        workmen_compens_any_domestic_workers: api.getParam(req, 'workmen_compens_any_domestic_workers', null),
        removal_debris: api.getParam(req, 'removal_debris', null),
        notes: api.getParam(req, 'notes', null),
	};

    
    api.requireSignedIn(req, function (user) {

        db.Quote.findOne({
            where: {
                id: quote_id
            }
        }).then(function (quote) {
            if (!quote) throw new errors.HaveNoRightsError();

            if(quote.user_id !== user.id && user.is_admin == 0)
                throw new errors.HaveNoRightsError();

            return quote.createImmovable(data);

        }).then(function (resImmovable) {
            if (!resImmovable) throw new errors.NotFoundError();
            res.send(resImmovable);
            next();
        });


	});
    
};