﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/quotes/:quote_id/vehicles';
exports.method = 'get';


exports.handler = function (req, res, next) {

    var quote_id = parseInt(req.params.quote_id || 0, 10);
    
    var data = {
        status: api.getParam(req, 'status', ''),
    };
    
    for (var propName in data) {
        if (data[propName] === '') {
            delete data[propName];
        } else {
            if(data[propName] !== 'quote_id') 
                data[propName] = { $like : '%' + data[propName] + '%' };
        }
    }

    api.requireSignedIn(req, function (user) {

        db.Quote.findOne({
            where: {
                id: quote_id
            }
        }).then(function(quote){
            if(!quote) throw new errors.NotFoundError();

            if(user.id !== quote.user_id && user.is_admin == 0)
                throw new errors.HaveNoRightsError();

            return quote.getVehicles({
                where: data,
            });

        }).then(function(vehicles){
            if(!vehicles) throw new errors.NotFoundError();
            res.send(vehicles);
            next();
        });
        
    });
};