﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/quotes/:quote_id/vehicles/:vehicle_id';
exports.method = 'put';


exports.handler = function(req, res, next) {
    
    var quote_id = parseInt(req.params.quote_id || 0, 10);
    var vehicle_id = parseInt(req.params.vehicle_id || 0, 10);

    var data = {
        status: api.getParam(req, 'status', 'active'),
        //young_drive_rage: api.getParam(req, 'young_driver_age', null),
        //new_driver_permit: api.getParam(req, 'new_driver_permit', null),
        insurance_type_comprehensive: api.getParam(req, 'insurance_type_comprehensive', null),
        insurance_type_third_party_fire_theft: api.getParam(req, 'insurance_type_third_party_fire_theft', null),
        insurance_type_third_party: api.getParam(req, 'insurance_type_third_party', null),
        current_value: api.getParam(req, 'current_value', null),
        vehicle_registration_number: api.getParam(req, 'vehicle_registration_number', null),
        vehicle_protected: api.getParam(req, 'vehicle_protected', null),
        make: api.getParam(req, 'make', null),
        model: api.getParam(req, 'model', null),
        year: api.getParam(req, 'year', null),
        vehicle_origin: api.getParam(req, 'vehicle_origin', null),
        custom_descr_make_model: api.getParam(req, 'custom_descr_make_model', null),
        vehicle_age: api.getParam(req, 'vehicle_age', null),
        usage_type: api.getParam(req, 'usage_type', null),
        accidents_recently: api.getParam(req, 'accidents_recently', null),
        //accidentdate: api.getParam(req, 'accidentdate', null),
        //you_deemed_responsible: api.getParam(req, 'you_deemed_responsible', null),
        //accident_cost: api.getParam(req, 'accident_cost', null),
        //accident_description: api.getParam(req, 'accident_description', null),
        windscreen_coverage: api.getParam(req, 'windscreen_coverage', null),
        windscreen_coverage_amount: api.getParam(req, 'windscreen_coverage_amount', null),
        emergency_roadside_assist: api.getParam(req, 'emergency_roadside_assist', null),
        windscreen_coverage_3rd_party: api.getParam(req, 'windscreen_coverage_3rd_party', null),
        windscreen_coverage_amount_3rd_party: api.getParam(req, 'windscreen_coverage_amount_3rd_party', null),
        emergency_roadside_assist_3rd_party: api.getParam(req, 'emergency_roadside_assist_3rd_party', null),
        windscreen_coverage_3rd_party_fire: api.getParam(req, 'windscreen_coverage_3rd_party_fire', null),
        windscreen_coverage_amount_3rd_party_fire: api.getParam(req, 'windscreen_coverage_amount_3rd_party_fire', null),
        emergency_roadside_assist_3rd_party_fire: api.getParam(req, 'emergency_roadside_assist_3rd_party_fire', null),
        flood_special_perils: api.getParam(req, 'flood_special_perils', null),
        partial_waiver_of_excess: api.getParam(req, 'partial_waiver_of_excess', null),
        loss_of_use: api.getParam(req, 'loss_of_use', null),
        new_for_old: api.getParam(req, 'new_for_old', null),
        no_claim_discount_transfer: api.getParam(req, 'no_claim_discount_transfer', null),
        no_claim_discount_years: api.getParam(req, 'no_claim_discount_years', null),
        member_of_credit_union: api.getParam(req, 'member_of_credit_union', null),
        live_close_to_work: api.getParam(req, 'live_close_to_work', null),
        defensive_driver_cert: api.getParam(req, 'defensive_driver_cert', null),
        defensive_driver_school: api.getParam(req, 'defensive_driver_school', null),
        loan: api.getParam(req, 'loan', null),
        bank_name: api.getParam(req, 'bank_name', null),
        custom_loan_bank: api.getParam(req, 'custom_loan_bank', null),
        credit_card: api.getParam(req, 'credit_card', null),
        credit_card_bank: api.getParam(req, 'credit_card_bank', null),
        credit_card_bank_custom: api.getParam(req, 'credit_card_bank_custom', null),
        sole_driver: api.getParam(req, 'sole_driver', null),
        notes: api.getParam(req, 'notes', null),
        have_child: api.getParam(req, 'have_child', null),
	};

    
    api.requireSignedIn(req, function (user) {

        db.Quote.findOne({
            where: {
                id: quote_id
            }
        }).then(function(quote){
            if(!quote) throw new errors.NotFoundError();

            if(user.id !== quote.user_id && user.is_admin == 0)
                throw new errors.HaveNoRightsError();

            return db.Vehicle.findOne({
                where: {
                    id: vehicle_id,
                    quote_id: quote.id
                }
            });
        }).then(function(vehicle){
            if (!vehicle) throw new errors.NotFoundError();

            if (data.status !== null) vehicle.status = data.status;
            //if (data.young_driver_age !== null) vehicle.young_driver_age = data.young_driver_age;
            //if (data.new_driver_permit !== null) vehicle.new_driver_permit = data.new_driver_permit;
            if (data.insurance_type_comprehensive !== null) vehicle.insurance_type_comprehensive = data.insurance_type_comprehensive;
            if (data.insurance_type_third_party_fire_theft !== null) vehicle.insurance_type_third_party_fire_theft = data.insurance_type_third_party_fire_theft;
            if (data.insurance_type_third_party !== null) vehicle.insurance_type_third_party = data.insurance_type_third_party;
            if (data.current_value !== null) vehicle.current_value = data.current_value;
            if (data.vehicle_registration_number !== null) vehicle.vehicle_registration_number = data.vehicle_registration_number;
            if (data.vehicle_protected !== null) vehicle.vehicle_protected = data.vehicle_protected;
            if (data.make !== null) vehicle.make = data.make;
            if (data.model !== null) vehicle.model = data.model;
            if (data.year !== null) vehicle.year = data.year;
            if (data.vehicle_origin !== null) vehicle.vehicle_origin = data.vehicle_origin;
            if (data.custom_descr_make_model !== null) vehicle.custom_descr_make_model = data.custom_descr_make_model;
            if (data.vehicle_age !== null) vehicle.vehicle_age = data.vehicle_age;
            if (data.usage_type !== null) vehicle.usage_type = data.usage_type;
            if (data.accidents_recently !== null) vehicle.accidents_recently = data.accidents_recently;
            //if (data.accidentdate !== null) vehicle.accidentdate = data.accidentdate;
            //if (data.you_deemed_responsible !== null) vehicle.you_deemed_responsible = data.you_deemed_responsible;
            //if (data.accident_cost !== null) vehicle.accident_cost = data.accident_cost;
            //if (data.accident_description !== null) vehicle.accident_description = data.accident_description;
            if (data.windscreen_coverage !== null) vehicle.windscreen_coverage = data.windscreen_coverage;
            if (data.windscreen_coverage_amount !== null) vehicle.windscreen_coverage_amount = data.windscreen_coverage_amount;
            if (data.emergency_roadside_assist !== null) vehicle.emergency_roadside_assist = data.emergency_roadside_assist;
            if (data.windscreen_coverage_3rd_party !== null) vehicle.windscreen_coverage_3rd_party = data.windscreen_coverage_3rd_party;
            if (data.windscreen_coverage_amount_3rd_party !== null) vehicle.windscreen_coverage_amount_3rd_party = data.windscreen_coverage_amount_3rd_party;
            if (data.emergency_roadside_assist_3rd_party !== null) vehicle.emergency_roadside_assist_3rd_party = data.emergency_roadside_assist_3rd_party;
            if (data.windscreen_coverage_3rd_party_fire !== null) vehicle.windscreen_coverage_3rd_party_fire = data.windscreen_coverage_3rd_party_fire;
            if (data.windscreen_coverage_amount_3rd_party_fire !== null) vehicle.windscreen_coverage_amount_3rd_party_fire = data.windscreen_coverage_amount_3rd_party_fire;
            if (data.emergency_roadside_assist_3rd_party_fire !== null) vehicle.emergency_roadside_assist_3rd_party_fire = data.emergency_roadside_assist_3rd_party_fire;
            if (data.flood_special_perils !== null) vehicle.flood_special_perils = data.flood_special_perils;
            if (data.partial_waiver_of_excess !== null) vehicle.partial_waiver_of_excess = data.partial_waiver_of_excess;
            if (data.loss_of_use !== null) vehicle.loss_of_use = data.loss_of_use;
            if (data.new_for_old !== null) vehicle.new_for_old = data.new_for_old;
            if (data.no_claim_discount_transfer !== null) vehicle.no_claim_discount_transfer = data.no_claim_discount_transfer;
            if (data.no_claim_discount_years !== null) vehicle.no_claim_discount_years = data.no_claim_discount_years;
            if (data.member_of_credit_union !== null) vehicle.member_of_credit_union = data.member_of_credit_union;
            if (data.live_close_to_work !== null) vehicle.live_close_to_work = data.live_close_to_work;
            if (data.defensive_driver_cert !== null) vehicle.defensive_driver_cert = data.defensive_driver_cert;
            if (data.defensive_driver_school !== null) vehicle.defensive_driver_school = data.defensive_driver_school;
            if (data.loan !== null) vehicle.loan = data.loan;
            if (data.bank_name !== null) vehicle.bank_name = data.bank_name;
            if (data.custom_loan_bank !== null) vehicle.custom_loan_bank = data.custom_loan_bank;
            if (data.credit_card !== null) vehicle.credit_card = data.credit_card;
            if (data.credit_card_bank !== null) vehicle.credit_card_bank = data.credit_card_bank;
            if (data.credit_card_bank_custom !== null) vehicle.credit_card_bank_custom = data.credit_card_bank_custom;
            if (data.sole_driver !== null) vehicle.sole_driver = data.sole_driver;
            if (data.notes !== null) vehicle.notes = data.notes;
            if (data.have_child !== null) vehicle.have_child = data.have_child;
            
            return vehicle.save();

        }).then(function(resVehicle){
            res.send(resVehicle);
            next();
        });


	});
    
};