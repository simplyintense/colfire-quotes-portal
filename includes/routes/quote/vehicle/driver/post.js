﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/quotes/:quote_id/vehicles/:vehicle_id/drivers';
exports.method = 'post';


exports.handler = function(req, res, next) {
    
    var quote_id = parseInt(req.params.quote_id || 0, 10);
    var vehicle_id = parseInt(req.params.vehicle_id || 0, 10);

    var data = {
        status: api.getParam(req, 'status', 'active'),
        firstname: api.getParam(req, 'firstname', null),
        lastname: api.getParam(req, 'lastname', null),
        young_driver_age: api.getParam(req, 'young_driver_age', null),
        driving_experience: api.getParam(req, 'driving_experience', null),
        new_driver_permit: api.getParam(req, 'new_driver_permit', null),
        notes: api.getParam(req, 'notes', null),
	};

    if (data.firstname == '')
        throw new Error('Please enter First Name');

    if (data.lastname == '')
        throw new Error('Please enter Last Name');

    api.requireSignedIn(req, function (user) {

        db.Quote.findOne({
            where: {
                id: quote_id
            }
        }).then(function (quote) {
            if (!quote) throw new errors.HaveNoRightsError();

            if(quote.user_id !== user.id && user.is_admin == 0)
                throw new errors.HaveNoRightsError();

            return db.Vehicle.findOne({
                where:{
                    id: vehicle_id,
                    quote_id: quote.id 
                }
            });
        }).then(function (vehicle) {
            if (!vehicle) throw new errors.NotFoundError();

            return vehicle.createDriver(data);

        }).then(function (resDriver) {
            if (!resDriver) throw new errors.NotFoundError();
            res.send(resDriver);
            next();
        });


	});
    
};