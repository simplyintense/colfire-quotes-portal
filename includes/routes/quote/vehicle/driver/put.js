﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/quotes/:quote_id/vehicles/:vehicle_id/drivers/:driver_id';
exports.method = 'put';


exports.handler = function(req, res, next) {
    
    var quote_id = parseInt(req.params.quote_id || 0, 10);
    var vehicle_id = parseInt(req.params.vehicle_id || 0, 10);
    var driver_id = parseInt(req.params.driver_id || 0, 10);

    var data = {
        status: api.getParam(req, 'status', null),
        firstname: api.getParam(req, 'firstname', null),
        lastname: api.getParam(req, 'lastname', null),
        young_driver_age: api.getParam(req, 'young_driver_age', null),
        driving_experience: api.getParam(req, 'driving_experience', null),
        new_driver_permit: api.getParam(req, 'new_driver_permit', null),
        notes: api.getParam(req, 'notes', null),
	};

    
    api.requireSignedIn(req, function (user) {

        db.Quote.findOne({
            where: {
                id: quote_id
            }
        }).then(function(quote){
            if(!quote) throw new errors.NotFoundError();

            if(user.id !== quote.user_id && user.is_admin == 0)
                throw new errors.HaveNoRightsError();

            return db.Vehicle.findOne({
                where: {
                    id: vehicle_id,
                    quote_id: quote.id
                }
            });
        }).then(function(vehicle){
            if (!vehicle) throw new errors.NotFoundError();

            return db.Driver.findOne({
                where: {
                    id: driver_id,
                    vehicle_id: vehicle.id
                }
            });
        }).then(function(driver){
            if (!driver) throw new errors.NotFoundError();

            if (data.status !== null) driver.status = data.status;
            if (data.firstname !== null) driver.firstname = data.firstname;
            if (data.lastname !== null) driver.lastname = data.lastname;
            if (data.young_driver_age !== null) driver.young_driver_age = data.young_driver_age;
            if (data.driving_experience !== null) driver.driving_experience = data.driving_experience;
            if (data.new_driver_permit !== null) driver.new_driver_permit = data.new_driver_permit;
            if (data.notes !== null) driver.notes = data.notes;
            
            return driver.save();

        }).then(function(resDriver){
            res.send(resDriver);
            next();
        });


	});
    
};