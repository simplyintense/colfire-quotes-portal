﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/quotes/:quote_id/vehicles/:vehicle_id/drivers';
exports.method = 'get';

exports.handler = function (req, res, next) {
    var quote_id = parseInt(req.params.quote_id || 0, 10);
    var vehicle_id = parseInt(req.params.vehicle_id || 0, 10);

    var data = {
        vehicle_id: vehicle_id,
        status: api.getParam(req, 'status', 'active'), //we want to get only active drivers |  all the other - means deleted
        firstname: api.getParam(req, 'firstname', null),
        lastname: api.getParam(req, 'lastname', null),
        notes: api.getParam(req, 'notes', null),
    };

    for (var propName in data) {
        if (data[propName] === null) {
            delete data[propName];
        } else {
            data[propName] = { $like: '%' + data[propName] + '%' };
        }
    }

    api.requireSignedIn(req, function (user) {
        
        db.Quote.findOne({
            where: {
                id: quote_id
            }
        }).then(function (quote) {
            if (!quote) throw new errors.HaveNoRightsError();

            if(quote.user_id !== user.id && user.is_admin == 0)
                throw new errors.HaveNoRightsError();

            return db.Vehicle.findOne({
                where:{
                    id: vehicle_id,
                    quote_id: quote.id 
                }
            });
        }).then(function (vehicle) {
            if (!vehicle) throw new errors.NotFoundError();

            return db.Driver.findAll({
                where: data
            });

        }).then(function (resDrivers) {
            if (!resDrivers) throw new errors.NotFoundError();
            res.send(resDrivers);
            next();
        });

    });

};