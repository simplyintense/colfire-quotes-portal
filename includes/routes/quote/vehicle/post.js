﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/quotes/:quote_id/vehicles';
exports.method = 'post';


exports.handler = function(req, res, next) {
    
    var quote_id = parseInt(req.params.quote_id || 0, 10);

    var data = {
        status: api.getParam(req, 'status', 'active'),
        //young_driver_age: api.getParam(req, 'young_driver_age', null),
        //new_driver_permit: api.getParam(req, 'new_driver_permit', null),
        insurance_type_comprehensive: api.getParam(req, 'insurance_type_comprehensive', null),
        insurance_type_third_party_fire_theft: api.getParam(req, 'insurance_type_third_party_fire_theft', null),
        insurance_type_third_party: api.getParam(req, 'insurance_type_third_party', null),
        current_value: api.getParam(req, 'current_value', null),
        vehicle_registration_number: api.getParam(req, 'vehicle_registration_number', null),
        vehicle_protected: api.getParam(req, 'vehicle_protected', null),
        make: api.getParam(req, 'make', null),
        model: api.getParam(req, 'model', null),
        year: api.getParam(req, 'year', null),
        vehicle_origin: api.getParam(req, 'vehicle_origin', null),
        custom_descr_make_model: api.getParam(req, 'custom_descr_make_model', null),
        vehicle_age: api.getParam(req, 'vehicle_age', null),
        usage_type: api.getParam(req, 'usage_type', null),
        accidents_recently: api.getParam(req, 'accidents_recently', null),
        //accidentdate: api.getParam(req, 'accidentdate', null),
        //you_deemed_responsible: api.getParam(req, 'you_deemed_responsible', null),
        //accident_cost: api.getParam(req, 'accident_cost', null),
        //accident_description: api.getParam(req, 'accident_description', null),
        windscreen_coverage: api.getParam(req, 'windscreen_coverage', null),
        windscreen_coverage_amount: api.getParam(req, 'windscreen_coverage_amount', null),
        emergency_roadside_assist: api.getParam(req, 'emergency_roadside_assist', null),
        windscreen_coverage_3rd_party: api.getParam(req, 'windscreen_coverage_3rd_party', null),
        windscreen_coverage_amount_3rd_party: api.getParam(req, 'windscreen_coverage_amount_3rd_party', null),
        emergency_roadside_assist_3rd_party: api.getParam(req, 'emergency_roadside_assist_3rd_party', null),
        windscreen_coverage_3rd_party_fire: api.getParam(req, 'windscreen_coverage_3rd_party_fire', null),
        windscreen_coverage_amount_3rd_party_fire: api.getParam(req, 'windscreen_coverage_amount_3rd_party_fire', null),
        emergency_roadside_assist_3rd_party_fire: api.getParam(req, 'emergency_roadside_assist_3rd_party_fire', null),
        flood_special_perils: api.getParam(req, 'flood_special_perils', null),
        partial_waiver_of_excess: api.getParam(req, 'partial_waiver_of_excess', null),
        loss_of_use: api.getParam(req, 'loss_of_use', null),
        new_for_old: api.getParam(req, 'new_for_old', null),
        no_claim_discount_transfer: api.getParam(req, 'no_claim_discount_transfer', null),
        no_claim_discount_years: api.getParam(req, 'no_claim_discount_years', null),
        member_of_credit_union: api.getParam(req, 'member_of_credit_union', null),
        live_close_to_work: api.getParam(req, 'live_close_to_work', null),
        defensive_driver_cert: api.getParam(req, 'defensive_driver_cert', null),
        defensive_driver_school: api.getParam(req, 'defensive_driver_school', null),
        loan: api.getParam(req, 'loan', null),
        bank_name: api.getParam(req, 'bank_name', null),
        custom_loan_bank: api.getParam(req, 'custom_loan_bank', null),
        credit_card: api.getParam(req, 'credit_card', null),
        credit_card_bank: api.getParam(req, 'credit_card_bank', null),
        credit_card_bank_custom: api.getParam(req, 'credit_card_bank_custom', null),
        sole_driver: api.getParam(req, 'sole_driver', null),
        notes: api.getParam(req, 'notes', null),
        have_child: api.getParam(req, 'have_child', null),
	};

    var driver_firstname = '';
    var driver_lastname = '';
    
    api.requireSignedIn(req, function (user) {

        db.Quote.findOne({
            where: {
                id: quote_id
            },
            include:{
                model: db.User,
                attributes: ['id'],
                include: {
                    model: db.Profile,
                    attributes: ['id','firstname','lastname']
                }
            }
        }).then(function (quote) {
            if (!quote) throw new errors.HaveNoRightsError();

            if(quote.user_id !== user.id && user.is_admin == 0)
                throw new errors.HaveNoRightsError();
            
            driver_firstname =  quote.User.Profile.firstname;
            driver_lastname = quote.User.Profile.lastname;
                
            return quote.createVehicle(data);

        }).then(function (resVehicle) {
            if (!resVehicle) throw new errors.NotFoundError();
            /*
            resVehicle.createDriver({
                firstname: driver_firstname,
                lastname: driver_lastname,
            });
            */
            res.send(resVehicle);
            next();
        });


	});
    
};