﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/quotes/:quote_id/vehicles/:vehicle_id/accidents';
exports.method = 'post';


exports.handler = function(req, res, next) {
    
    var quote_id = parseInt(req.params.quote_id || 0, 10);
    var vehicle_id = parseInt(req.params.vehicle_id || 0, 10);

    var data = {
        status: api.getParam(req, 'status', 'active'),
        accidentdate: api.getParam(req, 'accidentdate', null),
        you_deemed_responsible: api.getParam(req, 'you_deemed_responsible', null),
        accident_cost: api.getParam(req, 'accident_cost', null),
        accident_description: api.getParam(req, 'accident_description', null),
	};


    api.requireSignedIn(req, function (user) {

        db.Quote.findOne({
            where: {
                id: quote_id
            }
        }).then(function (quote) {
            if (!quote) throw new errors.HaveNoRightsError();

            if(quote.user_id !== user.id && user.is_admin == 0)
                throw new errors.HaveNoRightsError();

            return db.Vehicle.findOne({
                where:{
                    id: vehicle_id,
                    quote_id: quote.id 
                }
            });
        }).then(function (vehicle) {
            if (!vehicle) throw new errors.NotFoundError();

            return vehicle.createAccident(data);

        }).then(function (resAccident) {
            if (!resAccident) throw new errors.NotFoundError();
            res.send(resAccident);
            next();
        });


	});
    
};