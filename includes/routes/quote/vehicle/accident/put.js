﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/quotes/:quote_id/vehicles/:vehicle_id/accidents/:accident_id';
exports.method = 'put';


exports.handler = function(req, res, next) {
    
    var quote_id = parseInt(req.params.quote_id || 0, 10);
    var vehicle_id = parseInt(req.params.vehicle_id || 0, 10);
    var accident_id = parseInt(req.params.accident_id || 0, 10);

    var data = {
        status: api.getParam(req, 'status', null),
        accidentdate: api.getParam(req, 'accidentdate', null),
        you_deemed_responsible: api.getParam(req, 'you_deemed_responsible', null),
        accident_cost: api.getParam(req, 'accident_cost', null),
        accident_description: api.getParam(req, 'accident_description', null),
	};

    
    api.requireSignedIn(req, function (user) {

        db.Quote.findOne({
            where: {
                id: quote_id
            }
        }).then(function(quote){
            if(!quote) throw new errors.NotFoundError();

            if(user.id !== quote.user_id && user.is_admin == 0)
                throw new errors.HaveNoRightsError();

            return db.Vehicle.findOne({
                where: {
                    id: vehicle_id,
                    quote_id: quote.id
                }
            });
        }).then(function(vehicle){
            if (!vehicle) throw new errors.NotFoundError();

            return db.Accident.findOne({
                where: {
                    id: accident_id,
                    vehicle_id: vehicle.id
                }
            });
        }).then(function(accident){
            if (!accident) throw new errors.NotFoundError();

            if (data.status !== null) accident.status = data.status;
            if (data.accidentdate !== null) accident.accidentdate = data.accidentdate;
            if (data.you_deemed_responsible !== null) accident.you_deemed_responsible = data.you_deemed_responsible;
            if (data.accident_cost !== null) accident.accident_cost = data.accident_cost;
            if (data.accident_description !== null) accident.accident_description = data.accident_description;
            
            return accident.save();

        }).then(function(resAccident){
            res.send(resAccident);
            next();
        });


	});
    
};