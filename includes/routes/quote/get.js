﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/quotes/:quote_id';
exports.method = 'get';


exports.handler = function (req, res, next) {

    var quote_id = parseInt(req.params.quote_id || 0, 10);

    api.requireSignedIn(req, function (user) {

        var includedTables = [];
        if(user.is_admin == 1){
            includedTables.push(
                {
                    model: db.User,
                    attributes: ['id'],
                    include: {
                        model: db.Profile
                    }
                },
                {
                    model: db.Vehicle,
                    where: {
                        status: 'active',
                    },
                    required: false,
                    include: [
                        {
                            model: db.Driver,
                            where: {
                                status: 'active',
                            },
                            required: false,
                        },
                        {
                            model: db.Accident,
                            where: {
                                status: 'active',
                            },
                            required: false,
                        }
                    ]
                },
                {
                    model: db.Immovable,
                    include:{
                        model: db.Claim,
                        where: {
                            status:'active',
                        },
                        required: false,
                    }
                }
            );
        }

        db.Quote.findOne({
            where: {
                id: quote_id
            },
            include: includedTables
        }).then(function(quote){
            if(!quote) throw new errors.NotFoundError();

            if(user.id !== quote.user_id && user.is_admin == 0)
                throw new errors.HaveNoRightsError();

            res.send(quote);
            next();

        });
        
    });
};