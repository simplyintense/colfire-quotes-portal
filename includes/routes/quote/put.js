﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');

exports.route = '/api/quotes/:quote_id';
exports.method = 'put';


exports.handler = function (req, res, next) {

    var quote_id = parseInt(req.params.quote_id || 0, 10);

    var data = {
        status: api.getParam(req, 'status', null),
        insurance_type: api.getParam(req, 'insurance_type', null),
        user_id: api.getParam(req, 'user_id', null),
        notes: api.getParam(req, 'notes', null),
        rating: api.getParam(req, 'rating', null),
        ratingFeedback: api.getParam(req, 'ratingFeedback', null),
    };

    var send_mail = false;

    api.requireSignedIn(req, function (user) {

        db.Quote.findOne({
            where: {
                id: quote_id
            }
        }).then(function(quote){
            if(!quote) throw new errors.NotFoundError();

            if(user.id !== quote.user_id && user.is_admin == 0)
                throw new errors.HaveNoRightsError();

            
            if(quote.status !== 'submitted' && data.status == 'submitted')
                send_mail = true;

            if (data.status !== null) quote.status = data.status;
            if (data.insurance_type !== null) quote.insurance_type = data.insurance_type;
            if (data.user_id !== null) quote.user_id = data.user_id;
            if (data.notes !== null) quote.notes = data.notes;
            if (data.rating !== null) quote.rating = data.rating;
            if (data.ratingFeedback !== null) quote.ratingFeedback = data.ratingFeedback;
            
            return quote.save();
            
        }).then(function (resQuote) {
            if (!resQuote) throw new errors.NotFoundError();

            if(send_mail){
                db.Quote.sendConfirmationEmail({
                    quote_id:resQuote.id,
                });
            }

            res.send(resQuote);
            next();
        });
        
    });
};