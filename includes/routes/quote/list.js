﻿var rfr = require('rfr');
var db = rfr('includes/models');
var errors = rfr('includes/errors.js');
var api = rfr('includes/api.js');
var json2csv = require('json2csv');
var moment = db.moment;
var mail_replacements = rfr('includes/mail_replacements.js');

exports.route = '/api/quotes';
exports.method = 'get';


exports.handler = function (req, res, next) {

    var quote_id = parseInt(req.params.quote_id || 0, 10);
    var csv = api.getParam(req, 'csv', 'no');
    var proptype = api.getParam(req, 'proptype', 'no');
    var statquote = api.getParam(req, 'statquote', '');
    var startdate = api.getParam(req, 'startdate', 'no');
    var enddate = api.getParam(req, 'enddate', 'no');

    if (startdate == 'no' && csv !== 'no') {
        throw new errors.ValidationError();
    } else {
        startdate = moment(startdate).set({ 'hour': 0, 'minute': 0, 'second': 0 });
        enddate = moment(enddate).set({ 'hour': 0, 'minute': 0, 'second': 0 });
    }

    var data = {
        status: api.getParam(req, 'status', ''),
        insurance_type: api.getParam(req, 'insurance_type', ''),
        user_id: api.getParam(req, 'user_id', ''),
        ref_num: api.getParam(req, 'ref_num', ''),
    };

    for (var propName in data) {
        if (data[propName] === '') {
            delete data[propName];
        } else {
            if (data[propName] !== 'user_id')
                data[propName] = { $like: '%' + data[propName] + '%' };
        }
    }

    var customer_data = {
        firstname: api.getParam(req, 'firstname', ''),
        lastname: api.getParam(req, 'lastname', ''),
        email: api.getParam(req, 'email', ''),
        phone_number: api.getParam(req, 'phone_number', ''),
    };

    for (var propName in customer_data) {
        if (customer_data[propName] === '') {
            delete customer_data[propName];
        } else {
            if (customer_data[propName] !== 'id')
                customer_data[propName] = { $like: '%' + customer_data[propName] + '%' };
        }
    }


    api.requireSignedIn(req, function (user) {
        if ((!data.user_id || data.user_id !== user.id) && user.is_admin == 0)
            data.user_id = user.id;


        if (csv === 'yes' && user.is_admin === 1) {
            //Movable section
            var where = {
                //insurance_type: 'motor'
                createdAt: {
                    $gte: startdate,
                    $lte: enddate
                }
            };
            if (statquote == 'submitted' || statquote == 'active') {
                where.status = statquote;
            }
            

            if (proptype == 'movable') {
                var includedTables = [];
                if (user.is_admin == 1) {
                    includedTables.push(
                        {
                            model: db.Quote,
                            attributes: ['id', 'status', 'ref_num', 'rating', 'ratingFeedback', 'CreatedAt'],
                            include: {
                                model: db.User,
                                attributes: ['id'],
                                include: {
                                    model: db.Profile,
                                    attributes: ['firstname', 'lastname', 'dateofbirth', 'gender', 'email', 'occupation', 'phone_number', 'home_address', 'current_insurer', 'cur_cust_years', 'notes', 'status', 'CreatedAt']
                                }
                            },
                            
                            where: where,
                        },
                        {
                            model: db.Driver,
                            where: {
                                status: 'active',
                            },
                            required: false,
                        },
                        {
                            model: db.Accident,
                            where: {
                                status: 'active',
                            },
                            required: false,
                        }
                    );
                }

                db.Vehicle.findAll({
                    where: data,
                    include: includedTables,
                }).then(function (vehicles) {

                    var fields = [
                        {
                            label: 'Reference number',
                            value: function (row, field, data) {
                                if (row.Quote.ref_num) {
                                    return row.Quote.ref_num;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Quote Status',
                            value: function (row, field, data) {
                                if (row.Quote.status) {
                                    return row.Quote.status;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Quote Created',
                            value: function (row, field, data) {
                                    return moment(row.createdAt).format('DD/MM/YYYY');
                            },
                            default: ''
                        },
                        {
                            label: 'Contact Name, gender',
                            value: function (row, field, data) {
                                if (row.Quote.User.Profile.firstname || row.Quote.User.Profile.lastname || row.Quote.User.Profile.gender) {
                                    return row.Quote.User.Profile.firstname + ' ' + row.Quote.User.Profile.lastname + ', ' + row.Quote.User.Profile.gender;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Home Address',
                            value: function (row, field, data) {
                                if (row.Quote.User.Profile.home_address) {
                                    return row.Quote.User.Profile.home_address;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Email',
                            value: function (row, field, data) {
                                if (row.Quote.User.Profile.email) {
                                    return row.Quote.User.Profile.email;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Phone Number',
                            value: function (row, field, data) {
                                if (row.Quote.User.Profile.phone_number) {
                                    return row.Quote.User.Profile.phone_number;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Occupation',
                            value: function (row, field, data) {
                                if (row.Quote.User.Profile.occupation) {
                                    return row.Quote.User.Profile.occupation;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Date of Birth',
                            value: function (row, field, data) {
                                if (row.Quote.User.Profile.dateofbirth) {
                                    return moment(row.Quote.User.Profile.dateofbirth).format('DD/MM/YYYY');
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Current Insurer',
                            value: function (row, field, data) {
                                return row.Quote.User.Profile.current_insurer;
                            },
                            default: ''
                        },
                        {
                            label: 'Years a current customer',
                            value: function (row, field, data) {
                                return row.Quote.User.Profile.cur_cust_years;
                            },
                            default: ''
                        },
                        {
                            label: 'Vehicle description section',
                            value: function (row, field, data) {
                                return '        ||';
                            },
                            default: ''
                        },
                        {
                            label: 'Vehicle',
                            value: function (row, field, data) {
                                if (row.make != null) {
                                    return row.make + ' ' + row.model;
                                } else {
                                    if (row.custom_descr_make_model == '' || row.custom_descr_make_model == null || row.custom_descr_make_model == 'null') {
                                        return '';
                                    } else {
                                        return row.make + ' ' + row.custom_descr_make_model;
                                    }
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Year of manufacture',
                            value: function (row, field, data) {
                                return row.year;
                            },
                            default: ''
                        },
                        {
                            label: 'Vehicle registration number',
                            value: function (row, field, data) {
                                return row.vehicle_registration_number;
                            },
                            default: ''
                        },
                        {
                            label: 'Value of vehicle',
                            value: function (row, field, data) {
                                return row.current_value;
                            },
                            default: ''
                        },
                        {
                            label: 'Type of origination',
                            value: function (row, field, data) {
                                return mail_replacements.vehicle_origin[row.vehicle_origin];
                            },
                            default: ''
                        },
                        {
                            label: 'Vehicle protected',
                            value: function (row, field, data) {
                                return row.vehicle_protected;
                            },
                            default: ''
                        },
                        {
                            label: 'Accidents recently',
                            value: function (row, field, data) {
                                return row.accidents_recently;
                            },
                            default: ''
                        },
                        {
                            label: 'Motor insurance type =>',
                            value: function (row, field, data) {
                                return '        ||';
                            },
                            default: ''
                        },
                        {
                            label: 'Comprehensive',
                            value: function (row, field, data) {
                                if (row.insurance_type_comprehensive) {
                                    return row.insurance_type_comprehensive;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Third party fire and theft',
                            value: function (row, field, data) {
                                if (row.insurance_type_third_party_fire_theft) {
                                    return row.insurance_type_third_party_fire_theft;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Third party',
                            value: function (row, field, data) {
                                if (row.insurance_type_third_party) {
                                    return row.insurance_type_third_party;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Vehicle is used for',
                            value: function (row, field, data) {
                                return mail_replacements.car_usage_types[row.usage_type];
                            },
                            default: ''
                        },
                        {
                            label: 'Driver section',
                            value: function (row, field, data) {
                                return '        ||';
                            },
                            default: ''
                        },
                        {
                            label: 'Drivers info',
                            value: function (row, field, data) {
                                //var b = 1;
                                if (row.Drivers.length > 0) {
                                    var permit = '';
                                    var b = 1;
                                    row.Drivers.forEach(function(element) {
                                        permit = permit + ' [Driver ' + b + ': ' + element.firstname + ' ' + element.lastname + ' | ' + 'Driver age under 25? -' + element.young_driver_age + ' | ' + 'Drivers permit less than 24 months old? -' + element.new_driver_permit + '] ';
                                        b = b + 1;
                                    });
                                    return permit;
                                } else {
                                    return '';
                                }
                            }, 
                            default: ''
                        },
                        {
                            label: 'Accident section',
                            value: function (row, field, data) {
                                return '        ||';
                            },
                            default: ''
                        },
                        {
                            label: 'Accidents details',
                            value: function (row, field, data) {
                                if (row.Accidents.length > 0) {
                                    var instance = '';
                                    var d = 1;
                                    row.Accidents.forEach(function (element) {
                                        instance = instance + ' [Accident ' + d + ': ' + 'Last accident date ' + moment(element.accidentdate).format('DD/MM/YYYY') + ' | ' + 'Were deemed responsible? -' + element.you_deemed_responsible + ' | ' + 'How much was paid in total to you/third party? - $' + element.accident_cost + ' | ' + 'Description of the accident - ' + element.accident_description + '] ';
                                        d = d + 1;
                                    });
                                    return instance;
                                } else {
                                    return '';
                                }
                            }
                        },
                        {
                            label: 'Additional benefits section',
                            value: function (row, field, data) {
                                return '        ||        ';
                            },
                            default: ''
                        },
                        {
                            label: 'Additional benefits (Third Party)',
                            value: function (row, field, data) {
                                return '[Windscreen Coverage: ' + row.windscreen_coverage_3rd_party + ' | ' + 'Windscreen Coverage Amount: $' + row.windscreen_coverage_amount_3rd_party + ' | ' + 'Emergency Roadside Assist: ' + row.emergency_roadside_assist_3rd_party + '] ';
                           },
                            default: ''
                        },
                        {
                            label: 'Additional benefits (Third Party Fire & Theft)',
                            value: function (row, field, data) {
                                       return '[Windscreen Coverage: ' + row.windscreen_coverage_3rd_party_fire + ' | ' + 'Windscreen Coverage Amount: $' + row.windscreen_coverage_amount_3rd_party_fire + ' | ' + 'Emergency Roadside Assist: ' + row.emergency_roadside_assist_3rd_party_fire + '] ';
                            },
                            default: ''
                        },
                        {
                            label: 'Additional benefits (Comprehensive)',
                            value: function (row, field, data) {
                                return '[Windscreen Coverage: ' + row.windscreen_coverage + ' | ' + 'Windscreen Coverage Amount: $' + row.windscreen_coverage_amount + ' | ' + 'Emergency Roadside Assist: ' + row.emergency_roadside_assist + ' | ' + 'Flood Special Perils: ' + row.flood_special_perils + ' | ' + 'Partial Waiver of Excess: ' + row.partial_waiver_of_excess + ' | ' + 'Loss of Use: ' + row.loss_of_use + ' | ' + 'New for Old: ' + row.new_for_old + '] ';
                            },
                            default: ''
                        },
                        {
                            label: 'Discounts info section',
                            value: function (row, field, data) {
                                return '        ||        ';
                            },
                            default: ''
                        },
                        {
                            label: 'Have a no claim discount that will be transferring from another policy?',
                            value: function (row, field, data) {
                                return row.no_claim_discount_transfer;
                            },
                            default: ''
                        },
                        {
                            label: 'Number of years for a no claim discount',
                            value: function (row, field, data) {
                                return row.no_claim_discount_years;
                            },
                            default: ''
                        },
                        {
                            label: 'Membership at a Credit Union?',
                            value: function (row, field, data) {
                                return row.member_of_credit_union;
                            },
                            default: ''
                        },
                        {
                            label: 'Live within 20km of your work?',
                            value: function (row, field, data) {
                                return row.live_close_to_work;
                            },
                            default: ''
                        },
                        {
                            label: 'Have a defensive driving certificate?',
                            value: function (row, field, data) {
                                return row.defensive_driver_cert;
                            },
                            default: ''
                        },
                        {
                            label: 'Name of the defensive driving school',
                            value: function (row, field, data) {
                                return row.defensive_driver_school;
                            },
                            default: ''
                        },
                        {
                            label: 'Hold a loan on this vehicle?',
                            value: function (row, field, data) {
                                return row.loan;
                            },
                            default: ''
                        },
                        {
                            label: 'Bank name(loan)',
                            value: function (row, field, data) {
                                if (row.bank_name == 'other') {
                                    return mail_replacements.loan_bank_vehicle[row.bank_name] + ': ' + row.custom_loan_bank;
                                } else {
                                    return mail_replacements.loan_bank_vehicle[row.bank_name];
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Pay with a credit card?',
                            value: function (row, field, data) {
                                return row.credit_card;
                            },
                            default: ''
                        },
                        {
                            label: 'Credit Card Bank name',
                            value: function (row, field, data) {
                                if (row.credit_card_bank == 'other') {
                                    return mail_replacements.credit_card_bank[row.credit_card_bank] + ': ' + row.credit_card_bank_custom;
                                } else {
                                    return mail_replacements.credit_card_bank[row.credit_card_bank];
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Would this be only you driving the vehicle?',
                            value: function (row, field, data) {
                                return row.sole_driver;
                            },
                            default: ''
                        },
                        {
                            label: 'Have a child under 4 years?',
                            value: function (row, field, data) {
                                return row.have_child;
                            },
                            default: ''
                        },
                        {
                            label: 'Rating section',
                            value: function (row, field, data) {
                                return '        ||        ';
                            },
                            default: ''
                        },
                        {
                            label: 'Rating',
                            value: function (row, field, data) {
                                if (row.Quote.rating) {
                                    return row.Quote.rating + ' | ' + row.Quote.ratingFeedback;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                    ];

                    var rsVehicles = json2csv({ data: vehicles, fields: fields });

                    res.writeHead(200, {
                        'Content-Type': 'text/csv',
                        'Content-Disposition': 'attachment; filename=export.csv'
                    });

                    res.end(rsVehicles);
              
                });
            }

            //Immovable section
            var where = {
                //insurance_type: 'motor'
                createdAt: {
                    $gte: startdate,
                    $lte: enddate
                }
            };
            if (statquote == 'submitted' || statquote == 'active') {
                where.status = statquote;
            }
            if (proptype == 'immovable') {
                var includedTables = [];
                if (user.is_admin == 1) {
                    includedTables.push(
                        {
                            model: db.Quote,
                            attributes: ['id', 'status', 'ref_num', 'rating', 'ratingFeedback', 'CreatedAt'],
                            include: {
                                model: db.User,
                                attributes: ['id'],
                                include: {
                                    model: db.Profile,
                                    attributes: ['firstname', 'lastname', 'dateofbirth', 'gender', 'email', 'occupation', 'phone_number', 'home_address', 'current_insurer', 'cur_cust_years', 'notes', 'status', 'CreatedAt']
                                }
                            },
                            where: where,
                        },
                        {
                            model: db.Claim,
                            where: {
                                status: 'active'
                            },
                            required: false,                        
                        }
                    );
                }

                db.Immovable.findAll({
                    where: data,
                    include: includedTables,
                }).then(function (buildings) {

                    var fields = [
                        {
                            label: 'Reference number',
                            value: function (row, field, data) {
                                if (row.Quote.ref_num) {
                                    return row.Quote.ref_num;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Quote Status',
                            value: function (row, field, data) {
                                if (row.Quote.status) {
                                    return row.Quote.status;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Quote Created',
                            value: function (row, field, data) {
                                    return moment(row.createdAt).format('DD/MM/YYYY');
                            },
                            default: ''
                        },
                        {
                            label: 'Contact Name, gender',
                            value: function (row, field, data) {
                                if (row.Quote.User.Profile.firstname || row.Quote.User.Profile.lastname || row.Quote.User.Profile.gender) {
                                    return row.Quote.User.Profile.firstname + ' ' + row.Quote.User.Profile.lastname + ', ' + row.Quote.User.Profile.gender;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Home Address',
                            value: function (row, field, data) {
                                if (row.Quote.User.Profile.home_address) {
                                    return row.Quote.User.Profile.home_address;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Email',
                            value: function (row, field, data) {
                                if (row.Quote.User.Profile.email) {
                                    return row.Quote.User.Profile.email;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Phone Number',
                            value: function (row, field, data) {
                                if (row.Quote.User.Profile.phone_number) {
                                    return row.Quote.User.Profile.phone_number;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Occupation',
                            value: function (row, field, data) {
                                if (row.Quote.User.Profile.occupation) {
                                    return row.Quote.User.Profile.occupation;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Date of Birth',
                            value: function (row, field, data) {
                                if (row.Quote.User.Profile.dateofbirth) {
                                    return moment(row.Quote.User.Profile.dateofbirth).format('DD/MM/YYYY');
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Current Insurer',
                            value: function (row, field, data) {
                                return row.Quote.User.Profile.current_insurer;
                            },
                            default: ''
                        },
                        {
                            label: 'Years a current customer',
                            value: function (row, field, data) {
                                return row.Quote.User.Profile.cur_cust_years;
                            },
                            default: ''
                        },
                        {
                            label: 'Property info section',
                            value: function (row, field, data) {
                                return '        ||';
                            },
                            default: ''
                        },
                        {
                            label: 'Address line 1',
                            value: function (row, field, data) {
                                return row.address_line_1;
                            },
                            default: ''
                        },
                        {
                            label: 'Address line 2',
                            value: function (row, field, data) {
                                return row.address_line_2;
                            },
                            default: ''
                        },
                        {
                            label: 'Address line 3',
                            value: function (row, field, data) {
                                return row.address_line_3;
                            },
                            default: ''
                        },
                        {
                            label: 'City',
                            value: function (row, field, data) {
                                return row.city;
                            },
                            default: ''
                        },
                        {
                            label: 'Property insurance type',
                            value: function (row, field, data) {
                                return mail_replacements.immovable_insurance_types[row.insurance_type];
                            },
                            default: ''
                        },
                        {
                            label: 'Building insurance value',
                            value: function (row, field, data) {
                                return row.building_value;
                            },
                            default: ''
                        },
                        {
                            label: 'Contents insurance value',
                            value: function (row, field, data) {
                                return row.contents_value;
                            },
                            default: ''
                        },
                        {
                            label: 'Building usage type',
                            value: function (row, field, data) {
                                return mail_replacements.immovable_usage_types[row.usage_type];
                            },
                            default: ''
                        },
                        {
                            label: 'Property on a hill?',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.on_hill];
                            },
                            default: ''
                        },
                        {
                            label: 'Seafront of riverfront?',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.seafront_waterway];
                            },
                            default: ''
                        },
                        {
                            label: 'Wall type',
                            value: function (row, field, data) {
                                if (row.wall_type == 'other') {
                                    return mail_replacements.wall_types[row.wall_type] + ': ' + row.wall_type_other;
                                } else {
                                    return mail_replacements.wall_types[row.wall_type];
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Roof type',
                            value: function (row, field, data) {
                                if (row.roof_type == 'other') {
                                    return mail_replacements.roof_types[row.roof_type] + ': ' + row.roof_type_other;
                                } else {
                                    return mail_replacements.roof_types[row.roof_type];
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Floor type',
                            value: function (row, field, data) {
                                if (row.floor_type == 'other') {
                                    return mail_replacements.floor_types[row.floor_type] + ': ' + row.floor_type_other;
                                } else {
                                    return mail_replacements.floor_types[row.floor_type];
                                }
                            },
                            default: ''
                        },
                        {
                            label: 'Proximity to nearest building',
                            value: function (row, field, data) {
                                return mail_replacements.proximity_to_nearest_building_types[row.proximity_to_nearest_building];
                            },
                            default: ''
                        },
                        {
                            label: 'Age of the building',
                            value: function (row, field, data) {
                                return mail_replacements.building_age_types[row.building_age];
                            },
                            default: ''
                        },
                        {
                            label: 'Fire extinguishers',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.fire_extinguishers];
                            },
                            default: ''
                        },
                        {
                            label: 'Hose reels',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.hose_reels];
                            },
                            default: ''
                        },
                        {
                            label: 'Sprinkler system',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.sprinkler_system];
                            },
                            default: ''
                        },
                        {
                            label: 'Smoke detectors',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.smoke_detectors];
                            },
                            default: ''
                        },
                        {
                            label: 'Alarms',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.alarms];
                            },
                            default: ''
                        },
                        {
                            label: 'Security personnel',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.security_personnel];
                            },
                            default: ''
                        },
                        {
                            label: 'Deadbolts / Padlocks',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.deadbolts_padlocks];
                            },
                            default: ''
                        },
                        {
                            label: 'Gated community',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.gated_community];
                            },
                            default: ''
                        },
                        {
                            label: 'Enclosed / Fenced',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.gated_property];
                            },
                            default: ''
                        },
                        {
                            label: 'Burglar proofing',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.burglar_proofing];
                            },
                            default: ''
                        },
                        {
                            label: 'Claims section',
                            value: function (row, field, data) {
                                return '        ||';
                            },
                            default: ''
                        },
                        {
                            label: 'Claims details',
                            value: function (row, field, data) {
                                if (row.Claims.length > 0) {
                                    var instance = '';
                                    var k = 1;
                                    row.Claims.forEach(function (element) {
                                        instance = instance + ' [Claim ' + k + ': ' + 'Claim date: ' + moment(element.claim_date).format('DD/MM/YYYY') + ' | ' + 'Claim estimated value: $' + element.estimated_value + ' | ' + 'Claim description: ' + element.claims_history_descr + '] ';
                                        k = k + 1;
                                    });
                                    return instance;
                                } else {
                                    return '';
                                }
                            }
                        },
                        {
                            label: 'Additional benefits section',
                            value: function (row, field, data) {
                                return '        ||';
                            },
                            default: ''
                        },
                        {
                            label: 'Flood',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.flood];
                            },
                            default: ''
                        },
                        {
                            label: 'Subsidence and Landslip',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.subsidence_and_landslip];
                            },
                            default: ''
                        },
                        {
                            label: 'Retaining wall',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.subsidence_and_landslip_retaining_wall];
                            },
                            default: ''
                        },
                        {
                            label: 'Retaining wall insured amount, $',
                            value: function (row, field, data) {
                                return row.subsidence_and_landslip_insured;
                            },
                            default: ''
                        },
                        {
                            label: 'Damage to external radio and TV aerials',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.damage_to_external_radio_and_tv_aerials];
                            },
                            default: ''
                        },
                        {
                            label: 'Damage to underground water,gas pipes or electricity cables',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.damage_to_undergr_water_gas_pipes_or_electr_cables];
                            },
                            default: ''
                        },
                        {
                            label: 'Workmen compensation for any domestic workers',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.workmen_compens_any_domestic_workers];
                            },
                            default: ''
                        },
                        {
                            label: 'Removal of debris',
                            value: function (row, field, data) {
                                return mail_replacements.yes_no_list[row.removal_debris];
                            },
                            default: ''
                        },
                        {
                            label: 'Rating section',
                            value: function (row, field, data) {
                                return '        ||        ';
                            },
                            default: ''
                        },
                        {
                            label: 'Rating',
                            value: function (row, field, data) {
                                if (row.Quote.rating) {
                                    return row.Quote.rating + ' | ' + row.Quote.ratingFeedback;
                                } else {
                                    return '';
                                }
                            },
                            default: ''
                        },
                    ];
                    var rsBuildings = json2csv({ data: buildings, fields: fields });

                        res.writeHead(200, {
                            'Content-Type': 'text/csv',
                            'Content-Disposition': 'attachment; filename=export.csv'
                        });

                        res.end(rsBuildings);
              
                    });
            }
        }
        else {

            var includedTables = [];
            if (user.is_admin == 1) {
                includedTables.push(
                    {
                        model: db.User,
                        attributes: ['id'],
                        include: {
                            model: db.Profile,
                            where: customer_data
                        }
                    },
                    {
                        model: db.Vehicle,
                        where: {
                            status: 'active',
                        },
                        required: false,
                        include: [
                            {
                                model: db.Driver,
                                where: {
                                    status: 'active',
                                },
                                required: false,
                            },
                            {
                                model: db.Accident,
                                where: {
                                    status: 'active',
                                },
                                required: false,
                            }
                        ]
                    },
                    {
                        model: db.Immovable,
                        include: {
                            model: db.Claim,
                            where: {
                                status: 'active',
                            },
                            required: false,
                        }
                    }
                );
            }

            db.Quote.findAll({
                where: data,
                include: includedTables,
            }).then(function (quotes) {

                res.send(quotes);
                next();
            });
        }
    });
};