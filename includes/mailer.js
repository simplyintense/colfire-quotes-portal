var nodemailer = require('nodemailer');
var rfr = require('rfr');
var fs = require('fs');
var jSmart = require('jsmart');

var moment = require('moment-timezone'); //require('moment');
console.log('MAILER: Set api timezone to America/Port_of_Spain and locale to en-tt');
moment.tz.setDefault('America/Port_of_Spain');
moment.locale('en-tt');

jSmart.prototype.registerPlugin(
    'function',
    'frmtDate',
    function (params, data) {
        var s = '';
        if ('datetimeval' in params && 'frmt' in params) {
            s += moment(params['datetimeval']).format(params['frmt']);
        }
        return s;
    }
);

var postmark = require("postmark");
//postmark expects the following json structure in config.json:
//"smtp": {
//    "postmark_api": "762d019c-4dbb-4bc7-a1e7-e0eb0c168e18"
//},

//nodemailer expects the following json structure in config.json:
//also if using gmail after first sending visit the gmail account allow unsecure apps access 
//and approve warning dialog in the security part
//"smtp": {
//    "host": "smtp.gmail.com",
//    "username": "fromaddress@gmail.com",
//    "password": "[account password]",
//    "port": "587"
//},

var config = rfr('includes/config.js');
var mail_replacements = rfr('includes/mail_replacements.js');

var transporter = null;
var postmark_transporter = null;
if (config && config.smtp) {
	if (config.smtp.use_env_variables) {

        if(process.env[config.smtp.postmark_api]){
            postmark_transporter = new postmark.Client(process.env[config.smtp.postmark_api]);
        }else{
		    transporter = nodemailer.createTransport({
			    host: process.env[config.smtp.host],
			    port: process.env[config.smtp.port],
			    auth: {
				    user: process.env[config.smtp.username],
				    pass: process.env[config.smtp.password]
			    }
		    });
        }
	} else {
        if(config.smtp.postmark_api){
            postmark_transporter = new postmark.Client(config.smtp.postmark_api);
        }else{
		    transporter = nodemailer.createTransport({
			    host: config.smtp.host,
			    port: config.smtp.port,
			    auth: {
				    user: config.smtp.username,
				    pass: config.smtp.password
			    }
		    });
        }
	}

	if (transporter !== null && config.default_from_email)
		transporter.default_from_email = config.default_from_email;

    if (postmark_transporter !== null && config.default_from_email)
		postmark_transporter.default_from_email = config.default_from_email;
    
}


var default_replaces = {};
if (config.website_name)
	default_replaces.website_name = config.website_name;

if (config.site_path)
	default_replaces.site_path = config.site_path;

if (config.site_path_for_mail)
	default_replaces.site_path_for_mail = config.site_path_for_mail;



var __mailtemplates_cache = {};

var sendMail = function(to, subject, html) {
	if (transporter !== null)
		transporter.sendMail({
			from: transporter.default_from_email,
			to: to,
			subject: subject,
			html: html
		});

    if (postmark_transporter !== null)
		postmark_transporter.sendEmail({
            "From": postmark_transporter.default_from_email, 
            "To": to, 
            "Subject": subject, 
            "HtmlBody": html, //"TextBody": "Test Message",
        }, function(error, result) {
            if(error) {
                console.error("Unable to send via postmark: " + error.message);
                return;
            }
            console.info("Sent to postmark for delivery")
        });
}


//added jsmart support.  
//raw_replacements i.e. %replacement_variable% are kept here for backwards compatibility
var sendTemplate = function(templateName, to, replaces, jsmart_replaces) {
    jsmart_replaces = jsmart_replaces || {};

    jsmart_replaces.settings = mail_replacements;

	var doSend = function() {

        var raw_subject = __mailtemplates_cache[templateName].subject;
		var raw_body = __mailtemplates_cache[templateName].body;

        var jsmart_subject = new jSmart(raw_subject);
        var jsmart_body = new jSmart(raw_body);

		//var subject = __mailtemplates_cache[templateName].subject;
		//var body = __mailtemplates_cache[templateName].body;
        
        var subject = jsmart_subject.fetch(jsmart_replaces);
		var body = jsmart_body.fetch(jsmart_replaces);

		for (var k in replaces) {
			subject = subject.split('%' + k + '%').join(replaces[k]);
			body = body.split('%' + k + '%').join(replaces[k]);
		}

		for (var k in default_replaces) {
			subject = subject.split('%' + k + '%').join(default_replaces[k]);
			body = body.split('%' + k + '%').join(default_replaces[k]);
		}

		sendMail(to, subject, body);
	};

	if (typeof(__mailtemplates_cache[templateName]) === 'undefined') {
		fs.readFile('./data/mailtemplates/' + templateName + '.template', function(err, data) {
			var lines = ("" + data).split("\n");
			var subject = lines.shift();
			var body = lines.join("\n");
			__mailtemplates_cache[templateName] = {
				subject: subject,
				body: body
			};

			doSend();
		});
	} else {
		doSend();
	}
}


exports.transporter = transporter;
exports.send = sendMail;
exports.sendTemplate = sendTemplate;