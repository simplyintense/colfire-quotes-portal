var CookieParser = require('restify-cookies');
var restify = require('restify');
var rfr = require('rfr');
var db = rfr('includes/models');
var routes = rfr('includes/routes');
var servers = rfr('includes/servers');
var pages = rfr('config/pages.json');
var config = rfr('includes/config.js');
var path = require('path');

function respond(req, res, next) {
    res.send('hello ' + req.params.name);
    next();
}

var server = restify.createServer();
server.get('/hello/:name', respond);
server.head('/hello/:name', respond);



function getmyip(req, res, next) {
    srvhttp.request({
        hostname: 'myexternalip.com',
        path: '/raw',
        agent: false
    }, function (res) {
        if (res.statusCode != 200) {
            console.log('non-OK status: ' + res.statusCode);
        }
        res.setEncoding('utf-8');
        var ipAddress = '';
        res.on('data', function (chunk) {
            ipAddress += chunk;
        });
        res.on('end', function () {
            console.log(ipAddress);
            next();
        });
    }).on('error', function (err) {
        console.log(err);
    }).end();

    res.send('check console.log on the server =)');
    next();

}

server.get('/ip', getmyip);
server.head('/ip', getmyip);




db.sequelize
    .authenticate()
    .then(function (err) {
        console.log('Connection has been established successfully.');
    }, function (err) {
        console.log('Unable to connect to the database:', err);
    })
    .catch(function (err) {
        console.log('Unable to connect to the database:', err);
    });

// Routes
server.get('/api', function (request, response) {
    db.sequelize
        .authenticate()
        .then(function (err) {
            response.send('Connection has been established successfully. API is running');
            console.log('Connection has been established successfully.');
        }, function (err) {
            response.send('Unable to connect to the database. API not running. ' + err);
            console.log('Unable to connect to the database:', err);
        })
        .catch(function (err) {
            response.send('Unable to connect to the database: ' + err);
            console.log('Unable to connect to the database:', err);
        });
});




options = {};

// Server settings
server.pre(restify.pre.sanitizePath());
server.on('uncaughtException', function (req, res, route, err) {
    try {
        res.json(500, err);
    } catch (e) {
        res.end();
    }
    return true;
});

db.sequelize.Promise.onPossiblyUnhandledRejection(function (reason) {
    throw reason;
});

server.use(CookieParser.parse);
server.use(restify.queryParser());
server.use(restify.bodyParser());

// API routes
routes(function (routes) {
    routes.forEach(function (route) {
        server[route.method](route.route, route.handler);
    });
});

// Templates
var templatesDirectory = path.join(rfr.root, './public');
server.get(/jstemplates\/?.*/, servers.static_file_server({
    directory: templatesDirectory,
    suffix: '.tpl'
}));

// Static files and css/js minification
server.get(/images\/?.*/, servers.images_server());
server.get(/\/scripts\/?.*/, servers.public_server);
//server.get(/\/vendors\/?.*/, servers.public_server);
server.get(/\/vendors\/node_modules\/?.*/, servers.public_server);
server.get('/resources/js.js', servers.minify_server('javascript'));
server.get('/resources/css.css', servers.minify_server('css'));

// App index html file
for (var k in pages) {
    server.get(k, servers.index_server({
        indexFile: options.indexFile,
        indexDirectory: options.indexDirectory
    }));
}

db.sequelize.sync({
    force: false
}).then(function () {
    console.log('db sync finished');
}, function(err) {
    console.log(err);
});

if (process.env.COMPUTERNAME && process.env.COMPUTERNAME == 'WINDSOUNDDELL') {
    var port = 3337;  //for testing environment
} else if (process.env.COMPUTERNAME && process.env.COMPUTERNAME === 'YOGA') {
    port = 3337;  //for testing environment
} else {
    var port = process.env.PORT || 3337;
}

server.listen(port, function () {
    console.log('%s listening at %s', server.name, server.url);
});
